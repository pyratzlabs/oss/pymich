---
theme: gaia
autoscaling: true
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

# **Pymich**

Compiling a Python subset to Michelson

https://pymich.readthedocs.io/

---

# An upgradable contract

- We store a function in the storage
- We apply this function from the storage
- We allow updating this function from an entrypoint
- We can thus change the behavior of the smart contract

---
# An upgradable contract
## Pseudo-code

```python
class Contract:
    y: Nat
    f: Callable[[Nat], Nat]

    def update_f(self, f: Callable[[Nat], Nat]):
        self.f = f

    def update_y(self, x: Nat):
        self.y = self.f(x)
```

---
# An upgradable contract
## Pymich

```python
from pymich.michelson_types import *

class Contract:
    y: Nat
    f: Callable[[Nat], Nat]

    def update_f(self, f: Callable[[Nat], Nat]):
        self.f = f

    def update_y(self, x: Nat):
        self.y = self.f(x)
```
        
---
# Pymich goals

<style scoped>section { font-size: 170% }</style>

By defining a strict subset of Python that can compile to Michelson and ensuring that the semantics are preserved when executing the code in CPython or in a Michelson VM we get:

- syntax highlight
- auto-complete
- IDE integrated tychecking
- code formatters
- **breakpoints !!!!**
- jupyter notebooks (*e.g.* graphing AMM curves)
- ...
  
---
# What about the semantics ?

<style scoped>section { font-size: 140% }</style>

**Python and Michelson have very different semantics.**

Our constraints are:

- A Pymich typechecked program should always typecheck in MyPy **+++**
  + This allows IDEs to signal most, if not all, typechecking errors.
  + Type errors that are caught by Pymich but not MyPy should throw at runtime when the smart contract is ran in CPython.
- How are closures handled?
- How to distinguish between signed and unsigned integers?
- How to play nice with python LSP autocompletes? (are we really going to compile Python's `List.append`?)

**+++** since MyPy cannot be as restrictive than Pymich, /i.e./ valid MyPy typechecks can be invalid in Pymich
  
**All of these problems can be solved by using immutable datastructures.**
  
---
# Immutable data structures

<style scoped>section { font-size: 150% }</style>

**Pymich is /just/ a simple Python immutable data structures library**

Which makes sense, to each tool its job:

- ORM's have their own abstractions to manipulate DBs
- Numerical lib's have their own abstractions to manipulate array of numbers (ex: `np.array`)
- Pymich has **its own abstractions to manipulate Michelson datastructures**, that are fundamentally immutable.

**All datastructures are documented here: https://gitlab.com/pyratzlabs/oss/pymich/-/blob/master/pymich/michelson_types.py**
 
---
# Closures

Keeping in mind that:

- Since we are using immutable datastructures,
- and reassigning variables are prevented in Python unless `global`, `local` or `nonlocal` keywords are used, 
- then dissallowing them solves the problem of compiling closure reassignments.
- these keywords are so rarely used in Python, that this should not matter much
  
---
# Closures

## example

<style scoped>section { font-size: 190% }</style>

```python
k = Nat(10)

def f(x: Nat) -> Nat:
    return x + k
    
def g(x: Nat) -> Nat:
    return k * x

def compose(x: Nat) -> Nat:
    return k * f(g(x))
    
result = compose(k)
```
  
---
# Closures

## counter-example

<style scoped>section { font-size: 190% }</style>

Since Pymich closures partially apply variables in closures, the can not be mutated after the closure is defined to preserve. Pymich should throw an exception in strict mode and a warning otherwise:

```python
i = Nat(1)
def f(x: Nat) -> Nat:
    return x + i
i = Nat(2)

result = f(10)
# CPython returns Nat(12)
# Pymich retuns Nat(11)
```
  
---
# Inter contract calls

<style scoped>section { font-size: 110% }</style>

The `Contract` class exposes an `ops` attribute of type `Operations`. It is just a Michelson list of operations that only allows adding elements by pushing. Under the hood, the compiler prepends all transactions and inverses the operation list befor returning it.

This allows for constructing operations through functions.

The following code typechecks in MyPy:

```python
@dataclass
class MintOrBurn(Record):
    quantity: Int
    target: Address


def mint_or_burn(
        ops: Operations,
        lqt_address: Address,
        target: Address,
        quantity: Int,
    ) -> Operations:
    mint_or_burn_entrypoint = Contract[MintOrBurn](lqt_address, "%transfer")
    return ops.push(
        mint_or_burn_entrypoint,
        Mutez(0),
        MintOrBurn(quantity, target),
    )


class Proxy(Contract):
    def mint_or_burn(self, x: Nat) -> None:
        self.ops = mint_or_burn(self.ops, token_address, owner, Int(10))
```

---

# Inter contract calls

## Mypy typechecking

<style scoped>section { font-size: 170% }</style>

The `Operations` class type annotated in such a way that Mypy fails in the following example since `MintOrBurn(quantity, target)` is not of type `Nat`:

```python
mint_or_burn_entrypoint = Contract[Nat](lqt_address, "%transfer")
return ops.push(
    mint_or_burn_entrypoint,
    Mutez(0),
    MintOrBurn(quantity, target),
)
```
  
---
# Not supported

- passing `self` as a function parameter
- return anywhere other than as the last expression of a function body
  
---
# Choices

<style scoped>section { font-size: 180% }</style>

Mutating map or record elements in a Python closure would lead to a different behavior in Michelson unless some complex, sub-performant compiling logic is introducted.

We therefore chose to throw a warning on this syntax:

```python
michelson_map = Map[Nat, Nat]()
michelson_map[Nat(1)] = Nat(1)
```

And prefer this syntax:

```python
michelson_map = Map[Nat, Nat]()
michelson_map = michelson_map.set(Nat(1), Nat(1))
```
  
---
# Compiler architecture

<style scoped>section { font-size: 130% }</style>

```
pymich
├── compiler.py
├── michelson_types.py
├── pymich.py
├── stdlib.py
├── test_utils.py
├── frontend
│   ├── compiler.py
│   ├── passes.py
│   └── three_address_encode.py
├── middle_end
│   └── ir
│       ├── instr_types.py
│       ├── typechecker.py
│       └── vm_types.py
├── backend
│   ├── closures.py
│   ├── compiler.py
│   ├── micheline_emitter.py
│   └── michelson_typechecker.py
└── utils
    ├── compiler_stdlib.py
    ├── environment.py
    ├── exceptions.py
    └── helpers.py
```

---

# IR

- « three address coded » python
- single argument functions
- not yet SSA, need to figure out how to compile `phi(x1, x2)` efficiently
- dump contract IR with `pymich compile contract.py ir`

---
# Optimizations

Short term: 

- why not compile to Ligo and benefit from its optimizer
- allows focusing on the compiler frontend and testing framework
  
---
# Demo

All these rules may seem like a lot, but let's see what happens in the Python world if we respect them:

- LSP
- Mypy
- Breakpoints
- Jupyter Notebooks
