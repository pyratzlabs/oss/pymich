Pymich CLI
==========

Pymich provides a simple self-documented CLI using `cli2 <https://cli2.readthedocs.io/en/latest/>`_. If you have installed Pymich via Pypi, you will have the executable available in your shell provided Python's packages are in your path.


.. code-block:: bash

   $ pymich

   ERROR: No sub-command provided

   SYNOPSYS
   pymich.py SUB-COMMAND <...>
   pymich.py help SUB-COMMAND

   DESCRIPTION
   Represents a group of named commands.

   SUB-COMMANDS
   help     Get help for a command or group
   compile  Compiles a Python file and outputs the result to stdout

Compiling
---------

To Michelson:

.. code-block:: bash

   pymich compile <file-path> michelson

To Micheline:

.. code-block:: bash

   pymich compile <file-path> micheline

To Pymich-IR:

.. code-block:: bash

   pymich compile <file-path> ir
