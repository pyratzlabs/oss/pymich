.. Pymich documentation master file, created by
   sphinx-quickstart on Wed Apr 13 11:17:02 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Pymich
==================================

Build Tezos smart contracts using the programming language with the largest developer community.

The goal of this project is to build a compiler for a Tezos smart
contract language that is a subset of the Python language such that when
the smart contract is interpreted in CPython, it outputs the same
results as when its compiled-Michelson is interpreted in the Michelson VM.

That is, all Python code is not valid Python, but all Pymich code is
valid Python and guarantees the same behavior. This allows to leverage
the MyPy typechecker and IDE integration, autocomplete, Python test
tooling (test runner, code coverage...), go to definition and everything
that makes up a good Python development environment.


.. toctree::
   installation
   getting-started
   supported-python-subset
   pymich-types
   pymich-cli
   testing
   tooling
   examples
   compiler-architecture
   :maxdepth: 2
   :caption: Contents:

