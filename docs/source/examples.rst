Examples
========

FA1.2
-----

.. literalinclude:: ../../pymich/tests/end_to_end/FA12/FA12.py
  :language: Python

FA2 multi-asset
---------------

.. literalinclude:: ../../pymich/tests/end_to_end/FA2/multi_asset.py
  :language: Python

Auction
-------

.. literalinclude:: ../../pymich/tests/end_to_end/auction/auction.py
  :language: Python

Decentralized Exchange
----------------------

.. literalinclude:: ../../pymich/tests/end_to_end/dexter/dexter.py
  :language: Python

Election
--------

.. literalinclude:: ../../pymich/tests/end_to_end/election/election.py
  :language: Python

Escrow
------

.. literalinclude:: ../../pymich/tests/end_to_end/escrow/escrow.py
  :language: Python

Lottery
-------

.. literalinclude:: ../../pymich/tests/end_to_end/lottery/lottery.py
  :language: Python

Notarization
------------

.. literalinclude:: ../../pymich/tests/end_to_end/notarization/notarization.py
  :language: Python

Upgradable contract
-------------------

.. literalinclude:: ../../pymich/tests/end_to_end/upgradable/upgradable.py
  :language: Python

Visitor
-------

.. literalinclude:: ../../pymich/tests/end_to_end/visitor/visitor.py
  :language: Python
