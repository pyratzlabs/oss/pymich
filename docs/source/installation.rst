Installation
============

This document describes various means of installing Pymich.

Pip
---

Dependencies:

-   Pymich currently only supports Python 3.9.
-   A hard dependency of Pymich is
    `Pytezos <https://github.com/baking-bad/pytezos/>`_, which you need to
    install accordingly (along with its own dependencies).

You can then simply install pymich as follows:

.. code-block:: sh

    pip install pymich

The installation process will expose the :code:`pymich` command if your :code:`PATH` includes the path to your pip binary installs.

Docker
------

A Docker image is provided `here <https://hub.docker.com/r/pyratzlabs/pymich>`_.

.. code-block:: sh

   docker run --rm -v "$PWD":"$PWD" -w "$PWD" pyratzlabs/pymich compile <contract-source> michelson > output.tz


Git
---

.. code-block:: sh

    git clone git@yourlabs.io:pyratzlabs/pymich.git

Running the tests is then done using \`pytest\`:

.. code-block:: sh

    cd pymich
    pytest
