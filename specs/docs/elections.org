* Election contract

In this document, we present a simple election contract made with PyMich. We'll first go over its implementation, then interact with it using the PyTezos VM and the CPython interpreter to show that it has the same behavior between the two interpreters.

* Implementation

A PyMich contract is implemented as a Python class called ~Contract~, with attributes defined to be the contract's storage, and methods being its entrypoints. We make use of Python's type annotations to generate the storage, document the contract, and allow the typechecker to work properly.

Since we'd like this contract to run in a CPython interpreter without having to define an ~__init__~ method, we can use the ~dataclass~ class decorator to generate one for us. This decorator will be skipped by the PyMich compiler, but will allow us to instantiate the contract in CPython with ~Contract(storage_el_1, ...., storage_el_n)~.

Since PyMich's goal is to be isomorphic between the CPython interpreter and the Tezos Blockchain Michelson interpreter, we need to make a few imports that are unecessary to compile PyMich code, but necessary for CPython and all the Python tooling to work correctly (linters, type checkers, autocomplete etc): as you can see we have a contract administrator typed as an ~address~ which isn't a type defined in Python's standard library. Therefore, we provide a stub type with ~from stubs import *~ which will make ~SENDER~, the address of the transaction sender, available in our code.

The rest is fairly standard Python code. We also define a function that allows us to fail if a condition in not met. Note that Michelson's ~FAILWITH~ instruction can be summoned using a regular Python exception.

#+begin_src python
# election.py

from dataclasses import dataclass
from typing import Dict
from stubs import *


def require(condition: bool, message: str) -> int:
    if not condition:
        raise Exception(message)

    return 0


@dataclass
class Contract:
    admin: address
    manifest_url: str
    manifest_hash: str
    _open: str
    _close: str
    artifacts_url: str
    artifacts_hash: str

    def open(self, _open: str, manifest_url: str, manifest_hash: str):
        require(SENDER == self.admin, "Only admin can call this entrypoint")
        self._open = _open
        self.manifest_url = manifest_url
        self.manifest_hash = manifest_hash

    def close(self, _close: str):
        require(SENDER == self.admin, "Only admin can call this entrypoint")
        self._close = _close

    def artifacts(self, artifacts_url: str, artifacts_hash: str):
        require(SENDER == self.admin, "Only admin can call this entrypoint")
        self.artifacts_url = artifacts_url
        self.artifacts_hash = artifacts_hash

#+end_src

* Compiling the contract to Michelson

Let's now compile the contract to Michelson from Python. We'll import the PyMich compiler, pass it tour source, and compile it.

#+begin_src python
from pymich.compiler import Compiler

contract_source_file = "election.py"
with open(contract_source_file) as f:
    source = f.read()

micheline = Compiler(source).compile_contract()
#+end_src

Let's now run it in the PyTezos Michelson VM for which PyMich as a convenient wrapper available:

#+begin_src python
from pymich.compiler import VM
vm = VM()
vm.load_contract(micheline)

vm.contract
#  <pytezos.jupyter.ContractInterface object at 0x106e2bfa0>
#  
#  Properties
#  .block_id	head
#  .storage	# access storage data at block `block_id`
#  .parameter	# root entrypoint
#  
#  Entrypoints
#  .artifacts()
#  .close()
#  .open()
#  .default()
#  
#  Helpers
#  .big_map_get()
#  .create_from()
#  .from_context()
#  .from_file()
#  .from_micheline()
#  .from_michelson()
#  .from_url()
#  .metadata()
#  .metadata_url()
#  .operation_result()
#  .originate()
#  .program()
#  .script()
#  .to_file()
#  .to_micheline()
#  .to_michelson()
#  .using()
#+end_src

Let's interact with the ~open~ entrypoint. Let's first define a storage, we'll modify the default one generated by PyTezos so that we can have ourselves as the sender. The VM has a default sender set up, so we can just use this one for simplicity:

#+begin_src python
init_storage = vm.contract.storage.dummy()
init_storage['admin'] = vm.context.sender
#+end_src

We can now run the call and assert that the contract runs as expected by interpreting this call with the contract's admin address (~vm.context.sender~):

#+begin_src python
new_storage = vm.contract.open({"_open": "foo", "manifest_url": "bar", "manifest_hash": "baz"}).interpret(storage=init_storage, sender=vm.context.sender).storage
assert new_storage['_open'] == "foo"
assert new_storage['manifest_url'] == "bar"
assert new_storage['manifest_hash'] == "baz"

new_storage
#  {
#    'admin': 'tz3M4KAnKF2dCSjqfa1LdweNxBGQRqzvPL88',
#    'manifest_url': 'bar',
#    'manifest_hash': 'baz',
#    '_open': 'foo',
#    '_close': '',
#    'artifacts_url': '',
#    'artifacts_hash': ''
#  }
#+end_src

If we try to interpret this entrypoint's with a sender other than the admin, we'll get a Michelson runtime as expected:

#+begin_src python
from pytezos.michelson.micheline import MichelsonRuntimeError

try:
    new_storage = vm.contract.open({"_open": "foo", "manifest_url": "bar", "manifest_hash": "baz"}).interpret(storage=init_storage)
    assert 0
except MichelsonRuntimeError as e:
    self.assertEqual(e.format_stdout(), "FAILWITH: 'Only admin can call this entrypoint'")
#+end_src

* Interpreting the contract using CPython

Let's now interpret the ~open~ entrypoint in a regular CPython interpreter to show that we indeed get the same results.

We'll override the sender by monkey patching it. Note that since the ~address~ type is just an alias for an ~address~ in CPython's world, we can just pass it a string:

#+begin_src python
import stubs
admin = "some_admin"
stubs.SENDER = admin

from contract import Contract
#+end_src

Let's now instantiate and run the contract, call ~open~, and assert that we get the expected results:

#+begin_src python
contract = Contract(stubs.SENDER, "", "", "", "", "", "")
contract.open("foo", "bar", "baz")

assert contract._open == "foo"
assert contract.manifest_url == "bar"
assert contract.manifest_hash == "baz"
#+end_src

If we instantiate the contract with another admin, we'll get the expected exception that is raised:

#+begin_src python
contract = Contract("yolo", "", "", "", "", "", "")
try:
    contract.open("foo", "bar", "baz")
    assert 0
except Exception as e:
    assert e.args[0] == 'Only admin can call this entrypoint'
#+end_src

* Conclusion

For our first contract, a simple election contract, we learned how to implement it, compile it and showed that it results in an isomorphic behavior between the PyTezos VM and the CPython interpreter. You can find the full tests for the contract in [[../tests/end-to-end/election/test_pytezos_vm.py]] and [[../tests/end-to-end/election/test_cpython.py]].
