* Introduction

In this article, we'll implement a simple to record users into a smart contract.


* Implementation

#+begin_src python
# visitor.py

from dataclasses import dataclass
from typing import Dict
from pymich.stubs import address


@dataclass
class VisitorInfo:
    visits: int
    name: str

class Contract:
    visitors: Dict[str, VisitorInfo]
    total_visits: int

    def register(self, login: str, name: str):
        self.visitors[login] = VisitorInfo(0, name)

    def visit(self, login: str):
        self.visitors[login].visits = self.visitors[login].visits + 1
        self.total_visits = self.total_visits + 1
#+end_src

* Compilation

#+begin_src
python -m pymich.pymich visitor.py visitor.json micheline
#+end_src

* Test

#+begin_src python
# visitor_test.json

import unittest
import json
from pymich.compiler import VM

with open("tmp/visitor.json") as f:
    micheline = json.loads(f.read())

class TestContract(unittest.TestCase):
    def test_register(self):
        vm = VM()
        vm.load_contract(micheline)

        init_storage = {'total_visits': 0, 'visitors': {}}

        register_params ={"login": "dogus_admin", "name": "Dogus"}
        new_storage = vm.contract.register(register_params).interpret(storage=init_storage).storage

        self.assertEqual(new_storage['visitors']["dogus_admin"], {'visits': 0, 'name': 'Dogus'})
        self.assertEqual(new_storage['total_visits'], 0)

    def test_visit(self):
        vm = VM()
        vm.load_contract(micheline)

        init_storage = {'total_visits': 0, 'visitors': {"dogus_admin": {'visits': 0, 'name': 'Dogus'}}}

        new_storage = vm.contract.visit("dogus_admin").interpret(storage=init_storage).storage

        self.assertEqual(new_storage['visitors']["dogus_admin"]['visits'], 1)
        self.assertEqual(new_storage['total_visits'], 1)
#+end_src
