* Introduction

In this article, we shall implement a small lottery contract. A lottery, by definition, requires the generation of a random number, which is not trivial on smart contracts since they must be deterministic and random numbers are, by definition, not.

Make sure that you have updated Pymich before starting this tutorial:

#+begin_src
pip install --upgrade pymich
#+end_src

* Specification

- each user can bid some predefined amount, not less, not more.
- among the bidders, one is picked at random and takes all the bids.

What do we put in the storage? There must be a ~bid_amount~ field types as ~mutez~, and a dictionary that assigns to each address some information.

What are the entrypoints? There must be one allowing to bid, and one allowing the winner to claim the bids.

How do we pick a user at random? We'll see that in the next section.

* Picking a winner at random

We could either retrieve a random number off-chain, using an Oracle. Or, we can figure out a clever way to generate a random number on chain by asking all participants to send a random number and working out a random number form that.

We'll use the second, call a [[https://en.wikipedia.org/wiki/Commitment_scheme][commitment scheme]].

We do not want to allow the bidders to send a random number because then, other bidders could work out a winning number from that. So each bidder will:

1. commit to a hash of their random number,
2. reveal the value that was hashed, the contract then checks that the revealed value corresponds to the original hash.

We can then combine all of the values and generate a random winner from that.

In order to verify that the revealed value corresponds to the hash, we'll need to use some crypto-related functions provided in Michelson and available in Pymich. To get the hash of an arbitrary value, we first "pack" it, and then "hash" it with the ~blake2b~ function:

#+begin_src python
hash_value = blake2b(pack(value))
#+end_src

* Storage and entrypoints

We now have all the information necessary to design our storage. We'll need:

- the value to bid
- the deadline for both phases (commit a hashed value, and reveal the hashed value)
- a map recording the bids which will map addresses to records containing the bid number and the hashed value
- to simplify the calculations, we'll also cache the number of bids, the number of revealed values, and the sum of all the values.


We'll need three entrypoints:

- one to bid, where the bidder needs to send the appropriate amount in his transaction and provide a value hash
- one to reveal a value, the contract will then check that this value corresponds to the hashed value
- a claim entrypoint that will send all the bids back to the winner
  
#+begin_src python
from typing import Dict
from pymich.stubs import *

from dataclasses import dataclass
from datetime import datetime


@dataclass
class BidInfo:
    value_hash: bytes
    num_bid: nat

class Contract:
    bid_amount: mutez
    deadline_bet: datetime
    deadline_reveal: datetime
    bids: Dict[address, BidInfo]
    nb_bids: nat
    nb_revealed: int
    sum_values: int


def bet(self, value_hash: bytes):
    # check that the user has not already made a bid

    # check that the user has bidded the proper amount (stored in self.bid_amount)

    # check that the deadline to bet (self.deadline_bet) is not over

    # add the bid info (bid number and hashed value) to self.bids

    # increment self.nb_bids
    self.nb_bids = self.nb_bids + nat(1)

def reveal(self, value: int):
    # check that the sender has already made a bid

    # check that it is not too late to reveal a value (self.deadline_reveal)

    # check that the value corresponds to the stored hashed value

    # calculate the new self.sum_values

    # increment self.nb_revealed

    pass

def claim(self):
    # check that the reveal deadline is passed

    # check that the sender has made a bid

    # calculate the winner 
    num_winner = self.sum_values % self.nb_revealed

    if self.bids[SENDER].num_bid != num_winner:
        raise Exception("You have not won")

    # send the winner the contract's balance

    pass
#+end_src

* Operating on times

In Pymich, you can get the current time with ~datetime.now()~. Do ~from datetime import datetime~ so that your editor can typecheck it!

* Solution

The solution can be found [[../tests/end_to_end/lottery/lottery.py][here]].
