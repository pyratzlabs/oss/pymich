FROM yourlabs/python-arch:latest

#RUN pacman -Syu --noconfirm libsodium libsecp256k1 && rm -rf /var/cache/pacman/pkg

RUN mkdir /app
COPY setup.py /app
COPY pymich /app/pymich
RUN pip install --editable /app
ENTRYPOINT ["pymich"]