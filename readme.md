# Creators

PyraTzLabs is a Parisian based development and ventures studio with the
mission to deliver elegant applicative tools & solutions to foster Tezos
adoption.

# Goal of the project : Build Tezos smart contracts using the programming language with the largest developer community

The goal of this project is to build a compiler for a Tezos smart
contract language that is a subset of the Python language such that when
the smart contract is interpreted in CPython, it outputs the same
results as when its compiled-Michelson is interpreted in the Michelson
VM.

That is, all Python code is not valid Python, but all Pymich code is
valid Python and guarantees the same behavior. This allows to leverage
the MyPy typechecker and IDE integration, autocomplete, Python test
tooling (test runner, code coverage...), go to definition and everything
that makes up a good Python development environment.

# Documentation

The documentation is available [here](https://pymich.readthedocs.io/).
