#!/bin/bash

python -m mypy --strict FA12/FA12.py
python -m mypy --strict FA2/multi_asset.py
python -m mypy --strict auction/auction.py
python -m mypy --strict dexter/dexter.py
python -m mypy --strict election/election.py
python -m mypy --strict escrow/escrow.py
python -m mypy --strict lottery/lottery.py
python -m mypy --strict notarization/notarization.py
python -m mypy --strict heap/heap.py
python -m mypy --strict upgradable/upgradable.py
python -m mypy --strict visitor/visitor.py
