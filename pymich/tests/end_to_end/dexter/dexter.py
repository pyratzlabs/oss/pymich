# Pymich implementation of https://gitlab.com/dexter2tz/dexter2tz/-/blob/master/dexter.mligo
# Also uses flat curve from https://github.com/tezos-checker/flat-cfmm/blob/master/flat_cfmm.mligo

from dataclasses import dataclass
from pymich.michelson_types import *


PRICE_NUM = Nat(1)
PRICE_DENOM = Nat(1)

AMM = Nat(0)
CFMM = Nat(1)


def mutez_to_natural(a: Mutez) -> Nat:
    return a // Mutez(1)


def natural_to_mutez(a: Nat) -> Mutez:
    return a * Mutez(1)


def ceildiv(numerator: Nat, denominator: Nat) -> Nat:
    res = Nat(0)
    if denominator == Nat(0):
        raise Exception("DIV by 0")
    else:
        q = numerator // denominator
        r = numerator % denominator
        if r == Nat(0):
            res = q
        else:
            res = q + Nat(1)
    return res


@dataclass
class TransferParam(Record):
    _from: Address
    _to: Address
    value: Nat


@dataclass
class MintOrBurn(Record):
    quantity: Int
    target: Address


def token_transfer(
        ops: Operations,
        token_address: Address,
        from_: Address,
        to: Address,
        token_amount: Nat,
    ) -> Operations:
    transfer_entrypoint = Contract[TransferParam](token_address, "%transfer")
    return ops.push(
        transfer_entrypoint,
        Mutez(0),
        TransferParam(from_, to, token_amount),
    )


def mint_or_burn(
        ops: Operations,
        lqt_address: Address,
        target: Address,
        quantity: Int,
    ) -> Operations:
    mint_or_burn_entrypoint = Contract[MintOrBurn](lqt_address, "%transfer")
    return ops.push(
        mint_or_burn_entrypoint,
        Mutez(0),
        MintOrBurn(quantity, target),
    )


def xtz_transfer(
        ops: Operations,
        to: Address,
        amount: Mutez,
    ) -> Operations:
    return ops.push(
        Contract[Unit](to),
        amount,
        Unit(),
    )


def amm_tokens_bought(pool_in: Nat, pool_out: Nat, tokens_sold: Nat) -> Nat:
    return tokens_sold * Nat(997) * pool_out // (pool_in * Nat(1000) + (tokens_sold * Nat(997)))

@dataclass
class Point:
    x: Nat
    y: Nat


@dataclass
class SlopeInfo:
    """
    Gives information relative to the slope of a 2D curve

    :param [x]: x coodinate at which the slope is calculated
    :param [dx_dy]: derivative of x with respect to y
    """
    x: Nat
    dx_dy: Nat


def util(p: Point) -> SlopeInfo:
    plus = p.x + p.y
    minus = p.x - p.y
    plus_2 = plus * plus
    plus_4 = plus_2 * plus_2
    plus_8 = plus_4 * plus_4
    plus_7 = plus_8 // plus
    minus_2 = minus * minus
    minus_4 = minus_2 * minus_2
    minus_8 = minus_4 * minus_4
    minus_7 = Int(0)
    if minus != Int(0):
        minus_7 = minus_8 // minus

    return SlopeInfo(
        abs(plus_8 - minus_8),
        Nat(8) * abs(minus_7 + plus_7),
    )


@dataclass
class NewtonParam:
    x: Nat
    y: Nat
    dx: Nat
    dy: Nat
    u: Nat


def newton(param: NewtonParam) -> Nat:
    iterations = List[Nat](
        Nat(1), Nat(2), Nat(3), Nat(4),
    )
    for i in iterations:
        slope = util(Point(param.x + param.dx, abs(param.y - param.dy)))
        new_u = slope.x
        new_du_dy = slope.dx_dy
        param.dy = param.dy + abs((new_u - param.u) // new_du_dy)
    return param.dy


@dataclass
class FlatSwapParam:
    pool_in: Nat
    pool_out: Nat
    tokens_sold: Nat


def cfmm_tokens_bought(param: FlatSwapParam) -> Nat:
    x = param.pool_in * PRICE_NUM
    y = param.pool_out * PRICE_DENOM
    slope = util(Point(param.pool_in, param.pool_out))
    u = slope.x
    newton_param = NewtonParam(
        x,
        y,
        param.tokens_sold * PRICE_NUM,
        Nat(0),
        u,
    )
    return newton(newton_param)

def cfmm_xtz_bought(param: FlatSwapParam) -> Nat:
    x = param.pool_in * PRICE_DENOM
    y = param.pool_out * PRICE_NUM
    slope = util(Point(param.pool_in, param.pool_out))
    u = slope.x
    newton_param = NewtonParam(
        x,
        y,
        param.tokens_sold * PRICE_DENOM,
        Nat(0),
        u,
    )
    return newton(newton_param)


def get_tokens_bought(curve_id: Nat, xtz_pool: Nat, token_pool: Nat, nat_amount: Nat) -> Nat:
    if curve_id == AMM:
        return amm_tokens_bought(xtz_pool, token_pool, nat_amount)
    else:
        return cfmm_tokens_bought(FlatSwapParam(xtz_pool, token_pool, nat_amount))

def get_xtz_bought(curve_id: Nat, token_pool: Nat, xtz_pool: Nat, nat_amount: Nat) -> Nat:
    if curve_id == AMM:
        return amm_tokens_bought(token_pool, xtz_pool, nat_amount)
    else:
        return cfmm_tokens_bought(FlatSwapParam(token_pool, xtz_pool, nat_amount))


@dataclass(kw_only=True)
class Dexter(BaseContract):
    token_pool: Nat
    xtz_pool: Mutez
    lqt_total: Nat
    token_address: Address
    lqt_address: Address
    curve_id: Nat

    def add_liquidity(
            self,
            owner: Address,
            min_lqt_minted: Nat,
            max_tokens_deposited: Nat,
            deadline: Timestamp,
    ) -> None:
        if Timestamp.now() >= deadline:
            raise Exception("The current time must be less than the deadline.")
        else:
            xtz_pool = mutez_to_natural(self.xtz_pool)
            nat_amount = mutez_to_natural(Tezos.amount)
            lqt_minted = nat_amount * self.lqt_total // xtz_pool
            tokens_deposited = ceildiv(nat_amount * self.token_pool, xtz_pool)

            if tokens_deposited > max_tokens_deposited:
                raise Exception("Max tokens deposited must be greater than or equal to tokens deposited")
            elif lqt_minted < min_lqt_minted:
                raise Exception("Lqt minted must be greater than min lqt minted")
            else:
                self.lqt_total = self.lqt_total + lqt_minted
                self.token_pool = self.token_pool + tokens_deposited
                self.xtz_pool = self.xtz_pool + Tezos.amount

                self.ops = token_transfer(self.ops, self.token_address, Tezos.sender, Tezos.self_address, tokens_deposited)
                self.ops = mint_or_burn(self.ops, self.lqt_address, owner, lqt_minted.to_int())

    def remove_liquidity(
        self,
        to: Address,
        lqt_burned: Nat,
        min_xtz_withdrawn: Mutez,
        min_tokens_withdrawn: Nat,
        deadline: Timestamp,
    ) -> None:
        if Timestamp.now() >= deadline:
            raise Exception("The current time must be less than the deadline")
        elif Tezos.amount > Mutez(0):
            raise Exception("Amount must be zero")
        else:
            xtz_withdrawn = natural_to_mutez(lqt_burned * mutez_to_natural(self.xtz_pool) // self.lqt_total)
            tokens_withdrawn = lqt_burned * self.token_pool // self.lqt_total

            if xtz_withdrawn < min_xtz_withdrawn:
                raise Exception("The amount of xtz withdrawn must be greater than or equal to min xtz withdrawn")
            elif tokens_withdrawn < min_tokens_withdrawn:
                raise Exception("The amount of tokens withdrawn must be greater than or equal to min tokens withdrawn")
            else:
                self.lqt_total = (self.lqt_total - lqt_burned).is_nat().get("Cannot burn more than the total amount of lqt")
                self.token_pool = (self.token_pool - tokens_withdrawn).is_nat().get( "Token pool minus tokens withdrawn is negative")

                self.ops = mint_or_burn(self.ops, self.lqt_address, Tezos.sender, (Nat(0) - lqt_burned))
                self.ops = token_transfer(self.ops, self.token_address, Tezos.self_address, to, tokens_withdrawn)
                self.ops = xtz_transfer(self.ops, to, xtz_withdrawn)

    def xtz_to_token(self, to: Address, min_tokens_bought: Nat, deadline: Timestamp) -> None:
        if Timestamp.now() >= deadline:
            raise Exception("The current time must be less than the deadline")
        else:
            # we don't check that xtz_pool > 0, because that is impossible
            # unless all liquidity has been removed
            xtz_pool = mutez_to_natural(self.xtz_pool)
            nat_amount = mutez_to_natural(Tezos.amount)
            tokens_bought = get_tokens_bought(self.curve_id, xtz_pool, self.token_pool, nat_amount)
            if tokens_bought < min_tokens_bought:
                raise Exception("Tokens bought must be greater than or equal to min tokens bought")
            self.token_pool = (self.token_pool - tokens_bought).is_nat().get("Token pool minus tokens bought is negative")
            self.xtz_pool = self.xtz_pool + Tezos.amount
            self.ops = token_transfer(self.ops, self.token_address, Tezos.self_address, to, tokens_bought)

    def token_to_xtz(self, to: Address, tokens_sold: Nat, min_xtz_bought: Mutez, deadline: Timestamp) -> None:
        if Timestamp.now() >= deadline:
            raise Exception("The current time must be less than the deadline")
        elif Tezos.amount > Mutez(0):
            raise Exception("Amount must be zero")
        else:
            # we don't check that tokenPool > 0, because that is impossible
            # unless all liquidity has been removed
            xtz_bought = natural_to_mutez(get_xtz_bought(self.curve_id, self.token_pool, mutez_to_natural(self.xtz_pool), tokens_sold))
            if xtz_bought < min_xtz_bought:
                raise Exception("Xtz bought must be greater than or equal to min xtz bought")
            self.ops = token_transfer(self.ops, self.token_address, Tezos.sender, Tezos.self_address, tokens_sold)
            self.ops = xtz_transfer(self.ops, to, xtz_bought)
            self.token_pool = self.token_pool + tokens_sold
            self.xtz_pool = (self.xtz_pool - xtz_bought).get("negative mutez")
