from pymich.tests.end_to_end.dexter.dexter import Dexter
from pymich.michelson_types import Nat, Address, Mutez, Int, Timestamp, Operations, Tezos
from pymich.test_utils import TestContract
from pytezos.michelson.format import micheline_to_michelson
from pytezos.contract.result import OperationResult

from pymich.compiler import Compiler

from pytezos import pytezos

import pathlib
import os


ALICE_KEY = "edsk3EQB2zJvvGrMKzkUxhgERsy6qdDDw19TQyFWkYNUmGSxXiYm7Q"
ALICE_PK = "tz1Yigc57GHQixFwDEVzj5N1znSCU3aq15td"
SHELL = "http://localhost:8732"
using_params = dict(shell=SHELL, key=ALICE_KEY)
pytezos = pytezos.using(**using_params)
send_conf = dict(min_confirmations=1)



class TestPython(TestContract):
    contract_path = os.path.join(pathlib.Path(__file__).parent.absolute(), "dexter.py")

    def test_graph_conversions(self):
        dex = Dexter(
            token_pool=Nat(50),
            xtz_pool=Mutez(50),
            lqt_total=Nat(50),
            token_address=Address("KTtoken"),
            lqt_address=Address("KTlqt"),
            curve_id=Nat(0),
        )

        Tezos.amount = Mutez(50)
        dex.add_liquidity(
            owner=Address("tzAlice"),
            min_lqt_minted=Nat(1),
            max_tokens_deposited=Nat(1000000),
            deadline=Timestamp.now() + Int(10000),
        )

        self.assertEqual(dex.token_pool, Nat(100))
        self.assertEqual(dex.xtz_pool, Mutez(100))

        dex.ops = Operations()
        Tezos.amount = Mutez(0)
        dex.token_to_xtz(
            Address("tzBob"),
            Nat(100),
            Mutez(0),
            Timestamp.now() + Int(10)
        )
        self.assertEqual(dex.ops._Operations__list[1][1].amount, 49)

        Tezos.amount = Mutez(100)
        dex.ops = Operations()
        dex.xtz_to_token(to=Address("tzBob"), min_tokens_bought=Nat(0), deadline=Timestamp.now() + Int(10),)
        #self.assertEqual(dex.ops._Operations__list[0][2].value.value, 132)

    def test_jupyter(self):
        dex = Dexter(
            token_pool=Nat(700),
            xtz_pool=Mutez(1000),
            lqt_total=Nat(1000),
            token_address=Address("KTtoken"),
            lqt_address=Address("KTlqt"),
            curve_id=Nat(0),
        )

        from copy import deepcopy
        Tezos.amount = Mutez(3000)
        tmp_dex = deepcopy(dex)
        tmp_dex.xtz_to_token(
            to=Address("tzBob"),
            min_tokens_bought=Nat(0),
            deadline=Timestamp.now() + Int(10),
        )

        #conversions = []
        #pools = []
        #dex.ops = Operations()
        #for xtz_in_value in range(1, 3000):
        #    Tezos.amount = Mutez(xtz_in_value)
        #    tmp_dex = deepcopy(dex)
        #    tmp_dex.xtz_to_token(
        #        to=Address("tzBob"),
        #        min_tokens_bought=Nat(0),
        #        deadline=Timestamp.now() + Int(10),
        #    )
        #    conversions.append([Tezos.amount.amount, tmp_dex.ops._Operations__list[0][2].value.value])
        #    pools.append([tmp_dex.xtz_pool.amount, tmp_dex.token_pool.value])


class TestSandbox(TestContract):
    contract_path = os.path.join(pathlib.Path(__file__).parent.absolute(), "dexter.py")

    def skip_test_in_ci_for_now__test_deploy_sandbox(self):
        init_storage = self.contract.storage.dummy()

        self.contract = self.contract.using(**using_params)
        opg = self.contract.originate(initial_storage=init_storage).send(**send_conf)
        addr = OperationResult.from_operation_group(opg.opg_result)[0].originated_contracts[0]
        ci = pytezos.using(**using_params).contract(addr)
