import unittest

from pymich.backend.micheline_emitter import CompilerBackend
import pymich.middle_end.ir.instr_types as t


class TestCompilerBackend(unittest.TestCase):
    def test_compile_param(self):
        b = CompilerBackend()
        compiled_parameter = b.compile_type(t.Or(t.Int(), t.Int()))
        expected_parameter = {
            "prim": "or",
            "args": [
                {
                    "prim": "int",
                },
                {
                    "prim": "int",
                },
            ],
        }
        self.assertEqual(compiled_parameter, expected_parameter)


for TestSuite in [
    TestCompilerBackend,
]:
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestSuite)
    unittest.TextTestRunner().run(suite)
