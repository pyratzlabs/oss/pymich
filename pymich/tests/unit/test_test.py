import pytest

from pymich.test import ContractLoader


@pytest.mark.parametrize('path', (
    'end_to_end/FA2/multi_asset.py',
    'FA2/multi_asset.py',
))
def test_loader(path):
    loader = ContractLoader.factory(path)
    assert loader.source
    assert loader.compiler
    assert loader.micheline
    assert loader.interface
    assert loader.dummy
    assert loader.storage
    loader.storage['x'] = 'z'
    assert loader.storage['x'] == 'z'
