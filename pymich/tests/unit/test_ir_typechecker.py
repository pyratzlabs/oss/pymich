import pytest
import ast
import unittest

from pymich.utils.environment import Env
from pymich.middle_end.ir.typechecker import TypeChecker
import pymich.utils.exceptions as E
import pymich.middle_end.ir.instr_types as t


class TestArithmeticsTypechecker(unittest.TestCase):
    def test_typecheck_instantiate_nat(self):
        typechecker = TypeChecker()
        source = """
Nat(1)
        """
        module_ast = ast.parse(source)
        return_type = typechecker.get_expression_type(module_ast.body[0].value)
        assert isinstance(return_type, t.Nat)

    def test_typecheck_instantiate_nat_type_error(self):
        typechecker = TypeChecker()
        source = """
Nat("foo")
        """
        module_ast = ast.parse(source)
        with pytest.raises(E.TypeException):
            typechecker.get_expression_type(module_ast.body[0].value)

    def test_typecheck_nat_add_nat(self):
        typechecker = TypeChecker()
        source = """
Nat(1) + Nat(2)
        """
        module_ast = ast.parse(source)
        return_type = typechecker.get_expression_type(module_ast.body[0].value)
        assert isinstance(return_type, t.Nat)

    def test_typecheck_nat_sub_nat(self):
        typechecker = TypeChecker()
        source = """
Nat(1) - Nat(2)
        """
        module_ast = ast.parse(source)
        return_type = typechecker.get_expression_type(module_ast.body[0].value)
        assert isinstance(return_type, t.Int)

    def test_typecheck_nat_add_int(self):
        typechecker = TypeChecker()
        source = """
Nat(1) + Int(2)
        """
        module_ast = ast.parse(source)
        return_type = typechecker.get_expression_type(module_ast.body[0].value)
        assert isinstance(return_type, t.Int)

    def test_typecheck_add_int_string_error(self):
        typechecker = TypeChecker()
        source = """
Nat(1) + String("foo")
        """
        module_ast = ast.parse(source)
        with pytest.raises(E.CompilerException):
            typechecker.get_expression_type(module_ast.body[0].value)

class TestOptionsTypechecker(unittest.TestCase):
    def test_typecheck_open_option_some(self):
        typechecker = TypeChecker()
        source = """
Option(Nat(1)).get("my error")
        """
        module_ast = ast.parse(source)
        return_type = typechecker.get_expression_type(module_ast.body[0].value)
        assert isinstance(return_type, t.Nat)

    def test_typecheck_stdlib_method(self):
        typechecker = TypeChecker()
        source = """
Option(Nat(1)).get
        """
        module_ast = ast.parse(source)
        return_type = typechecker.get_expression_type(module_ast.body[0].value)
        expected_instance_type = t.StdlibMethodInstance(
            "",
            t.MultiArgFunctionPrototype(arg_types=[t.PythonString()], return_type=t.Nat()),
            typechecker.std_lib.types_mapping[t.Option].methods["get"].compile_callback,
        )
        assert return_type == expected_instance_type

    def test_typecheck_std_method_does_not_exist(self):
        typechecker = TypeChecker()
        source = """
Option(Nat(1)).foobar()
        """
        module_ast = ast.parse(source)
        with pytest.raises(E.CompilerException):
            typechecker.get_expression_type(module_ast.body[0].value)

    def test_typecheck_std_attribute_does_not_exist(self):
        typechecker = TypeChecker()
        source = """
Option(Nat(1)).foobar
        """
        module_ast = ast.parse(source)
        with pytest.raises(E.CompilerException):
            typechecker.get_expression_type(module_ast.body[0].value)


    def test_typecheck_std_method_attribute_type_error(self):
        typechecker = TypeChecker()
        source = """
Option(Nat(1)).get()
        """
        module_ast = ast.parse(source)
        with pytest.raises(E.TypeException):
            typechecker.get_expression_type(module_ast.body[0].value)

        source = """
Option(Nat(1)).get(0)
        """
        module_ast = ast.parse(source)
        with pytest.raises(E.TypeException):
            typechecker.get_expression_type(module_ast.body[0].value)

        source = """
Option(Nat(1)).get("foo", "bar")
        """
        module_ast = ast.parse(source)
        with pytest.raises(E.TypeException):
            typechecker.get_expression_type(module_ast.body[0].value)


class TestListTypechecker(unittest.TestCase):
    def skip_test_typecheck_list_prepend_type_correct(self):
        typechecker = TypeChecker()
        source = """
List[Nat]().prepend(Nat(0))
        """
        module_ast = ast.parse(source)
        with pytest.raises(E.TypeException):
            typechecker.get_expression_type(module_ast.body[0].value)

    def test_typecheck_list_prepend_type_error(self):
        typechecker = TypeChecker()
        source = """
List[Nat]().prepend(Int(0))
        """
        module_ast = ast.parse(source)
        with pytest.raises(E.TypeException):
            typechecker.get_expression_type(module_ast.body[0].value)


class TestBytesTypechecker(unittest.TestCase):
    def test_typecheck_bytes_unpack_argument(self):
        typechecker = TypeChecker()
        source = """
Bytes(String("hello")).unpack(String)
        """
        module_ast = ast.parse(source)
        return_type = typechecker.get_expression_type(module_ast.body[0].value)
        assert return_type == t.String()

    def test_typecheck_bytes_unpack_argument_argument_error(self):
        typechecker = TypeChecker()
        source = """
Bytes(String("hello")).unpack()
        """
        module_ast = ast.parse(source)
        with pytest.raises(E.TypeException):
            typechecker.get_expression_type(module_ast.body[0].value)

        typechecker = TypeChecker()
        source = """
Bytes(String("hello")).unpack(1)
        """
        module_ast = ast.parse(source)
        # TODO: implement this test
        #with pytest.raises(E.TypeException):
        #    typechecker.get_expression_type(module_ast.body[0].value)

class TestFunctionTypechecker(unittest.TestCase):
    def test_typecheck_function_argument_correct(self):
        typechecker = TypeChecker()
        source = """
foo(Int(1))
        """
        module_ast = ast.parse(source)
        types = {
            "foo": t.FunctionPrototype(
                arg_type=t.Int(),
                return_type=t.Int(),
            )
        }
        env = Env({}, -1, {}, types)
        return_type = typechecker.get_expression_type(module_ast.body[0].value, env)
        assert return_type == t.Int()

    def test_typecheck_function_argument_error(self):
        typechecker = TypeChecker()
        source = """
foo(1)
        """
        module_ast = ast.parse(source)
        types = {
            "foo": t.FunctionPrototype(
                arg_type=t.String(),
                return_type=t.Int(),
            )
        }
        env = Env({}, -1, {}, types)
        with pytest.raises(E.TypeException):
            typechecker.get_expression_type(module_ast.body[0].value, env)


class TestRecordTypechecker(unittest.TestCase):
    def test_typecheck_record_constructor_argument_correct(self):
        typechecker = TypeChecker()
