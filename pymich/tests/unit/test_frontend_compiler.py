import unittest
import ast

from pymich.frontend.three_address_encode import ThreeAddressEncode


class TestThreeAddressEncode(unittest.TestCase):
    def test_do_not_rewrite_simple_arithmetics(self):
        source = """
1 + 2
        """
        expected_source = """
1 + 2
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_statement(self):
        source = """
1 + 2 + 3
        """
        expected_source = """
tmp_var_0 = 1 + 2
tmp_var_0 + 3
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

        source = """
1 + 2 + 3 + 4
        """
        expected_source = """
tmp_var_0 = 1 + 2
tmp_var_1 = tmp_var_0 + 3
tmp_var_1 + 4
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

        source = """
1 + 2 + 3 + 4 + 5
        """
        expected_source = """
tmp_var_0 = 1 + 2
tmp_var_1 = tmp_var_0 + 3
tmp_var_2 = tmp_var_1 + 4
tmp_var_2 + 5
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_assign(self):
        source = """
a = 1 + 2 + 3
        """
        expected_source = """
tmp_var_0 = 1 + 2
a = tmp_var_0 + 3
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

        source = """
a = 1 + 2 * 3 + 4 - 5
        """
        expected_source = """
tmp_var_0 = 2 * 3
tmp_var_1 = 1 + tmp_var_0
tmp_var_2 = tmp_var_1 + 4
a = tmp_var_2 - 5
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_simple_subscript(self):
        source = """
a[1]
        """
        expected_source = """
a[1]
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_complex_subscript(self):
        source = """
a[1 + 2 + 3]
        """
        expected_source = """
tmp_var_0 = 1 + 2
tmp_var_1 = tmp_var_0 + 3
a[tmp_var_1]
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_subscript_name(self):
        source = """
foo.bar[1]
        """
        expected_source = """
tmp_var_0 = foo.bar
tmp_var_0[1]
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_assign_subscript_load(self):
        source = """
b = a[1 + 2 + 3]
        """
        expected_source = """
tmp_var_0 = 1 + 2
tmp_var_1 = tmp_var_0 + 3
b = a[tmp_var_1]
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_assign_subscript_save(self):
        source = """
a[1 + 2 + 3] = b
        """
        expected_source = """
tmp_var_0 = 1 + 2
tmp_var_1 = tmp_var_0 + 3
a[tmp_var_1] = b
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_assign_subscript_save_and_load(self):
        source = """
a[1 + 2 + 3] = b[4 + 5 + 6]
        """
        expected_source = """
tmp_var_0 = 4 + 5
tmp_var_1 = tmp_var_0 + 6
tmp_var_2 = 1 + 2
tmp_var_3 = tmp_var_2 + 3
a[tmp_var_3] = b[tmp_var_1]
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_do_not_rewrite_funcall_args(self):
        source = """
f(1)
        """
        expected_source = """
f(1)
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_funcall_single_args(self):
        source = """
f(1 + 2)
        """
        expected_source = """
tmp_var_0 = 1 + 2
f(tmp_var_0)
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        #print("\n" + ast.unparse(new_ast))
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_aa_rewrite_funcall_name(self):
        source = """
foo.bar(x)
        """
        expected_source = """
tmp_var_0 = foo.bar
tmp_var_0(x)
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_funcall_of_funcall(self):
        source = """
f(g(x))
        """
        expected_source = """
tmp_var_0 = g(x)
f(tmp_var_0)
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_funcall_multi_args(self):
        source = """
a = f(g(x), 1 + 2, 3)
        """
        expected_source = """
tmp_var_0 = g(x)
tmp_var_1 = 1 + 2
a = f(tmp_var_0, tmp_var_1, 3)
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_nested_record(self):
        source = """
foo.bar.baz
        """
        expected_source = """
tmp_var_0 = foo.bar
tmp_var_0.baz
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_nested_record_with_function(self):
        source = """
foo.bar.baz(x).foo#[x]
        """
        expected_source = """
tmp_var_0 = foo.bar
tmp_var_1 = tmp_var_0.baz
tmp_var_2 = tmp_var_1(x)
tmp_var_2.foo
#tmp_var_3[x]
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))


    def test_rewrite_nested_record_with_function_2(self):
        source = """
foo.bar.baz(x).foo[x]
        """
        expected_source = """
tmp_var_0 = foo.bar
tmp_var_1 = tmp_var_0.baz
tmp_var_2 = tmp_var_1(x)
tmp_var_3 = tmp_var_2.foo
tmp_var_3[x]
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_do_not_rewrite_record(self):
        source = """
foo.bar
        """
        expected_source = """
foo.bar
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_bool_op(self):
        source = """
1 == 1 and 2 >= 2
        """
        expected_source = """
tmp_var_0 = 1 == 1
tmp_var_1 = 2 >= 2
tmp_var_0 and tmp_var_1
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_do_not_rewrite_bool_op(self):
        source = """
1 == 1
        """
        expected_source = """
1 == 1
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_in(self):
        source = """
foo in bar[baz]
        """
        expected_source = """
tmp_var_0 = bar[baz]
foo in tmp_var_0
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_do_no_rewrite_in(self):
        source = """
foo in bar
        """
        expected_source = """
foo in bar
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_unary(self):
        source = """
not bar[baz]
        """
        expected_source = """
tmp_var_0 = bar[baz]
not tmp_var_0
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_do_not_rewrite_unary(self):
        source = """
not bar
        """
        expected_source = """
not bar
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_condition_if(self):
        source = """
if a == b:
    c = foo.bar.baz
        """
        expected_source = """
tmp_var_0 = a == b
if tmp_var_0:
    tmp_var_1 = foo.bar
    c = tmp_var_1.baz
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_condition_if_elif_else(self):
        source = """
if a == b:
    c = foo.bar.baz
elif a == c:
    d = foo.bar.baz
elif a == d:
    e = baz.bar.foo
else:
    f = baz.bar.foo
        """
        expected_source = """
tmp_var_0 = a == b
if tmp_var_0:
    tmp_var_1 = foo.bar
    c = tmp_var_1.baz
else:
    tmp_var_2 = a == c
    if tmp_var_2:
        tmp_var_3 = foo.bar
        d = tmp_var_3.baz
    else:
        tmp_var_4 = a == d
        if tmp_var_4:
            tmp_var_5 = baz.bar
            e = tmp_var_5.foo
        else:
            tmp_var_6 = baz.bar
            f = tmp_var_6.foo
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_rewrite_complex_1(self):
        source = """
b[1 + 2 + 3] + 4 + 5
        """
        expected_source = """
tmp_var_0 = 1 + 2
tmp_var_1 = tmp_var_0 + 3
tmp_var_2 = b[tmp_var_1]
tmp_var_3 = tmp_var_2 + 4
tmp_var_3 + 5
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))


    def test_rewrite_complex_2(self):
        source = """
foo.bar[1] + 2 + foo.bar[3] + baz[a + b]
        """
        expected_source = """
tmp_var_0 = foo.bar
tmp_var_1 = tmp_var_0[1]
tmp_var_2 = tmp_var_1 + 2
tmp_var_3 = foo.bar
tmp_var_4 = tmp_var_3[3]
tmp_var_5 = tmp_var_2 + tmp_var_4
tmp_var_6 = a + b
tmp_var_7 = baz[tmp_var_6]
tmp_var_5 + tmp_var_7
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))


    def test_rewrite_complex_3(self):
        source = """
foo.bar[1 + 2] + 3 + foo.bar[baz(x, y + z)] + baz[a + b](c - 4)
        """
        expected_source = """
tmp_var_0 = foo.bar
tmp_var_1 = 1 + 2
tmp_var_2 = tmp_var_0[tmp_var_1]
tmp_var_3 = tmp_var_2 + 3
tmp_var_4 = foo.bar
tmp_var_5 = y + z
tmp_var_6 = baz(x, tmp_var_5)
tmp_var_7 = tmp_var_4[tmp_var_6]
tmp_var_8 = tmp_var_3 + tmp_var_7
tmp_var_9 = a + b
tmp_var_10 = baz[tmp_var_9]
tmp_var_11 = c - 4
tmp_var_12 = tmp_var_10(tmp_var_11)
tmp_var_8 + tmp_var_12
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_propagates_into_function(self):
        """
        Ensures that arguments are not parsed, but body and
        function return are.
        """
        source = """
def f(x: Map[Nat, Nat], z: Map[Nat, Nat]):
    z = x[Nat(1)] + y[Nat(1)]
    return x[Nat(2)] + y[Nat(2)] + z
        """
        expected_source = """
def f(x: Map[Nat, Nat], z: Map[Nat, Nat]):
    tmp_var_0 = x[Nat(1)]
    tmp_var_1 = y[Nat(1)]
    z = tmp_var_0 + tmp_var_1
    tmp_var_2 = x[Nat(2)]
    tmp_var_3 = y[Nat(2)]
    tmp_var_4 = tmp_var_2 + tmp_var_3
    tmp_var_5 = tmp_var_4 + z
    return tmp_var_5
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_propagates_into_for_in_loops(self):
        """
        Ensures that arguments are not parsed, but body and
        function return are.
        """
        source = """
c = 0
for i in range(a + b):
    c = c + i * 2
        """
        expected_source = """
c = 0
tmp_var_0 = a + b
tmp_var_1 = range(tmp_var_0)
for i in tmp_var_1:
    tmp_var_2 = i * 2
    c = c + tmp_var_2
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_propagates_into_classes(self):
        source = """
class C:
    def f(x: Map[Nat, Nat], z: Map[Nat, Nat]):
        z = x[Nat(1)] + y[Nat(1)]
        return x[Nat(2)] + y[Nat(2)] + z
        """
        expected_source = """
class C:
    def f(x: Map[Nat, Nat], z: Map[Nat, Nat]):
        tmp_var_0 = x[Nat(1)]
        tmp_var_1 = y[Nat(1)]
        z = tmp_var_0 + tmp_var_1
        tmp_var_2 = x[Nat(2)]
        tmp_var_3 = y[Nat(2)]
        tmp_var_4 = tmp_var_2 + tmp_var_3
        tmp_var_5 = tmp_var_4 + z
        return tmp_var_5
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_expand_fa12(self):
        source = """
@dataclass(eq=True, frozen=True)
class AllowanceKey(Record):
    owner: Address
    spender: Address


@dataclass
class FA12(Contract):
    tokens: BigMap[Address, Nat]
    allowances: BigMap[AllowanceKey, Nat]
    total_supply: Nat
    owner: Address

    def mint(self, _to: Address, value: Nat):
        if SENDER != self.owner:
            raise Exception("Only owner can mint")

        self.total_supply = self.total_supply + value

        self.tokens[_to] = self.tokens.get(_to, Nat(0)) + value

    def approve(self, spender: Address, value: Nat):
        allowance_key = AllowanceKey(SENDER, spender)

        previous_value = self.allowances.get(allowance_key, Nat(0))

        if previous_value > Nat(0) and value > Nat(0):
            raise Exception("UnsafeAllowanceChange")

        self.allowances[allowance_key] = value

    def transfer(self, _from: Address, _to: Address, value: Nat):
        if SENDER != _from:
            allowance_key = AllowanceKey(_from, SENDER)

            authorized_value = self.allowances.get(allowance_key, Nat(0))

            if (authorized_value - value) < Int(0):
                raise Exception("NotEnoughAllowance")

            self.allowances[allowance_key] = abs(authorized_value - value)

        from_balance = self.tokens.get(_from, Nat(0))

        if (from_balance - value) < Int(0):
            raise Exception("NotEnoughBalance")

        self.tokens[_from] = abs(from_balance - value)

        to_balance = self.tokens.get(_to, Nat(0))

        self.tokens[_to] = to_balance + value

    def getAllowance(self, owner: Address, spender: Address) -> Nat:
        return self.allowances.get(AllowanceKey(owner, spender), Nat(0))

    def getBalance(self, owner: Address) -> Nat:
        return self.tokens.get(owner, Nat(0))

    def getTotalSupply(self) -> Nat:
        return self.total_supply
        """
        expected_source = """
@dataclass(eq=True, frozen=True)
class AllowanceKey(Record):
    owner: Address
    spender: Address

@dataclass
class FA12(Contract):
    tokens: BigMap[Address, Nat]
    allowances: BigMap[AllowanceKey, Nat]
    total_supply: Nat
    owner: Address

    def mint(self, _to: Address, value: Nat):
        tmp_var_5 = self.owner
        tmp_var_6 = SENDER != tmp_var_5
        if tmp_var_6:
            raise Exception('Only owner can mint')
        tmp_var_7 = self.total_supply
        self.total_supply = tmp_var_7 + value
        tmp_var_0 = self.tokens
        tmp_var_8 = self.tokens
        tmp_var_9 = tmp_var_8.get
        tmp_var_10 = tmp_var_9(_to, Nat(0))
        tmp_var_0[_to] = tmp_var_10 + value
        self.tokens = tmp_var_0

    def approve(self, spender: Address, value: Nat):
        allowance_key = AllowanceKey(SENDER, spender)
        tmp_var_11 = self.allowances
        tmp_var_12 = tmp_var_11.get
        previous_value = tmp_var_12(allowance_key, Nat(0))
        tmp_var_13 = previous_value > Nat(0)
        tmp_var_14 = value > Nat(0)
        tmp_var_15 = tmp_var_13 and tmp_var_14
        if tmp_var_15:
            raise Exception('UnsafeAllowanceChange')
        tmp_var_1 = self.allowances
        tmp_var_1[allowance_key] = value
        self.allowances = tmp_var_1

    def transfer(self, _from: Address, _to: Address, value: Nat):
        tmp_var_16 = SENDER != _from
        if tmp_var_16:
            allowance_key = AllowanceKey(_from, SENDER)
            tmp_var_17 = self.allowances
            tmp_var_18 = tmp_var_17.get
            authorized_value = tmp_var_18(allowance_key, Nat(0))
            tmp_var_19 = authorized_value - value
            tmp_var_20 = tmp_var_19 < Int(0)
            if tmp_var_20:
                raise Exception('NotEnoughAllowance')
            tmp_var_2 = self.allowances
            tmp_var_21 = authorized_value - value
            tmp_var_2[allowance_key] = abs(tmp_var_21)
            self.allowances = tmp_var_2
        tmp_var_22 = self.tokens
        tmp_var_23 = tmp_var_22.get
        from_balance = tmp_var_23(_from, Nat(0))
        tmp_var_24 = from_balance - value
        tmp_var_25 = tmp_var_24 < Int(0)
        if tmp_var_25:
            raise Exception('NotEnoughBalance')
        tmp_var_3 = self.tokens
        tmp_var_26 = from_balance - value
        tmp_var_3[_from] = abs(tmp_var_26)
        self.tokens = tmp_var_3
        tmp_var_27 = self.tokens
        tmp_var_28 = tmp_var_27.get
        to_balance = tmp_var_28(_to, Nat(0))
        tmp_var_4 = self.tokens
        tmp_var_4[_to] = to_balance + value
        self.tokens = tmp_var_4

    def getAllowance(self, owner: Address, spender: Address) -> Nat:
        tmp_var_29 = self.allowances
        tmp_var_30 = tmp_var_29.get
        tmp_var_31 = AllowanceKey(owner, spender)
        tmp_var_32 = tmp_var_30(tmp_var_31, Nat(0))
        return tmp_var_32

    def getBalance(self, owner: Address) -> Nat:
        tmp_var_33 = self.tokens
        tmp_var_34 = tmp_var_33.get
        tmp_var_35 = tmp_var_34(owner, Nat(0))
        return tmp_var_35

    def getTotalSupply(self) -> Nat:
        tmp_var_36 = self.total_supply
        return tmp_var_36
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))


class TestThreeAddressEncodeStdLib(unittest.TestCase):
    def test_do_not_rewrite_type_constructors(self):
        source = """
Map[Nat(1), MyRecord]()
        """
        expected_source = """
Map[Nat(1), MyRecord]()
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

        source = """
f(Map[Nat, Address]().add(Nat(1), Address("tz...")), 1)
        """
        expected_source = """
tmp_var_0 = Map[Nat, Address]()
tmp_var_1 = tmp_var_0.add
tmp_var_2 = tmp_var_1(Nat(1), Address('tz...'))
f(tmp_var_2, 1)
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))

    def test_is_base_type(self):
        sources = [
            "my_var",
            "Nat(1)",
            "Int(1)",
            "String('foo')",
            "Mutez(1)",
            "Address('tz...')",
            "Timestamp(123)",
        ]
        for source in sources:
            source_ast = ast.parse(source)
            is_base_type = ThreeAddressEncode().helpers.is_base_type
            stmt = source_ast.body[0].value
            self.assertEqual(is_base_type(stmt), True)

    def test_base_type_in_function(self):
        """
        ex: `Nat(1)` should not be refactory into a variable as it is considered
            an atomic intruction that compiles to Michelson natural numbers.
        """
        source = """
my_var
f(Nat(1))
f(Int(1))
f(Bytes(1)),
f(String('foo')),
f(Mutez(1)),
f(Address('tz...')),
f(Timestamp(123)),
        """
        expected_source = """
my_var
f(Nat(1))
f(Int(1))
f(Bytes(1)),
f(String('foo')),
f(Mutez(1)),
f(Address('tz...')),
f(Timestamp(123)),
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.dump(new_ast), ast.dump(expected_ast))


class TestThreeAddressEncodeTacPass1(unittest.TestCase):
    def test_do_not_expand_unnested_record_assignment(self):
        source = """
foo.bar = 1
        """
        expected_source = """
foo.bar = 1
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))

    def test_expand_nested_record_assignment(self):
        source = """
foo.bar.baz.cha.bla = 1
        """
        expected_source = """
tmp_var_0 = foo.bar
tmp_var_1 = tmp_var_0.baz
tmp_var_2 = tmp_var_1.cha
tmp_var_2.bla = 1
tmp_var_1.cha = tmp_var_2
tmp_var_0.baz = tmp_var_1
foo.bar = tmp_var_0
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))

    def test_do_not_expand_unnested_dict_assignment(self):
        source = """
foo[bar] = 1
        """
        expected_source = """
foo[bar] = 1
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))

    def test_expand_nested_dict_assignment(self):
        source = """
foo[bar][baz][cha][bla] = 1
        """
        expected_source = """
tmp_var_0 = foo[bar]
tmp_var_1 = tmp_var_0[baz]
tmp_var_2 = tmp_var_1[cha]
tmp_var_2[bla] = 1
tmp_var_1[cha] = tmp_var_2
tmp_var_0[baz] = tmp_var_1
foo[bar] = tmp_var_0
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))

    def test_expand_nested_dict_attributes_assignment(self):
        source = """
foo[bar][foo].baz[cha].bla = 1
        """
        expected_source = """
tmp_var_0 = foo[bar]
tmp_var_1 = tmp_var_0[foo]
tmp_var_2 = tmp_var_1.baz
tmp_var_3 = tmp_var_2[cha]
tmp_var_3.bla = 1
tmp_var_2[cha] = tmp_var_3
tmp_var_1.baz = tmp_var_2
tmp_var_0[foo] = tmp_var_1
foo[bar] = tmp_var_0
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))


class TestThreeAddressEncodeTacPass1And2(unittest.TestCase):
    def test_expand_nested_dict_attributes_assignment(self):
        source = """
foo[a + b][foo].baz[cha.chla].bla = 1
        """
        expected_source = """
tmp_var_4 = a + b
tmp_var_0 = foo[tmp_var_4]
tmp_var_1 = tmp_var_0[foo]
tmp_var_2 = tmp_var_1.baz
tmp_var_5 = cha.chla
tmp_var_3 = tmp_var_2[tmp_var_5]
tmp_var_3.bla = 1
tmp_var_6 = cha.chla
tmp_var_2[tmp_var_6] = tmp_var_3
tmp_var_1.baz = tmp_var_2
tmp_var_0[foo] = tmp_var_1
tmp_var_7 = a + b
foo[tmp_var_7] = tmp_var_0
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))

    def test_expand_nested_dict_attributes_assignment(self):
        source = """
foo[a + b][foo].baz[cha.chla].bla = flu[c + d](glu + moo)(Nat(10)).barf[cha.chla].bla
        """
        expected_source = """
tmp_var_4 = a + b
tmp_var_0 = foo[tmp_var_4]
tmp_var_1 = tmp_var_0[foo]
tmp_var_2 = tmp_var_1.baz
tmp_var_5 = cha.chla

# at this point: tmp_var_3 = foo[a + b][foo].baz[cha.chla]
tmp_var_3 = tmp_var_2[tmp_var_5]
tmp_var_6 = c + d
tmp_var_7 = flu[tmp_var_6]
tmp_var_8 = glu + moo
tmp_var_9 = tmp_var_7(tmp_var_8)
tmp_var_10 = tmp_var_9(Nat(10))
tmp_var_11 = tmp_var_10.barf
tmp_var_12 = cha.chla

# at this point: tmp_var_13 = flu[c + d](glu + moo)(Nat(10)).barf[cha.chla]
tmp_var_13 = tmp_var_11[tmp_var_12]
tmp_var_3.bla = tmp_var_13.bla
tmp_var_14 = cha.chla
tmp_var_2[tmp_var_14] = tmp_var_3
tmp_var_1.baz = tmp_var_2
tmp_var_0[foo] = tmp_var_1
tmp_var_15 = a + b
foo[tmp_var_15] = tmp_var_0
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))

    def test_expand_in_function(self):
        source = """
def approve(approve__param: approveParam) -> Storage:
    spender = approve__param.spender
    value = approve__param.value
    allowance_key = AllowanceKey(SENDER, spender)
    previous_value = __STORAGE__.allowances.get(allowance_key, Nat(0))
    if previous_value > Nat(0) and value > Nat(0):
        raise Exception('UnsafeAllowanceChange')
    __STORAGE__.allowances[allowance_key] = value
    return __STORAGE__
        """
        expected_source = """
def approve(approve__param: approveParam) -> Storage:
    spender = approve__param.spender
    value = approve__param.value
    allowance_key = AllowanceKey(SENDER, spender)
    tmp_var_1 = __STORAGE__.allowances
    tmp_var_2 = tmp_var_1.get
    previous_value = tmp_var_2(allowance_key, Nat(0))
    tmp_var_3 = previous_value > Nat(0)
    tmp_var_4 = value > Nat(0)
    tmp_var_5 = tmp_var_3 and tmp_var_4
    if tmp_var_5:
        raise Exception('UnsafeAllowanceChange')
    tmp_var_0 = __STORAGE__.allowances
    tmp_var_0[allowance_key] = value
    __STORAGE__.allowances = tmp_var_0
    return __STORAGE__
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))

    def test_expand_fa12_post_macro_expand(self):
        source = """
@dataclass(eq=True, frozen=True)
class AllowanceKey(Record):
    owner: Address
    spender: Address

@dataclass
class Storage:
    tokens: BigMap[Address, Nat]
    allowances: BigMap[AllowanceKey, Nat]
    total_supply: Nat
    owner: Address

@dataclass
class FA12(Contract):

    def deploy():
        return Storage()

    def mint(mint__param: mintParam) -> Storage:
        _to = mint__param._to
        value = mint__param.value
        if SENDER != __STORAGE__.owner:
            raise Exception('Only owner can mint')
        __STORAGE__.total_supply = __STORAGE__.total_supply + value
        __STORAGE__.tokens[_to] = __STORAGE__.tokens.get(_to, Nat(0)) + value
        return __STORAGE__

    def approve(approve__param: approveParam) -> Storage:
        spender = approve__param.spender
        value = approve__param.value
        allowance_key = AllowanceKey(SENDER, spender)
        previous_value = __STORAGE__.allowances.get(allowance_key, Nat(0))
        if previous_value > Nat(0) and value > Nat(0):
            raise Exception('UnsafeAllowanceChange')
        __STORAGE__.allowances[allowance_key] = value
        return __STORAGE__

    def transfer(transfer__param: transferParam) -> Storage:
        _from = transfer__param._from
        _to = transfer__param._to
        value = transfer__param.value
        if SENDER != _from:
            allowance_key = AllowanceKey(_from, SENDER)
            authorized_value = __STORAGE__.allowances.get(allowance_key, Nat(0))
            if authorized_value - value < Int(0):
                raise Exception('NotEnoughAllowance')
            __STORAGE__.allowances[allowance_key] = abs(authorized_value - value)
        from_balance = __STORAGE__.tokens.get(_from, Nat(0))
        if from_balance - value < Int(0):
            raise Exception('NotEnoughBalance')
        __STORAGE__.tokens[_from] = abs(from_balance - value)
        to_balance = __STORAGE__.tokens.get(_to, Nat(0))
        __STORAGE__.tokens[_to] = to_balance + value
        return __STORAGE__

    def getAllowance(getAllowance__param: getAllowanceParam) -> Storage:
        owner = getAllowance__param.owner
        spender = getAllowance__param.spender
        __callback__ = getAllowance__param.__callback__transaction(__callback__, Mutez(0), __STORAGE__.allowances.get(AllowanceKey(owner, spender), Nat(0)))
        return __STORAGE__

    def getBalance(getBalance__param: getBalanceParam) -> Storage:
        owner = getBalance__param.owner
        __callback__ = getBalance__param.__callback__transaction(__callback__, Mutez(0), __STORAGE__.tokens.get(owner, Nat(0)))
        return __STORAGE__

    def getTotalSupply(__callback__: Contract[Nat]) -> Storage:
        transaction(__callback__, Mutez(0), __STORAGE__.total_supply)
        return __STORAGE__
        """
        expected_source = """
@dataclass(eq=True, frozen=True)
class AllowanceKey(Record):
    owner: Address
    spender: Address

@dataclass
class Storage:
    tokens: BigMap[Address, Nat]
    allowances: BigMap[AllowanceKey, Nat]
    total_supply: Nat
    owner: Address

@dataclass
class FA12(Contract):

    def deploy():
        tmp_var_5 = Storage()
        return tmp_var_5

    def mint(mint__param: mintParam) -> Storage:
        _to = mint__param._to
        value = mint__param.value
        tmp_var_6 = __STORAGE__.owner
        tmp_var_7 = SENDER != tmp_var_6
        if tmp_var_7:
            raise Exception('Only owner can mint')
        tmp_var_8 = __STORAGE__.total_supply
        __STORAGE__.total_supply = tmp_var_8 + value
        tmp_var_0 = __STORAGE__.tokens
        tmp_var_9 = __STORAGE__.tokens
        tmp_var_10 = tmp_var_9.get
        tmp_var_11 = tmp_var_10(_to, Nat(0))
        tmp_var_0[_to] = tmp_var_11 + value
        __STORAGE__.tokens = tmp_var_0
        return __STORAGE__

    def approve(approve__param: approveParam) -> Storage:
        spender = approve__param.spender
        value = approve__param.value
        allowance_key = AllowanceKey(SENDER, spender)
        tmp_var_12 = __STORAGE__.allowances
        tmp_var_13 = tmp_var_12.get
        previous_value = tmp_var_13(allowance_key, Nat(0))
        tmp_var_14 = previous_value > Nat(0)
        tmp_var_15 = value > Nat(0)
        tmp_var_16 = tmp_var_14 and tmp_var_15
        if tmp_var_16:
            raise Exception('UnsafeAllowanceChange')
        tmp_var_1 = __STORAGE__.allowances
        tmp_var_1[allowance_key] = value
        __STORAGE__.allowances = tmp_var_1
        return __STORAGE__

    def transfer(transfer__param: transferParam) -> Storage:
        _from = transfer__param._from
        _to = transfer__param._to
        value = transfer__param.value
        tmp_var_17 = SENDER != _from
        if tmp_var_17:
            allowance_key = AllowanceKey(_from, SENDER)
            tmp_var_18 = __STORAGE__.allowances
            tmp_var_19 = tmp_var_18.get
            authorized_value = tmp_var_19(allowance_key, Nat(0))
            tmp_var_20 = authorized_value - value
            tmp_var_21 = tmp_var_20 < Int(0)
            if tmp_var_21:
                raise Exception('NotEnoughAllowance')
            tmp_var_2 = __STORAGE__.allowances
            tmp_var_22 = authorized_value - value
            tmp_var_2[allowance_key] = abs(tmp_var_22)
            __STORAGE__.allowances = tmp_var_2
        tmp_var_23 = __STORAGE__.tokens
        tmp_var_24 = tmp_var_23.get
        from_balance = tmp_var_24(_from, Nat(0))
        tmp_var_25 = from_balance - value
        tmp_var_26 = tmp_var_25 < Int(0)
        if tmp_var_26:
            raise Exception('NotEnoughBalance')
        tmp_var_3 = __STORAGE__.tokens
        tmp_var_27 = from_balance - value
        tmp_var_3[_from] = abs(tmp_var_27)
        __STORAGE__.tokens = tmp_var_3
        tmp_var_28 = __STORAGE__.tokens
        tmp_var_29 = tmp_var_28.get
        to_balance = tmp_var_29(_to, Nat(0))
        tmp_var_4 = __STORAGE__.tokens
        tmp_var_4[_to] = to_balance + value
        __STORAGE__.tokens = tmp_var_4
        return __STORAGE__

    def getAllowance(getAllowance__param: getAllowanceParam) -> Storage:
        owner = getAllowance__param.owner
        spender = getAllowance__param.spender
        tmp_var_30 = getAllowance__param.__callback__transaction
        tmp_var_31 = __STORAGE__.allowances
        tmp_var_32 = tmp_var_31.get
        tmp_var_33 = AllowanceKey(owner, spender)
        tmp_var_34 = tmp_var_32(tmp_var_33, Nat(0))
        __callback__ = tmp_var_30(__callback__, Mutez(0), tmp_var_34)
        return __STORAGE__

    def getBalance(getBalance__param: getBalanceParam) -> Storage:
        owner = getBalance__param.owner
        tmp_var_35 = getBalance__param.__callback__transaction
        tmp_var_36 = __STORAGE__.tokens
        tmp_var_37 = tmp_var_36.get
        tmp_var_38 = tmp_var_37(owner, Nat(0))
        __callback__ = tmp_var_35(__callback__, Mutez(0), tmp_var_38)
        return __STORAGE__

    def getTotalSupply(__callback__: Contract[Nat]) -> Storage:
        tmp_var_39 = __STORAGE__.total_supply
        transaction(__callback__, Mutez(0), tmp_var_39)
        return __STORAGE__
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))

    def test_expand_multiple_functions(self):
        source = """
@dataclass
class FA12(Contract):

    def deploy():
        return Storage()

    def mint(mint__param: mintParam) -> Storage:
        __STORAGE__.tokens[_to] = __STORAGE__.tokens.get(_to, Nat(0)) + value
        return __STORAGE__

    def approve(approve__param: approveParam) -> Storage:
        __STORAGE__.allowances[allowance_key] = value
        return __STORAGE__
        """
        expected_source = """
@dataclass
class FA12(Contract):

    def deploy():
        tmp_var_2 = Storage()
        return tmp_var_2

    def mint(mint__param: mintParam) -> Storage:
        tmp_var_0 = __STORAGE__.tokens
        tmp_var_3 = __STORAGE__.tokens
        tmp_var_4 = tmp_var_3.get
        tmp_var_5 = tmp_var_4(_to, Nat(0))
        tmp_var_0[_to] = tmp_var_5 + value
        __STORAGE__.tokens = tmp_var_0
        return __STORAGE__

    def approve(approve__param: approveParam) -> Storage:
        tmp_var_1 = __STORAGE__.allowances
        tmp_var_1[allowance_key] = value
        __STORAGE__.allowances = tmp_var_1
        return __STORAGE__
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))

    def test_expand_if_else(self):
        source = """
def create_token(self, metadata: TokenMetadata):
    require_owner(self.owner)

    new_token_id = metadata.token_id

    if new_token_id in self.token_metadata:
        raise Exception("FA2_DUP_TOKEN_ID")
    else:
        self.token_metadata[new_token_id] = metadata
        self.token_total_supply[new_token_id] = Nat(0)
        """
        source = """
if new_token_id in self.token_metadata:
    raise Exception("FA2_DUP_TOKEN_ID")
else:
    self.token_metadata[new_token_id].yolo = metadata
    self.token_total_supply[new_token_id] = Nat(0)
        """
        expected_source = """
tmp_var_3 = self.token_metadata
tmp_var_4 = new_token_id in tmp_var_3
if tmp_var_4:
    raise Exception('FA2_DUP_TOKEN_ID')
else:
    tmp_var_0 = self.token_metadata
    tmp_var_1 = tmp_var_0[new_token_id]
    tmp_var_1.yolo = metadata
    tmp_var_0[new_token_id] = tmp_var_1
    self.token_metadata = tmp_var_0
    tmp_var_2 = self.token_total_supply
    tmp_var_2[new_token_id] = Nat(0)
    self.token_total_supply = tmp_var_2
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))



    def test_expand_through_classes(self):
        source = """
class Visitor:
    def register(self, name: String):
        self.visitors[SENDER] = VisitorInfo(Nat(0), name, Timestamp.now())

    def visit(self):
        self.visitors[SENDER].visits = n_visits + Nat(1)
    """
        expected_source = """
class Visitor:
    def register(self, name: String):
        tmp_var_0 = self.visitors
        tmp_var_3 = Timestamp.now
        tmp_var_4 = tmp_var_3()
        tmp_var_0[SENDER] = VisitorInfo(Nat(0), name, tmp_var_4)
        self.visitors = tmp_var_0

    def visit(self):
        tmp_var_1 = self.visitors
        tmp_var_2 = tmp_var_1[SENDER]
        tmp_var_2.visits = n_visits + Nat(1)
        tmp_var_1[SENDER] = tmp_var_2
        self.visitors = tmp_var_1
        """
        source_ast = ast.parse(source)
        expected_ast = ast.parse(expected_source)
        new_ast = ThreeAddressEncode().compile_module(source_ast)
        self.assertEqual(ast.unparse(new_ast), ast.unparse(expected_ast))
