import unittest
from copy import deepcopy

from pymich.backend.michelson_typechecker import (
    MichelsonTypeChecker,
    StackLengthException,
    InvalidMichelsonException,
    StackType as Stack,
    BooleanTypeChecker,
    TypeException,
    MapTypeChecker,
    StackManipulationTypeChecker,
    IntOrNatTypeChecker,
    StringTypeChecker,
    PairTypeChecker,
    OptionTypeChecker,
    AddressTypeChecker,
    MutezTypeChecker,
    BytesTypeChecker,
    CryptographicPrimitivesTypeChecker,
    ControlStructuresTypeChecker,
    UnionTypeChecker,
    ListTypeChecker,
)
import pymich.utils.exceptions as E
import pymich.middle_end.ir.instr_types as t
from pymich.middle_end.ir.vm_types import Instr

from pymich.compiler import Compiler
from pytezos.michelson.format import micheline_to_michelson

from pymich.compiler import Compiler
import logging


logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)


def print_michelson(source: str):
        micheline = Compiler(source).compile_contract()
        michelson = micheline_to_michelson(micheline)
        print(michelson)


class TestIntOrNatTypeChecker(unittest.TestCase):
    def test_typecheck_neg(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int()])
        typechecker.typecheck_neg()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack = Stack([t.Nat()])
        typechecker.typecheck_neg()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_neg_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_neg()

    def test_typecheck_neg_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Address()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_neg()

    def test_typecheck_abs(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int()])
        typechecker.typecheck_abs()
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

    def test_typecheck_abs_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_abs()

    def test_typecheck_abs_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_abs()

    def test_typecheck_is_nat(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int()])
        typechecker.typecheck_is_nat()
        self.assertEqual(typechecker.stack, Stack([t.Option(t.Nat())]))

    def test_typecheck_is_nat_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_abs()

    def test_typecheck_is_nat_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_abs()

    def test_typecheck_int(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat()])
        typechecker.typecheck_int()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_is_int_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_int()

    def test_typecheck_is_int_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_int()

    def test_typecheck_or(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat(), t.Nat()])
        typechecker.typecheck_or()
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

    def test_typecheck_or_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_or()

        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_or()

    def test_typecheck_or_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int(), t.Nat()])
        with self.assertRaises(E.OperandException):
            typechecker.typecheck_or()

    def test_typecheck_and(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat(), t.Nat()])
        typechecker.typecheck_and()
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

        typechecker.stack = Stack([t.Nat(), t.Int()])
        typechecker.typecheck_and()
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

    def test_typecheck_and_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_and()

        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_and()

    def test_typecheck_and_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int(), t.Int()])
        with self.assertRaises(E.OperandException):
            typechecker.typecheck_and()

    def test_typecheck_xor(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat(), t.Nat()])
        typechecker.typecheck_xor()
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

    def test_typecheck_xor_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_xor()

        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_xor()

    def test_typecheck_xor_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int(), t.Nat()])
        with self.assertRaises(E.OperandException):
            typechecker.typecheck_xor()

    def test_typecheck_not(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int(), t.Nat()])
        typechecker.typecheck_not()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack = Stack([t.Nat(), t.Int()])
        typechecker.typecheck_not()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_not_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_not()

        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_not()

    def test_typecheck_not_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat(), t.Nat()])
        with self.assertRaises(E.OperandException):
            typechecker.typecheck_not()

    def test_typecheck_compare(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int(), t.Int()])
        typechecker.typecheck_compare()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack = Stack([t.Nat(), t.Nat()])
        typechecker.typecheck_compare()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_compare_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_compare()

        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_compare()

    def test_typecheck_compare_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Nat(), t.Int()])
        with self.assertRaises(E.OperandException):
            typechecker.typecheck_compare()

class TestAddressTypeChecker(unittest.TestCase):
    def test_typecheck_compare(self):
        typechecker = AddressTypeChecker()
        typechecker.stack = Stack([t.Address(), t.Address()])
        typechecker.typecheck_compare()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_compare_stack_error(self):
        typechecker = AddressTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_compare()

        typechecker.stack = Stack([t.String()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_compare()

    def test_typecheck_compare_type_error(self):
        typechecker = AddressTypeChecker()
        typechecker.stack = Stack([t.String(), t.Nat()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_compare()


class TestBooleanTypeChecker(unittest.TestCase):
    def test_typecheck_not(self):
        typechecker = BooleanTypeChecker()
        typechecker.stack = Stack([t.Bool()])
        typechecker.typecheck_not()
        self.assertEqual(typechecker.stack, Stack([t.Bool()]))


class TestMutezTypeChecker(unittest.TestCase):
    def test_typecheck_compare(self):
        typechecker = MutezTypeChecker()
        typechecker.stack = Stack([t.Mutez(), t.Mutez()])
        typechecker.typecheck_compare()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_compare_stack_error(self):
        typechecker = MutezTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_compare()

        typechecker.stack = Stack([t.String()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_compare()

    def test_typecheck_compare_type_error(self):
        typechecker = MutezTypeChecker()
        typechecker.stack = Stack([t.String(), t.Nat()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_compare()


class TestStringTypeChecker(unittest.TestCase):
    def test_typecheck_concat(self):
        typechecker = StringTypeChecker()
        typechecker.stack = Stack([t.String(), t.String()])
        typechecker.typecheck_concat()
        self.assertEqual(typechecker.stack, Stack([t.String()]))

    def test_typecheck_concat_stack_error(self):
        typechecker = StringTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_concat()

        typechecker.stack = Stack([t.Address()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_concat()

    def test_typecheck_concat_type_error(self):
        typechecker = StringTypeChecker()
        typechecker.stack = Stack([t.Address(), t.Int()])
        with self.assertRaises(E.OperandException):
            typechecker.typecheck_concat()

    def test_typecheck_size(self):
        typechecker = StringTypeChecker()
        typechecker.stack = Stack([t.String()])
        typechecker.typecheck_size()
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

    def test_typecheck_size_stack_error(self):
        typechecker = StringTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_size()

    def test_typecheck_size_type_error(self):
        typechecker = StringTypeChecker()
        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(E.OperandException):
            typechecker.typecheck_size()

    def test_typecheck_slice(self):
        typechecker = StringTypeChecker()
        typechecker.stack = Stack([t.String(), t.Nat(), t.Nat()])
        typechecker.typecheck_slice()
        self.assertEqual(typechecker.stack, Stack([t.Option(t.String())]))

    def test_typecheck_slice_stack_error(self):
        typechecker = StringTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_slice()

        typechecker.stack = Stack([t.String()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_slice()

        typechecker.stack = Stack([t.String(), t.Nat()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_slice()

    def test_typecheck_slice_type_error(self):
        typechecker = StringTypeChecker()
        typechecker.stack = Stack([t.String(), t.Nat(), t.Int()])
        with self.assertRaises(E.OperandException):
            typechecker.typecheck_slice()

    def test_typecheck_compare(self):
        typechecker = StringTypeChecker()
        typechecker.stack = Stack([t.String(), t.String()])
        typechecker.typecheck_compare()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_compare_stack_error(self):
        typechecker = StringTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_compare()

        typechecker.stack = Stack([t.String()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_compare()

    def test_typecheck_compare_type_error(self):
        typechecker = StringTypeChecker()
        typechecker.stack = Stack([t.String(), t.Nat()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_compare()


class TestPair(unittest.TestCase):
    def test_typecheck_pair(self):
        typechecker = PairTypeChecker()
        typechecker.stack = Stack([t.String(), t.Nat()])
        typechecker.typecheck_pair()
        self.assertEqual(typechecker.stack, Stack([t.Pair(t.Nat(), t.String())]))

    def test_typecheck_compare_stack_error(self):
        typechecker = PairTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_pair()

        typechecker.stack = Stack([t.String()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_pair()

    def test_typecheck_upair(self):
        typechecker = PairTypeChecker()
        typechecker.stack = Stack([t.Pair(t.String(), t.Nat())])
        typechecker.typecheck_unpair()
        self.assertEqual(typechecker.stack, Stack([t.Nat(), t.String()]))

    def test_typecheck_compare_stack_error(self):
        typechecker = PairTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_unpair()

    def test_typecheck_compare_type_error(self):
        typechecker = PairTypeChecker()
        typechecker.stack = Stack([t.String()])
        with self.assertRaises(E.TypeException):
            typechecker.typecheck_unpair()

    def test_typecheck_pair_n(self):
        typechecker = PairTypeChecker()

        typechecker.stack = Stack([t.String(), t.Nat()])
        typechecker.typecheck_pair_n(2)
        self.assertEqual(typechecker.stack, Stack([t.Pair(t.Nat(), t.String())]))

        typechecker.stack = Stack([t.String(), t.Nat(), t.Int()])
        typechecker.typecheck_pair_n(3)
        self.assertEqual(typechecker.stack, Stack([t.Pair(t.Int(), t.Pair(t.Nat(), t.String()))]))

        typechecker.stack = Stack([t.String(), t.Nat(), t.Int(), t.Address()])
        typechecker.typecheck_pair_n(4)
        typechecker.stack.get_at_depth(0)
        self.assertEqual(typechecker.stack, Stack([t.Pair(t.Address(), t.Pair(t.Int(), t.Pair(t.Nat(), t.String())))]))

    def test_typecheck_pair_n_stack_error(self):
        typechecker = PairTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_pair_n(2)

        typechecker.stack = Stack([t.String()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_pair_n(2)

    def test_typecheck_pair_n_invalid_michelson_error(self):
        typechecker = PairTypeChecker()
        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_pair_n(0)

        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_pair_n(1)

    def test_typecheck_car(self):
        typechecker = PairTypeChecker()
        typechecker.stack = Stack([t.Pair(t.String(), t.Int())])
        typechecker.typecheck_car()
        self.assertEqual(typechecker.stack, Stack([t.String()]))

    def test_typecheck_car_stack_error(self):
        typechecker = PairTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_car()

    def test_typecheck_car_type_error(self):
        typechecker = PairTypeChecker()
        typechecker.stack = Stack([t.String()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_car()

    def test_typecheck_cdr(self):
        typechecker = PairTypeChecker()
        typechecker.stack = Stack([t.Pair(t.String(), t.Int())])
        typechecker.typecheck_cdr()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_cdr_stack_error(self):
        typechecker = PairTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_cdr()

    def test_typecheck_cdr_type_error(self):
        typechecker = PairTypeChecker()
        typechecker.stack = Stack([t.String()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_cdr()

    def test_typecheck_get_n(self):
        typechecker = PairTypeChecker()
        pair = t.Pair.pair_n([t.Int(), t.Nat(), t.Address(), t.Bool()])

        typechecker.stack = Stack([pair])
        typechecker.typecheck_get_n(0)
        self.assertEqual(typechecker.stack, Stack([pair]))

        typechecker.stack = Stack([pair])
        typechecker.typecheck_get_n(1)
        self.assertEqual(typechecker.stack, Stack([pair.car]))

        typechecker.stack = Stack([pair])
        typechecker.typecheck_get_n(2)
        self.assertEqual(typechecker.stack, Stack([pair.cdr]))

        typechecker.stack = Stack([pair])
        typechecker.typecheck_get_n(3)
        self.assertEqual(typechecker.stack, Stack([pair.cdr.car]))

        typechecker.stack = Stack([pair])
        typechecker.typecheck_get_n(4)
        self.assertEqual(typechecker.stack, Stack([pair.cdr.cdr]))

        typechecker.stack = Stack([pair])
        typechecker.typecheck_get_n(5)
        self.assertEqual(typechecker.stack, Stack([pair.cdr.cdr.car]))

        typechecker.stack = Stack([pair])
        typechecker.typecheck_get_n(6)
        self.assertEqual(typechecker.stack, Stack([pair.cdr.cdr.cdr]))

    def test_typecheck_get_n_stack_error(self):
        typechecker = PairTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_cdr()

    def test_typecheck_get_n_type_error(self):
        typechecker = PairTypeChecker()
        typechecker.stack = Stack([t.String()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_cdr()

    def test_typecheck_get_n_too_large(self):
        typechecker = PairTypeChecker()
        pair = t.Pair.pair_n([t.Int(), t.Nat(), t.Address()])

        typechecker.stack = Stack([pair])
        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_get_n(5)

        typechecker.stack = Stack([pair])
        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_get_n(6)

    def test_typecheck_update_n(self):
        typechecker = PairTypeChecker()
        pair = t.Pair.pair_n([t.Int(), t.Nat(), t.Address(), t.Bool()])

        typechecker.stack = Stack([pair, t.Mutez()])
        typechecker.typecheck_update_n(0)
        self.assertEqual(typechecker.stack, Stack([t.Mutez()]))

        typechecker.stack = Stack([pair, t.Mutez()])
        typechecker.typecheck_update_n(1)
        expected_pair = t.Pair.pair_n([t.Mutez(), t.Nat(), t.Address(), t.Bool()])
        self.assertEqual(typechecker.stack, Stack([expected_pair]))

        typechecker.stack = Stack([pair, t.Mutez()])
        typechecker.typecheck_update_n(2)
        expected_pair = t.Pair.pair_n([t.Int(), t.Mutez()])
        typechecker.stack.get_at_depth(0)
        self.assertEqual(typechecker.stack, Stack([expected_pair]))

        typechecker.stack = Stack([pair, t.Mutez()])
        typechecker.typecheck_update_n(3)
        expected_pair = t.Pair.pair_n([t.Int(), t.Mutez(), t.Address(), t.Bool()])
        typechecker.stack.get_at_depth(0)
        self.assertEqual(typechecker.stack, Stack([expected_pair]))

        typechecker.stack = Stack([pair, t.Mutez()])
        typechecker.typecheck_update_n(4)
        expected_pair = t.Pair.pair_n([t.Int(), t.Nat(), t.Mutez()])
        typechecker.stack.get_at_depth(0)
        self.assertEqual(typechecker.stack, Stack([expected_pair]))

        typechecker.stack = Stack([pair, t.Mutez()])
        typechecker.typecheck_update_n(5)
        expected_pair = t.Pair.pair_n([t.Int(), t.Nat(), t.Mutez(), t.Bool()])
        typechecker.stack.get_at_depth(0)
        self.assertEqual(typechecker.stack, Stack([expected_pair]))

        typechecker.stack = Stack([pair, t.Mutez()])
        typechecker.typecheck_update_n(6)
        expected_pair = t.Pair.pair_n([t.Int(), t.Nat(), t.Address(), t.Mutez()])
        typechecker.stack.get_at_depth(0)
        self.assertEqual(typechecker.stack, Stack([expected_pair]))

    def test_typecheck_update_n_too_large(self):
        typechecker = PairTypeChecker()
        pair = t.Pair.pair_n([t.Int(), t.Nat(), t.Address(), t.Bool()])

        typechecker.stack = Stack([pair, t.Mutez()])
        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_update_n(7)

        typechecker.stack = Stack([pair, t.Mutez()])
        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_update_n(8)

        typechecker.stack = Stack([pair, t.Mutez()])
        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_update_n(9)

    def test_typecheck_update_n_stack_error(self):
        typechecker = PairTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_update_n(2)

        typechecker.stack = Stack([t.String()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_update_n(2)

    def test_typecheck_update_n_type_error(self):
        typechecker = PairTypeChecker()
        typechecker.stack = Stack([t.String(), t.String()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_update_n(2)

class TestAdd(unittest.TestCase):
    def test_typecheck_add(self):
        typechecker = IntOrNatTypeChecker()

        typechecker.stack = Stack([t.Int(), t.Int()])
        typechecker.typecheck_add()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack = Stack([t.Int(), t.Nat()])
        typechecker.typecheck_add()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack = Stack([t.Nat(), t.Int()])
        typechecker.typecheck_add()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack = Stack([t.Nat(), t.Nat()])
        typechecker.typecheck_add()
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

    def test_typecheck_add_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_add()

    def test_typecheck_add_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int(), t.Address()])
        with self.assertRaises(E.OperandException):
            typechecker.typecheck_add()


class TestSub(unittest.TestCase):
    def test_typecheck_sub(self):
        typechecker = IntOrNatTypeChecker()

        typechecker.stack = Stack([t.Int(), t.Int()])
        typechecker.typecheck_sub()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack = Stack([t.Int(), t.Nat()])
        typechecker.typecheck_sub()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack = Stack([t.Nat(), t.Int()])
        typechecker.typecheck_sub()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack = Stack([t.Nat(), t.Nat()])
        typechecker.typecheck_sub()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_sub_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_sub()

    def test_typecheck_sub_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int(), t.Address()])
        with self.assertRaises(E.OperandException):
            typechecker.typecheck_sub()


class TestMul(unittest.TestCase):
    def test_typecheck_mul(self):
        typechecker = IntOrNatTypeChecker()

        typechecker.stack = Stack([t.Int(), t.Int()])
        typechecker.typecheck_mul()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack = Stack([t.Nat(), t.Int()])
        typechecker.typecheck_mul()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack = Stack([t.Int(), t.Nat()])
        typechecker.typecheck_mul()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack = Stack([t.Nat(), t.Nat()])
        typechecker.typecheck_mul()
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

    def test_typecheck_mul_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_mul()

    def test_typecheck_mul_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int(), t.Address()])
        with self.assertRaises(E.OperandException):
            typechecker.typecheck_mul()


class TestEdiv(unittest.TestCase):
    def test_typecheck_ediv(self):
        typechecker = IntOrNatTypeChecker()

        typechecker.stack = Stack([t.Int(), t.Int()])
        typechecker.typecheck_ediv()
        self.assertEqual(typechecker.stack, Stack([t.Option(t.Pair(t.Int(), t.Nat()))]))

        typechecker.stack = Stack([t.Nat(), t.Int()])
        typechecker.typecheck_ediv()
        self.assertEqual(typechecker.stack, Stack([t.Option(t.Pair(t.Int(), t.Nat()))]))

        typechecker.stack = Stack([t.Int(), t.Nat()])
        typechecker.typecheck_ediv()
        self.assertEqual(typechecker.stack, Stack([t.Option(t.Pair(t.Int(), t.Nat()))]))

        typechecker.stack = Stack([t.Nat(), t.Nat()])
        typechecker.typecheck_ediv()
        self.assertEqual(typechecker.stack, Stack([t.Option(t.Pair(t.Nat(), t.Nat()))]))

    def test_typecheck_ediv_stack_error(self):
        typechecker = IntOrNatTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_ediv()

    def test_typecheck_ediv_type_error(self):
        typechecker = IntOrNatTypeChecker()
        typechecker.stack = Stack([t.Int(), t.Address()])
        with self.assertRaises(E.OperandException):
            typechecker.typecheck_ediv()


class TestCryptographicPrimitives(unittest.TestCase):
    def test_typecheck_blake2b(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        typechecker.stack = Stack([t.Bytes()])
        typechecker.typecheck_blake2b()
        self.assertEqual(typechecker.stack, Stack([t.Bytes()]))

    def test_typecheck_blake2b_stack_error(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_blake2b()

    def test_typecheck_blake2b_type_error(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        typechecker.stack = Stack([t.Address()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_blake2b()

    def test_typecheck_keccak(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        typechecker.stack = Stack([t.Bytes()])
        typechecker.typecheck_keccak()
        self.assertEqual(typechecker.stack, Stack([t.Bytes()]))

    def test_typecheck_keccak_stack_error(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_keccak()

    def test_typecheck_keccak_type_error(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        typechecker.stack = Stack([t.Address()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_keccak()

    def test_typecheck_sha256(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        typechecker.stack = Stack([t.Bytes()])
        typechecker.typecheck_sha256()
        self.assertEqual(typechecker.stack, Stack([t.Bytes()]))

    def test_typecheck_sha256_stack_error(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_sha256()

    def test_typecheck_sha256_type_error(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        typechecker.stack = Stack([t.Address()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_sha256()

    def test_typecheck_sha512(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        typechecker.stack = Stack([t.Bytes()])
        typechecker.typecheck_sha512()
        self.assertEqual(typechecker.stack, Stack([t.Bytes()]))

    def test_typecheck_sha512_stack_error(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_sha512()

    def test_typecheck_sha512_type_error(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        typechecker.stack = Stack([t.Address()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_sha512()

    def test_typecheck_sha3(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        typechecker.stack = Stack([t.Bytes()])
        typechecker.typecheck_sha3()
        self.assertEqual(typechecker.stack, Stack([t.Bytes()]))

    def test_typecheck_sha3_stack_error(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_sha3()

    def test_typecheck_sha3_type_error(self):
        typechecker = CryptographicPrimitivesTypeChecker()
        typechecker.stack = Stack([t.Address()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_sha3()

class TestBytes(unittest.TestCase):
    def test_typecheck_pack(self):
        typechecker = BytesTypeChecker()
        typechecker.stack = Stack([t.Int()])
        typechecker.typecheck_pack()
        self.assertEqual(typechecker.stack, Stack([t.Bytes()]))

    def test_typecheck_pack_stack_error(self):
        typechecker = BytesTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_pack()

    def test_typecheck_unpack(self):
        typechecker = BytesTypeChecker()
        typechecker.stack = Stack([t.Bytes()])
        typechecker.typecheck_unpack(Instr("UNPACK", [t.Int()], {}))
        self.assertEqual(typechecker.stack, Stack([t.Option(t.Int())]))

    def test_typecheck_unpack_stack_error(self):
        typechecker = BytesTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_unpack(Instr("UNPACK", [t.Int()], {}))

    def test_typecheck_unpack_invalid_michelson_error(self):
        typechecker = BytesTypeChecker()
        typechecker.stack = Stack([t.Bytes()])

        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_unpack(Instr("UNPACK", [], {}))

        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_unpack(Instr("UNPACK", [10], {}))

    def test_typecheck_compare(self):
        typechecker = BytesTypeChecker()
        typechecker.stack = Stack([t.Bytes(), t.Bytes()])
        typechecker.typecheck_compare()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_compare_stack_error(self):
        typechecker = BytesTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_compare()

        typechecker.stack = Stack([t.Bytes()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_compare()

    def test_typecheck_compare_type_error(self):
        typechecker = BytesTypeChecker()
        typechecker.stack = Stack([t.Bytes(), t.Nat()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_compare()

    def test_typecheck_concat(self):
        typechecker = BytesTypeChecker()
        typechecker.stack = Stack([t.Bytes(), t.Bytes()])
        typechecker.typecheck_concat()
        self.assertEqual(typechecker.stack, Stack([t.Bytes()]))

    def test_typecheck_concat_stack_error(self):
        typechecker = BytesTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_concat()

        typechecker.stack = Stack([t.Bytes()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_concat()

    def test_typecheck_concat_type_error(self):
        typechecker = BytesTypeChecker()
        typechecker.stack = Stack([t.Bytes(), t.Nat()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_concat()

    def test_typecheck_size(self):
        typechecker = BytesTypeChecker()
        typechecker.stack = Stack([t.Bytes()])
        typechecker.typecheck_size()
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

    def test_typecheck_size_stack_error(self):
        typechecker = BytesTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_size()

    def test_typecheck_size_type_error(self):
        typechecker = BytesTypeChecker()
        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_size()

    def test_typecheck_slice(self):
        typechecker = BytesTypeChecker()
        typechecker.stack = Stack([t.Bytes(), t.Nat(), t.Nat()])
        typechecker.typecheck_slice()
        self.assertEqual(typechecker.stack, Stack([t.Option(t.Bytes())]))

    def test_typecheck_slice_stack_error(self):
        typechecker = BytesTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_slice()

        typechecker.stack = Stack([t.Bytes()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_slice()

        typechecker.stack = Stack([t.Bytes(), t.Nat()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_slice()

    def test_typecheck_slice_type_error(self):
        typechecker = BytesTypeChecker()
        typechecker.stack = Stack([t.Bytes(), t.Nat(), t.Int()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_slice()


class TestUnion(unittest.TestCase):
    def test_typecheck_left(self):
        typechecker = UnionTypeChecker()
        typechecker.stack = Stack([t.Int()])
        typechecker.typecheck_left(Instr("LEFT", [t.Bool()], {}))
        self.assertEqual(typechecker.stack, Stack([t.Or(t.Int(), t.Bool())]))

    def test_typecheck_left_stack_error(self):
        typechecker = UnionTypeChecker()
        typechecker.stack = Stack([])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_left(Instr("LEFT", [t.Bool()], {}))

    def test_typecheck_left_invalid_michelson_error(self):
        typechecker = UnionTypeChecker()
        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_left(Instr("LEFT", [], {}))

    def test_typecheck_right(self):
        typechecker = UnionTypeChecker()
        typechecker.stack = Stack([t.Int()])
        typechecker.typecheck_right(Instr("RIGHT", [t.Bool()], {}))
        self.assertEqual(typechecker.stack, Stack([t.Or(t.Bool(), t.Int())]))

    def test_typecheck_right_stack_error(self):
        typechecker = UnionTypeChecker()
        typechecker.stack = Stack([])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_right(Instr("RIGHT", [t.Bool()], {}))

    def test_typecheck_right_invalid_michelson_error(self):
        typechecker = UnionTypeChecker()
        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_right(Instr("RIGHT", [], {}))

    def test_typecheck_if_left(self):
        typechecker = UnionTypeChecker()

        typechecker.stack = Stack([t.Or(t.Nat(), t.Int())])
        typechecker.typecheck_if_left(
            Instr("IF_LEFT", [
                [
                    Instr("PUSH", [t.Int(), 2], {}),
                    Instr("SUB", [], {}),
                ],
                [
                    Instr("PUSH", [t.Int(), 6], {}),
                    Instr("ADD", [], {}),
                ],
            ], {}),
        )
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_if_left_type_error(self):
        typechecker = UnionTypeChecker()

        typechecker.stack = Stack([t.Or(t.Int(), t.Nat())])
        with self.assertRaises(E.ConditionBranchesTypeMismatch):
            typechecker.typecheck_if_left(
                Instr("IF_LEFT", [
                    [
                        Instr("PUSH", [t.Int(), 2], {}),
                        Instr("ADD", [], {}),
                    ],
                    [
                        Instr("PUSH", [t.Nat(), 5], {}),
                        Instr("ADD", [], {}),
                    ],
                ], {}),
            )

        typechecker.stack = Stack([t.Or(t.Int(), t.Nat())])
        with self.assertRaises(E.ConditionBranchesTypeMismatch):
            typechecker.typecheck_if_left(
                Instr("IF_LEFT", [
                    [
                    ],
                    [
                    ],
                ], {}),
            )

        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_if_left(
                Instr("IF_LEFT", [
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                        Instr("PUSH", [t.Int(), 2], {}),
                    ],
                    [
                        Instr("PUSH", [t.Int(), 3], {}),
                        Instr("PUSH", [t.Int(), 4], {}),
                    ],
                ], {}),
            )

    def test_typecheck_if_left_stack_error(self):
        typechecker = UnionTypeChecker()

        typechecker.stack = Stack([])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_if_left(
                Instr("IF", [
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                        Instr("PUSH", [t.Int(), 2], {}),
                    ],
                    [
                        Instr("PUSH", [t.Int(), 5], {}),
                    ],
                ], {}),
            )

class TestOption(unittest.TestCase):
    def test_typecheck_some(self):
        typechecker = OptionTypeChecker()
        typechecker.stack = Stack([t.Int()])
        typechecker.typecheck_some()
        self.assertEqual(typechecker.stack, Stack([t.Option(t.Int())]))

    def test_typecheck_some_stack_error(self):
        typechecker = OptionTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_some()

    def test_typecheck_none(self):
        typechecker = OptionTypeChecker()
        typechecker.stack = Stack([])
        typechecker.typecheck_none(Instr("NONE", [t.Int()], {}))
        self.assertEqual(typechecker.stack, Stack([t.Option(t.Int())]))

    def test_typecheck_invalid_michelson(self):
        typechecker = OptionTypeChecker()
        typechecker.stack = Stack([])
        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_none(Instr("NONE", [10], {}))

    def test_typecheck_compare(self):
        typechecker = OptionTypeChecker()
        typechecker.stack = Stack([t.Option(t.Int()), t.Option(t.Mutez())])
        typechecker.typecheck_compare()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_compare_stack_error(self):
        typechecker = OptionTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_compare()

        typechecker.stack = Stack([t.String()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_compare()

    def test_typecheck_compare_type_error(self):
        typechecker = OptionTypeChecker()
        typechecker.stack = Stack([t.String(), t.Nat()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_compare()

    def test_typecheck_if_none(self):
        typechecker = OptionTypeChecker()

        typechecker.stack = Stack([t.Option(t.Int())])
        typechecker.typecheck_if_none(
            Instr("IF_NONE", [
                [
                    Instr("PUSH", [t.Int(), 1], {}),
                    Instr("PUSH", [t.Int(), 2], {}),
                    Instr("ADD", [], {}),
                ],
                [
                    Instr("PUSH", [t.Int(), 6], {}),
                    Instr("SUB", [], {}),
                ],
            ], {}),
        )
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_if_none_type_error(self):
        typechecker = OptionTypeChecker()

        typechecker.stack = Stack([t.Option(t.Int())])
        with self.assertRaises(E.ConditionBranchesTypeMismatch):
            typechecker.typecheck_if_none(
                Instr("IF_NONE", [
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                        Instr("PUSH", [t.Int(), 2], {}),
                    ],
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                        Instr("PUSH", [t.Int(), 5], {}),
                    ],
                ], {}),
            )

        typechecker.stack = Stack([t.Option(t.Int())])
        with self.assertRaises(E.ConditionBranchesTypeMismatch):
            typechecker.typecheck_if_none(
                Instr("IF_NONE", [
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                    ],
                    [
                        Instr("PUSH", [t.Int(), 5], {}),
                        Instr("PUSH", [t.Int(), 2], {}),
                    ],
                ], {}),
            )

        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_if_none(
                Instr("IF_NONE", [
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                        Instr("PUSH", [t.Int(), 2], {}),
                    ],
                    [
                        Instr("PUSH", [t.Int(), 3], {}),
                        Instr("PUSH", [t.Int(), 4], {}),
                    ],
                ], {}),
            )

    def test_typecheck_if_none_stack_error(self):
        typechecker = OptionTypeChecker()

        typechecker.stack = Stack([])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_if_none(
                Instr("IF_NONE", [
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                        Instr("PUSH", [t.Int(), 2], {}),
                    ],
                    [
                        Instr("PUSH", [t.Int(), 5], {}),
                    ],
                ], {}),
            )

    def test_integration(self):
        source = """
total_supply = Nat(2)
amount = Nat(3)
total_supply = (total_supply - amount).is_nat().get_with_default(Nat(10))
        """
        micheline = Compiler(source).compile_expression_object()
        typechecker = MichelsonTypeChecker(instructions=micheline)
        typechecker.typecheck()


class TestSet(unittest.TestCase):
    def test_new_set(self):
        source = """
Set[Nat]()
        """
        micheline = Compiler(source).compile_expression_object()
        typechecker = MichelsonTypeChecker(instructions=micheline)
        typechecker.typecheck()
        assert len(typechecker.stack) == 1
        assert typechecker.stack.get_at_depth(0) == t.Set(t.Nat())

    def test_el_in_set(self):
        source = """
Nat(1) in Set[Nat]()
        """
        micheline = Compiler(source).compile_expression_object()
        typechecker = MichelsonTypeChecker(instructions=micheline)
        typechecker.typecheck()
        assert len(typechecker.stack) == 2
        assert typechecker.stack.get_at_depth(0) == t.Bool()

    def test_el_not_in_set(self):
        source = """
Nat(1) not in Set[Nat]()
        """
        micheline = Compiler(source).compile_expression_object()
        typechecker = MichelsonTypeChecker(instructions=micheline)
        typechecker.typecheck()
        assert len(typechecker.stack) == 2
        assert typechecker.stack.get_at_depth(0) == t.Bool()

    def test_set_add_el(self):
        source = """
a = Set[Nat]()
a.add(Nat(1))
        """
        micheline = Compiler(source).compile_expression_object()
        typechecker = MichelsonTypeChecker(instructions=micheline)
        typechecker.typecheck()
        assert len(typechecker.stack) == 3
        assert typechecker.stack.get_at_depth(0) == t.Set(t.Nat())

    def test_set_remove_el(self):
        source = """
s = Set[Nat]()
s.remove(Nat(1))
        """
        micheline = Compiler(source).compile_expression_object()
        typechecker = MichelsonTypeChecker(instructions=micheline)
        typechecker.typecheck()
        assert len(typechecker.stack) == 3
        assert typechecker.stack.get_at_depth(0) == t.Set(t.Nat())

class TestList(unittest.TestCase):
    def test_typecheck_empty_list(self):
        typechecker = ListTypeChecker()
        typechecker.typecheck_empty_list(Instr("NIL", [t.Int()], {}))
        self.assertEqual(typechecker.stack, Stack([t.List(t.Int())]))

    def test_typecheck_empty_list_invalid_michelson(self):
        typechecker = ListTypeChecker()
        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_empty_list(Instr("NIL", [1], {}))

        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_empty_list(Instr("NIL", [t.Int(), t.Int()], {}))

    def test_typecheck_cons(self):
        typechecker = ListTypeChecker()
        typechecker.stack = Stack([t.List(t.Int()), t.Int()])
        typechecker.typecheck_cons()
        self.assertEqual(typechecker.stack, Stack([t.List(t.Int())]))

    def test_typecheck_const_wrong_stack_type(self):
        typechecker = ListTypeChecker()
        typechecker.stack = Stack([t.List(t.Int()), t.Nat()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_cons()

        typechecker.stack = Stack([t.Int(), t.Nat()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_cons()

    def test_typecheck_const_wrong_stack_length(self):
        typechecker = ListTypeChecker()
        typechecker.stack = Stack([t.List(t.Int())])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_cons()

    def test_typecheck_size(self):
        typechecker = ListTypeChecker()
        typechecker.stack = Stack([t.List(t.Int())])
        typechecker.typecheck_size()
        expected_stack = Stack([t.Nat()])
        self.assertEqual(typechecker.stack, expected_stack)

    def test_typecheck_size_stack_error(self):
        typechecker = ListTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_size()

    def test_typecheck_size_type_error(self):
        typechecker = MapTypeChecker()
        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_size()

    def test_typecheck_iter(self):
        typechecker = ListTypeChecker()
        typechecker.stack = Stack([t.Int(), t.List(t.Int())])
        instruction = Instr(
            "ITER",
            [[
                Instr("PUSH", [t.Int(), 1], {}),
                Instr("ADD", [], {}),
                Instr("DROP", [], {}),
            ]],
            {}
        )
        typechecker.typecheck_iter(instruction)
        expected_stack = Stack([t.Int()])
        self.assertEqual(typechecker.stack, expected_stack)

    def test_typecheck_iter_must_return_empty_stack(self):
        typechecker = ListTypeChecker()
        typechecker.stack = Stack([t.Int(), t.List(t.Int())])
        instruction = Instr(
            "ITER",
            [[
                Instr("PUSH", [t.Int(), 1], {}),
                Instr("ADD", [], {}),
            ]],
            {}
        )
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_iter(instruction)

    def test_typecheck_iter_not_a_list_error(self):
        typechecker = ListTypeChecker()
        typechecker.stack = Stack([t.Int()])
        instruction = Instr(
            "ITER",
            [[
                Instr("PUSH", [t.Int(), 1], {}),
                Instr("ADD", [], {}),
            ]],
            {}
        )
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_iter(instruction)

    def test_typecheck_iter_body_typecheck_error(self):
        typechecker = ListTypeChecker()
        typechecker.stack = Stack([t.Int()])
        instruction = Instr(
            "ITER",
            [[
                Instr("PUSH", [t.String(), "foo"], {}),
                Instr("ADD", [], {}),
            ]],
            {}
        )
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_iter(instruction)

class TestMap(unittest.TestCase):
    def test_typecheck_empty_map(self):
        typechecker = MapTypeChecker()
        typechecker.typecheck_empty_map(Instr("EMPTY_MAP", [t.Int(), t.String()], {}))
        self.assertEqual(typechecker.stack, Stack([t.Dict(t.Int(), t.String())]))

    def test_typecheck_get(self):
        typechecker = MapTypeChecker()
        typechecker.stack = Stack([t.Dict(t.Int(), t.String()), t.Int()])
        typechecker.typecheck_get_map()
        expected_stack = Stack([t.Option(t.String())])
        self.assertEqual(typechecker.stack, expected_stack)

    def test_typecheck_get_stack_error(self):
        typechecker = MapTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_get_map()

        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_get_map()

    def test_typecheck_get_type_error(self):
        typechecker = MapTypeChecker()
        typechecker.stack = Stack([t.Dict(t.String(), t.String()), t.Int()])
        with self.assertRaises(E.MapKeyTypeException):
            typechecker.typecheck_get_map()

    def test_typecheck_mem(self):
        typechecker = MapTypeChecker()
        typechecker.stack = Stack([t.Dict(t.Int(), t.String()), t.Int()])
        typechecker.typecheck_mem_map()
        expected_stack = Stack([t.Bool()])
        self.assertEqual(typechecker.stack, expected_stack)

    def test_typecheck_mem_stack_error(self):
        typechecker = MapTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_mem_map()

        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_mem_map()

    def test_typecheck_mem_type_error(self):
        typechecker = MapTypeChecker()
        typechecker.stack = Stack([t.Dict(t.String(), t.String()), t.Int()])
        with self.assertRaises(E.MapKeyTypeException):
            typechecker.typecheck_mem_map()

    def test_typecheck_update(self):
        typechecker = MapTypeChecker()
        typechecker.stack = Stack(
            [t.Dict(t.Int(), t.String()), t.Option(t.String()), t.Int()]
        )
        typechecker.typecheck_update_map()
        expected_stack = Stack([t.Dict(t.Int(), t.String())])
        self.assertEqual(typechecker.stack, expected_stack)

    def test_typecheck_update_stack_error(self):
        typechecker = MapTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_update_map()

        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_update_map()

        typechecker.stack = Stack([t.Int(), t.Int()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_update_map()

    def test_typecheck_update_type_error(self):
        typechecker = MapTypeChecker()

        typechecker.stack = Stack([t.Dict(t.String(), t.String()), t.Int(), t.String()])
        with self.assertRaises(E.MapValueTypeException):
            typechecker.typecheck_update_map()

        typechecker.stack = Stack([t.Dict(t.String(), t.String()), t.String(), t.Int()])
        with self.assertRaises(E.MapValueTypeException):
            typechecker.typecheck_update_map()

        typechecker.stack = Stack([t.Int(), t.String(), t.Int()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_update_map()

    def test_typecheck_size(self):
        typechecker = MapTypeChecker()
        typechecker.stack = Stack([t.Dict(t.Int(), t.String())])
        typechecker.typecheck_size()
        expected_stack = Stack([t.Nat()])
        self.assertEqual(typechecker.stack, expected_stack)

    def test_typecheck_size_stack_error(self):
        typechecker = MapTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_size()

    def test_typecheck_size_type_error(self):
        typechecker = MapTypeChecker()
        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_size()


## Control structures

class TestDig(unittest.TestCase):
    def test_typecheck_dig(self):
        make_stack = lambda: Stack([t.String(), t.Nat(), t.Nat()])
        typechecker = ControlStructuresTypeChecker()

        typechecker.stack = make_stack()
        typechecker.typecheck_dip(
            Instr("DIP", [0, [Instr("PACK", [], {})]], {}),
            MichelsonTypeChecker().typecheck
        )
        self.assertEqual(typechecker.stack, Stack([t.String(), t.Nat(), t.Bytes()]))

        typechecker.stack = make_stack()
        typechecker.typecheck_dip(
            Instr("DIP", [1, [Instr("PACK", [], {})]], {}),
            MichelsonTypeChecker().typecheck
        )
        self.assertEqual(typechecker.stack, Stack([t.String(), t.Bytes(), t.Nat()]))

        typechecker.stack = make_stack()
        typechecker.typecheck_dip(
            Instr("DIP", [2, [Instr("PACK", [], {})]], {}),
            MichelsonTypeChecker().typecheck
        )
        self.assertEqual(typechecker.stack, Stack([t.Bytes(), t.Nat(), t.Nat()]))

    def test_typecheck_stack_error(self):
        instructions = [
            Instr("PUSH", [t.String(), "bar"], {}),
            Instr("PUSH", [t.Nat(), 1], {}),
            Instr("PUSH", [t.Nat(), 3], {}),
            Instr("DIP", [3, [Instr("PACK", [], {})]], {}),
        ]
        typechecker = MichelsonTypeChecker(instructions=instructions)
        with self.assertRaises(StackLengthException):
            typechecker.typecheck()


class TestIf(unittest.TestCase):
    def test_typecheck_if(self):
        typechecker = ControlStructuresTypeChecker()

        typechecker.stack = Stack([t.Bool()])
        typechecker.typecheck_if(
            Instr("IF", [
                [
                    Instr("PUSH", [t.Int(), 1], {}),
                    Instr("PUSH", [t.Int(), 2], {}),
                    Instr("ADD", [], {}),
                ],
                [
                    Instr("PUSH", [t.Int(), 5], {}),
                    Instr("PUSH", [t.Int(), 6], {}),
                    Instr("SUB", [], {}),
                ],
            ], {}),
        )
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_if_type_error(self):
        typechecker = ControlStructuresTypeChecker()

        typechecker.stack = Stack([t.Bool()])
        with self.assertRaises(E.ConditionBranchesTypeMismatch):
            typechecker.typecheck_if(
                Instr("IF", [
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                        Instr("PUSH", [t.Int(), 2], {}),
                    ],
                    [
                        Instr("PUSH", [t.Int(), 5], {}),
                    ],
                ], {}),
            )

        typechecker.stack = Stack([t.Bool()])
        with self.assertRaises(E.ConditionBranchesTypeMismatch):
            typechecker.typecheck_if(
                Instr("IF", [
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                    ],
                    [
                        Instr("PUSH", [t.Int(), 5], {}),
                        Instr("PUSH", [t.Int(), 2], {}),
                    ],
                ], {}),
            )

        typechecker.stack = Stack([t.Int()])
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_if(
                Instr("IF", [
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                        Instr("PUSH", [t.Int(), 2], {}),
                    ],
                    [
                        Instr("PUSH", [t.Int(), 3], {}),
                        Instr("PUSH", [t.Int(), 4], {}),
                    ],
                ], {}),
            )

    def test_typecheck_if_stack_error(self):
        typechecker = ControlStructuresTypeChecker()

        typechecker.stack = Stack([])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_if(
                Instr("IF", [
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                        Instr("PUSH", [t.Int(), 2], {}),
                    ],
                    [
                        Instr("PUSH", [t.Int(), 5], {}),
                    ],
                ], {}),
            )

## Stack Manipulations

class TestPush(unittest.TestCase):
    def test_typecheck_push(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.typecheck_push(Instr("PUSH", [t.Int(), 10], {}))
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

        typechecker.stack.reset()
        typechecker.typecheck_push(Instr("PUSH", [t.Nat(), 10], {}))
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

        typechecker.stack.reset()
        typechecker.typecheck_push(Instr("PUSH", [t.Mutez(), 10], {}))
        self.assertEqual(typechecker.stack, Stack([t.Mutez()]))

        typechecker.stack.reset()
        typechecker.typecheck_push(Instr("PUSH", [t.String(), "foobar"], {}))
        self.assertEqual(typechecker.stack, Stack([t.String()]))

        typechecker.stack.reset()
        typechecker.typecheck_push(Instr("PUSH", [t.Address(), "foobar"], {}))
        self.assertEqual(typechecker.stack, Stack([t.Address()]))


    def test_typecheck_push_type_error(self):
        typechecker = StackManipulationTypeChecker()

        with self.assertRaises(E.PushTypeException):
            typechecker.typecheck_push(Instr("PUSH", [t.Int(), "foobar"], {}))

        with self.assertRaises(E.PushTypeException):
            typechecker.typecheck_push(Instr("PUSH", [t.Nat(), "foobar"], {}))

        with self.assertRaises(E.PushTypeException):
            typechecker.typecheck_push(Instr("PUSH", [t.Mutez(), "foobar"], {}))

        with self.assertRaises(E.PushTypeException):
            typechecker.typecheck_push(Instr("PUSH", [t.String(), 1], {}))


class TestDrop(unittest.TestCase):
    def test_typecheck_drop(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int()])
        typechecker.typecheck_drop()
        self.assertEqual(typechecker.stack, Stack([]))

    def test_typecheck_drop_stack_length_error(self):
        typechecker = StackManipulationTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_dup(Instr("DROP", [], {}))


class TestDup(unittest.TestCase):
    def test_typecheck_dup(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int()])
        typechecker.typecheck_dup(Instr("DUP", [], {}))
        self.assertEqual(typechecker.stack, Stack([t.Int(), t.Int()]))

    def test_empty_stack(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_dup(Instr("DUP", [], {}))

    def test_typecheck_dup_1(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int()])
        typechecker.typecheck_dup(Instr("DUP", [1], {}))
        self.assertEqual(typechecker.stack, Stack([t.Int(), t.Int()]))

    def test_typecheck_dup_2(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int(), t.String()])
        typechecker.typecheck_dup(Instr("DUP", [2], {}))
        self.assertEqual(typechecker.stack, Stack([t.Int(), t.String(), t.Int()]))

    def test_typecheck_dup_n_stack_too_small(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int(), t.String()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_dup(Instr("DUP", [3], {}))

    def test_typecheck_dup_invalid_michelson(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int(), t.String()])

        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_dup(Instr("DUP", ["my_string"], {}))

        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_dup(Instr("DUP", [0], {}))


class TestDug(unittest.TestCase):
    def test_typecheck_dug_0(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.String(), t.Int()])
        typechecker.typecheck_dug(Instr("DUG", [0], {}))
        self.assertEqual(typechecker.stack, Stack([t.String(), t.Int()]))

    def test_typecheck_dug(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.String(), t.Int()])
        typechecker.typecheck_dug(Instr("DUG", [1], {}))
        self.assertEqual(typechecker.stack, Stack([t.Int(), t.String()]))

    def test_typecheck_dug_2(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int(), t.String(), t.Nat()])
        typechecker.typecheck_dug(Instr("DUG", [2], {}))
        self.assertEqual(typechecker.stack, Stack([t.Nat(), t.Int(), t.String()]))

    def test_empty_stack(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_dug(Instr("DUG", [1], {}))

    def test_typecheck_dig_n_stack_too_small(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int(), t.String()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_dug(Instr("DUG", [3], {}))

    def test_typecheck_dug_invalid_michelson(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int(), t.String()])

        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_dug(Instr("DUG", ["my_string"], {}))

        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_dug(Instr("DUG", [], {}))


class TestFailwith(unittest.TestCase):
    def test_typecheck_if_failwith(self):
        typechecker = ControlStructuresTypeChecker()

        typechecker.stack = Stack([t.Bool()])
        typechecker.typecheck_if(
                Instr("IF", [
                        [
                                Instr("PUSH", [t.String(), "error"], {}),
                                Instr("FAILWITH", [], {}),
                        ],
                        [
                                Instr("PUSH", [t.Int(), 5], {}),
                        ],
                ], {}),
        )

        assert typechecker.stack == Stack([t.Int()])

        typechecker.stack = Stack([t.Bool()])
        typechecker.typecheck_if(
                Instr("IF", [
                        [
                                Instr("FAILWITH", [], {}),
                        ],
                        [
                        ],
                ], {}),
        )
        typechecker.stack = Stack([])


class TestDig(unittest.TestCase):
    def test_typecheck_dig(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int(), t.String()])
        typechecker.typecheck_dig(Instr("DIG", [1], {}))
        self.assertEqual(typechecker.stack, Stack([t.String(), t.Int()]))

    def test_empty_stack(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_dig(Instr("DIG", [1], {}))

    def test_typecheck_dig_2(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int(), t.String(), t.Int()])
        typechecker.typecheck_dig(Instr("DIG", [2], {}))
        self.assertEqual(typechecker.stack, Stack([t.String(), t.Int(), t.Int()]))

    def test_typecheck_dig_n_stack_too_small(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int(), t.String()])
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_dig(Instr("DIG", [3], {}))

    def test_typecheck_dup_invalid_michelson(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack([t.Int(), t.String()])

        with self.assertRaises(InvalidMichelsonException):
            typechecker.typecheck_dig(Instr("DIG", ["my_string"], {}))


class TestLambda(unittest.TestCase):
    def test_typecheck_lambda(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack()
        lambda_instr = Instr(
            "LAMBDA",
            [
                t.String(),
                t.Nat(),
                [
                    Instr("SIZE", [], {}),
                ]
            ],
            {}
        )
        typechecker.typecheck_lambda(lambda_instr)
        self.assertEqual(
            typechecker.stack,
            Stack([t.FunctionPrototype(t.String(), t.Nat())]),
        )

    def test_typecheck_lambda_wrong_return_type(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack()
        lambda_instr = Instr(
            "LAMBDA",
            [
                t.String(),
                t.Nat(),
                [
                    Instr("PUSH", [t.String(), "hello"], {}),
                    Instr("CONCAT", [], {}),
                ]
            ],
            {}
        )
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck_lambda(lambda_instr)

    def test_typecheck_lambda_body_does_not_typecheck(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack()
        lambda_instr = Instr(
            "LAMBDA",
            [
                t.String(),
                t.Nat(),
                [
                    Instr("PUSH", [t.Nat(), 1], {}),
                    Instr("ADD", [t.Nat(), 1], {}),
                ]
            ],
            {}
        )

        with self.assertRaises(E.OperandException):
            typechecker.typecheck_lambda(lambda_instr)

    def test_typecheck_lambda_wrong_stack_length(self):
        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack()
        lambda_instr = Instr(
            "LAMBDA",
            [
                t.String(),
                t.Nat(),
                [
                    Instr("PUSH", [t.Nat(), 1], {}),
                ]
            ],
            {}
        )

        with self.assertRaises(StackLengthException):
            typechecker.typecheck_lambda(lambda_instr)

        typechecker = StackManipulationTypeChecker()
        typechecker.stack = Stack()
        lambda_instr = Instr(
            "LAMBDA",
            [
                t.String(),
                t.Nat(),
                [
                    Instr("DROP", [], {}),
                ]
            ],
            {}
        )

        with self.assertRaises(StackLengthException):
            typechecker.typecheck_lambda(lambda_instr)

    def test_typecheck_exec(self):
        typechecker = ControlStructuresTypeChecker()
        typechecker.stack = Stack([
            t.FunctionPrototype(t.Int(), t.Nat()),
            t.Int(),
        ])
        instruction = Instr("EXEC", [], {})
        typechecker.typecheck_exec(instruction)
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

    def test_typecheck_exec_wrong_argument_type(self):
        typechecker = ControlStructuresTypeChecker()
        typechecker.stack = Stack([
            t.FunctionPrototype(t.Int(), t.Nat()),
            t.Nat(),
        ])
        instruction = Instr("EXEC", [], {})
        with self.assertRaises(E.LambdaArgumentTypeException):
            typechecker.typecheck_exec(instruction)

    def test_typecheck_exec_wrong_stack_length(self):
        typechecker = ControlStructuresTypeChecker()
        typechecker.stack = Stack([
            t.FunctionPrototype(t.Int(), t.Nat()),
        ])
        instruction = Instr("EXEC", [], {})
        with  self.assertRaises(StackLengthException):
            typechecker.typecheck_exec(instruction)


class TestIntegration(unittest.TestCase):
    def test_load_typechecker(self):
        instructions = [
            Instr("PUSH", [t.Int(), 10], {}),
            Instr("DUP", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Int(), t.Int()]))


    def test_typecheck_contract(self):
        micheline = [
            {
                "prim": "parameter",
                "args": [t.Int()],
            },
            {
                "prim": "storage",
                "args": [t.Int()],
            },
            {
                "prim": "code",
                "args": [
                    Instr("UNPAIR", [], {}),
                    Instr("ADD", [], {}),
                    Instr("NIL", [t.Operation()], {}),
                    Instr("PAIR", [], {}),
                ],
            },
        ]
        typechecker = MichelsonTypeChecker()
        typechecker.typecheck(micheline)

    def skip_test_typecheck_contract_wrong_return_type(self):
        micheline = [
            {
                "prim": "parameter",
                "args": [t.Int()],
            },
            {
                "prim": "storage",
                "args": [t.Int()],
            },
            {
                "prim": "code",
                "args": [
                    Instr("UNPAIR", [], {}),
                    Instr("ADD", [], {}),
                ],
            },
        ]
        typechecker = MichelsonTypeChecker(instructions=micheline)
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck(micheline)

    def skip_test_typecheck_contract_wrong_stack_length(self):
        micheline = [
            {
                "prim": "parameter",
                "args": [t.Int()],
            },
            {
                "prim": "storage",
                "args": [t.Int()],
            },
            {
                "prim": "code",
                "args": [
                    Instr("UNPAIR", [], {}),
                    Instr("ADD", [], {}),
                    Instr("NIL", [t.Operation()], {}),
                    Instr("PAIR", [], {}),
                    Instr("DUP", [], {}),
                ],
            },
        ]
        typechecker = MichelsonTypeChecker()
        with self.assertRaises(StackLengthException):
            typechecker.typecheck_contract(micheline)

    def test_typecheck_pymich_small_compiled_contract(self):
        source = """
class Contract:
    owner_id: Nat
    counter: Nat

    def add(a: Nat):
        b = Nat(10)
        self.counter = self.counter + a + b

    def update_owner_id(new_id: Nat):
        self.owner_id = new_id
        """
        contract = Compiler(source).compile_contract_object()
        typechecker = MichelsonTypeChecker()
        typechecker.typecheck_contract(contract)


    def test_mint(self):
        source = """
@dataclass
class AllowanceKey(Record):
    owner: Address
    spender: Address

@dataclass
class Storage(Record):
    tokens: BigMap[Address, Nat]
    allowances: BigMap[AllowanceKey, Nat]
    total_supply: Nat
    owner: Address

storage = Storage(
    BigMap[Address, Nat](),
    BigMap[AllowanceKey, Nat](),
    Nat(0),
    Address("tz1234"),
)
_to = Address("tz1234")
value = Nat(1)

storage.tokens[_to] = value
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression_object()
        typechecker = MichelsonTypeChecker(instructions=micheline)
        typechecker.typecheck()

    def test_typecheck_pymich_fa12_contracts(self):
        source = """
@dataclass(eq=True, frozen=True)
class AllowanceKey(Record):
    owner: Address
    spender: Address


@dataclass
class FA12(Contract):
    tokens: BigMap[Address, Nat]
    allowances: BigMap[AllowanceKey, Nat]
    total_supply: Nat
    owner: Address

    def mint(self, _to: Address, value: Nat):
        if Tezos.sender != self.owner:
            raise Exception("Only owner can mint")

        self.total_supply = self.total_supply + value

        self.tokens[_to] = self.tokens.get(_to, Nat(0)) + value

    def approve(self, spender: Address, value: Nat):
        allowance_key = AllowanceKey(Tezos.sender, spender)

        previous_value = self.allowances.get(allowance_key, Nat(0))

        if previous_value > Nat(0) and value > Nat(0):
            raise Exception("UnsafeAllowanceChange")

        self.allowances[allowance_key] = value

    def transfer(self, _from: Address, _to: Address, value: Nat):
        if Tezos.sender != _from:
            allowance_key = AllowanceKey(_from, Tezos.sender)

            authorized_value = self.allowances.get(allowance_key, Nat(0))

            if (authorized_value - value) < Int(0):
                raise Exception("NotEnoughAllowance")

            self.allowances[allowance_key] = abs(authorized_value - value)

        from_balance = self.tokens.get(_from, Nat(0))

        if (from_balance - value) < Int(0):
            raise Exception("NotEnoughBalance")

        self.tokens[_from] = abs(from_balance - value)

        to_balance = self.tokens.get(_to, Nat(0))

        self.tokens[_to] = to_balance + value

    def getAllowance(self, owner: Address, spender: Address) -> Nat:
        return self.allowances.get(AllowanceKey(owner, spender), Nat(0))

    def getBalance(self, owner: Address) -> Nat:
        return self.tokens.get(owner, Nat(0))

    def getTotalSupply(self) -> Nat:
        return self.total_supply
        """
        contract = Compiler(source).compile_contract_object()
        typechecker = MichelsonTypeChecker()
        typechecker.typecheck_contract(contract)

    def test_typecheck_pymich_fa2_contracts(self):
        source = """
@dataclass#(eq=True, frozen=True)
class OperatorKey(Record):
    owner: Address
    operator: Address
    token_id: Nat

@dataclass#(eq=True, frozen=True)
class LedgerKey(Record):
    owner: Address
    token_id: Nat

@dataclass#(eq=True, frozen=True)
class TokenMetadata(Record):
    token_id: Nat
    token_info: Dict[String, Bytes]

@dataclass
class TransactionInfo(Record):
    to_: Address
    token_id: Nat
    amount: Nat

@dataclass
class TransferArgs(Record):
    from_: Address
    txs: List[TransactionInfo]


def require_owner(owner: Address) -> Nat:
    if Tezos.sender != owner:
        raise Exception("FA2_NOT_CONTRACT_ADMINISTRATOR")

    return Nat(0)


@dataclass
class Contract:
    ledger: BigMap[LedgerKey, Nat]
    operators: BigMap[OperatorKey, Nat]
    token_total_supply: BigMap[Nat, Nat]
    token_metadata: BigMap[Nat, TokenMetadata]
    owner: Address

    def create_token(self, metadata: TokenMetadata):
        require_owner(self.owner)

        new_token_id = metadata.token_id

        if new_token_id in self.token_metadata:
            raise Exception("FA2_DUP_TOKEN_ID")
        else:
            self.token_metadata[new_token_id] = metadata
            self.token_total_supply[new_token_id] = Nat(0)

    def mint_tokens(self, owner: Address, token_id: Nat, amount: Nat):
        require_owner(self.owner)

        if not token_id in self.token_metadata:
            raise Exception("FA2_TOKEN_DOES_NOT_EXIST")

        ledger_key = LedgerKey(owner, token_id)
        self.ledger[ledger_key] = self.ledger.get(ledger_key, Nat(0)) + amount
        self.token_total_supply[token_id] = self.token_total_supply[token_id] + amount

    def transfer(self, transactions: List[TransferArgs]):
        for transaction in transactions:
            for tx in transaction.txs:
                if not tx.token_id in self.token_metadata:
                    raise Exception("FA2_TOKEN_UNDEFINED")
                else:
                    if not (transaction.from_ == Tezos.sender or OperatorKey(transaction.from_, Tezos.sender, tx.token_id) in self.operators):
                        raise Exception("FA2_NOT_OPERATOR")

                    from_key = LedgerKey(transaction.from_, tx.token_id)
                    from_balance = self.ledger.get(from_key, Nat(0))

                    if tx.amount > from_balance:
                        raise Exception("FA2_INSUFFICIENT_BALANCE")

                    self.ledger[from_key] = abs(from_balance - tx.amount)

                    to_key = LedgerKey(tx.to_, tx.token_id)
                    self.ledger[to_key] = self.ledger.get(to_key, Nat(0)) + tx.amount

    def updateOperator(self, owner: Address, operator: Address, token_id: Nat, add_operator: Boolean):
        if Tezos.sender != owner:
            raise Exception("FA2_NOT_OWNER")

        operator_key = OperatorKey(owner, operator, token_id)
        if add_operator:
            self.operators[operator_key] = Nat(0)
        #else:
        #    del self.operators[operator_key]

    def balanceOf(self, owner: Address, token_id: Nat) -> Nat:
        if not token_id in self.token_metadata:
            raise Exception("FA2_TOKEN_UNDEFINED")

        return self.ledger.get(LedgerKey(owner, token_id), Nat(0))
        """
        contract = Compiler(source).compile_contract_object()
        typechecker = MichelsonTypeChecker()
        typechecker.typecheck_contract(contract)

class TestIntegrationSize(unittest.TestCase):
    def test_typecheck_string_size(self):
        instructions = [
            Instr("PUSH", [t.String(), "foobar"], {}),
            Instr("SIZE", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

    def test_typecheck_dict_size(self):
        instructions = [
            Instr("EMPTY_MAP", [t.String(), t.Nat()], {}),
            Instr("SIZE", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))

    def test_typecheck_size_type_error(self):
        instructions = [
            Instr("PUSH", [t.Int(), 1], {}),
            Instr("SIZE", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        with self.assertRaises(E.StackTopTypeException):
            typechecker.typecheck()

    def test_typecheck_bytes_size(self):
        instructions = [
            Instr("EMPTY_MAP", [t.String(), t.Nat()], {}),
            Instr("PACK", [], {}),
            Instr("SIZE", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Nat()]))


class TestIntegrationCompare(unittest.TestCase):
    def test_typecheck_compare_string(self):
        instructions = [
            Instr("PUSH", [t.String(), "foo"], {}),
            Instr("PUSH", [t.String(), "bar"], {}),
            Instr("COMPARE", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_compare_address(self):
        instructions = [
            Instr("PUSH", [t.Address(), "foo"], {}),
            Instr("PUSH", [t.Address(), "bar"], {}),
            Instr("COMPARE", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_compare_int(self):
        instructions = [
            Instr("PUSH", [t.Int(), 1], {}),
            Instr("PUSH", [t.Int(), 2], {}),
            Instr("COMPARE", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_compare_nat(self):
        instructions = [
            Instr("PUSH", [t.Nat(), 1], {}),
            Instr("PUSH", [t.Nat(), 2], {}),
            Instr("COMPARE", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_compare_mutez(self):
        instructions = [
            Instr("PUSH", [t.Mutez(), 1], {}),
            Instr("PUSH", [t.Mutez(), 2], {}),
            Instr("COMPARE", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_compare_option(self):
        instructions = [
            Instr("PUSH", [t.Mutez(), 1], {}),
            Instr("SOME", [], {}),
            Instr("NONE", [t.Int()], {}),
            Instr("COMPARE", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

    def test_typecheck_compare_bytes(self):
        instructions = [
            Instr("PUSH", [t.Mutez(), 1], {}),
            Instr("PACK", [], {}),
            Instr("PUSH", [t.Int(), 1], {}),
            Instr("PACK", [], {}),
            Instr("COMPARE", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Int()]))

class TestIntegrationGet(unittest.TestCase):
    def test_typecheck_pair_get(self):
        instructions = [
            Instr("PUSH", [t.Int(), 1], {}),
            Instr("PUSH", [t.String(), "hello"], {}),
            Instr("PUSH", [t.Nat(), 2], {}),
            Instr("PAIR", [3], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Pair(t.Nat(), t.Pair(t.String(), t.Int()))]))

        typechecker.instructions = [Instr("GET", [3], {})]
        typechecker.typecheck(typechecker.stack)

        self.assertEqual(typechecker.stack, Stack([t.String()]))

    def test_typecheck_pair_update(self):
        instructions = [
            Instr("PUSH", [t.Int(), 1], {}),
            Instr("PUSH", [t.String(), "hello"], {}),
            Instr("PUSH", [t.Nat(), 2], {}),
            Instr("PAIR", [3], {}),
            Instr("PUSH", [t.Mutez(), 2], {}),
            Instr("UPDATE", [3], {}),
        ]
        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        expected_pair = t.Pair.pair_n([t.Nat(), t.Mutez(), t.Int()])
        self.assertEqual(typechecker.stack, Stack([expected_pair]))

    def test_typecheck_map_update_and_get(self):
        instructions = [
            Instr("EMPTY_MAP", [t.Int(), t.Nat()], {}),
            Instr("PUSH", [t.Nat(), 1], {}),
            Instr("SOME", [], {}),
            Instr("PUSH", [t.Int(), 1], {}),
            Instr("UPDATE", [], {}),
        ]

        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Dict(t.Int(), t.Nat())]))

        typechecker.instructions = [
            Instr("PUSH", [t.Int(), 1], {}),
            Instr("GET", [], {}),
        ]

        typechecker.typecheck(typechecker.stack)
        self.assertEqual(typechecker.stack, Stack([t.Option(t.Nat())]))


class TestIntegrationConcat(unittest.TestCase):
    def test_typecheck_string_concat(self):
        instructions = [
            Instr("PUSH", [t.String(), "foo"], {}),
            Instr("PUSH", [t.String(), "bar"], {}),
            Instr("CONCAT", [], {}),
        ]
        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.String()]))

    def test_typecheck_bytes_concat(self):
        instructions = [
            Instr("PUSH", [t.String(), "foo"], {}),
            Instr("PACK", [], {}),
            Instr("PUSH", [t.String(), "bar"], {}),
            Instr("PACK", [], {}),
            Instr("CONCAT", [], {}),
        ]
        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Bytes()]))


class TestIntegrationSlice(unittest.TestCase):
    def test_typecheck_string_slice(self):
        instructions = [
            Instr("PUSH", [t.String(), "foo"], {}),
            Instr("PUSH", [t.Nat(), 1], {}),
            Instr("PUSH", [t.Nat(), 3], {}),
            Instr("SLICE", [], {}),
        ]
        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Option(t.String())]))

    def test_typecheck_bytes_slice(self):
        instructions = [
            Instr("PUSH", [t.String(), "bar"], {}),
            Instr("PACK", [], {}),
            Instr("PUSH", [t.Nat(), 1], {}),
            Instr("PUSH", [t.Nat(), 3], {}),
            Instr("SLICE", [], {}),
        ]
        typechecker = MichelsonTypeChecker(instructions=instructions)
        typechecker.typecheck()
        self.assertEqual(typechecker.stack, Stack([t.Option(t.Bytes())]))


class TestClosures(unittest.TestCase):
    def test_function_closure(self):
        source = """
def f(x: Nat) -> Nat:
    return Nat(1)

def g(x: Nat) -> Nat:
    return f(Nat(1))

y = g(Nat(1))
        """
        micheline = Compiler(source).compile_expression_object()
        typechecker = MichelsonTypeChecker(instructions=micheline)
        typechecker.typecheck()
        assert len(typechecker.stack) == 3
        assert typechecker.stack.get_at_depth(0) == t.Nat()

    def test_function_closure_2(self):
        source = """
a = Nat(1)
b = Nat(1)

def g(x: Nat) -> Nat:
    return Nat(1) + a + b

y = g(Nat(1))
        """
        micheline = Compiler(source).compile_expression_object()
        typechecker = MichelsonTypeChecker(instructions=micheline)
        typechecker.typecheck()
        assert len(typechecker.stack) == 4
        assert typechecker.stack.get_at_depth(0) == t.Nat()
