from dataclasses import dataclass
from pymich.michelson_types import *
from pymich.stdlib import *


## tests

b = Bytes(Nat(10))
a: Nat = b.unpack(Nat)


@dataclass
class MyRecord(Record):
    foo: Map[String, Nat]


c = MyRecord(Map[String, Nat]().add(String("a"), Nat(10)))
bar = c.foo
bar[String("b")] = Nat(10)

# c.hello

d = 5
#c.foo = 10
#c.hello = "yo"
#d["a"] = 100

l = List(c)

l2 = List[Nat]().prepend(Nat(10))
#List[Nat]().prepend(Int(10))

#m: Map[String, Set[Nat]] = Map({String("a"): Set(Nat(1), Nat(2))})
m = Map[String, Set[Nat]]().add(String("a"), Set(Nat(1), Nat(2)))
k = m[String("a")]

foo = len(m)

# for a, b in BigMap({String("a"): [1, 2, 3]}):
#     pass

Map[String, Int]().add(String("a"), Int(2))


m2 = Map[Set[Nat], Set[Nat]]().add(Set(Nat(1), Nat(2)), Set(Nat(1), Nat(2)))

s: Set[Nat] = Set()
s.add(Nat(1))


Set(True)


michelson_map_a = Map[Nat, Nat]()
michelson_map_a[Nat(1)] = Nat(1)

michelson_map_b = Map[Nat, Map[Nat, Nat]]()
michelson_map_b[Nat(1)] = michelson_map_a

michelson_map_a_copy = michelson_map_b[Nat(1)]
michelson_map_a_copy[Nat(2)] = Nat(2)

michelson_map_a_copy[Nat(2)]  # works
#michelson_map_a[Nat(2)]       # throws exception


python_map_a = {1: 1}
python_map_b = {1: python_map_a}
python_map_a_bis = python_map_b[1]
python_map_a_bis[2] = 2

python_map_a_bis[2]  # works
python_map_a[2]      # works as well


# options

some = Option(Int(10))
opened_some = some.get()
is_some = some.is_none()
#reveal_type(opened_some)

none = Option[Nat](None)
default = none.get_with_default(Nat(0))
is_none = none.is_none()
#reveal_type(none)
