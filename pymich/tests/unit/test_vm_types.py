import unittest
import pymich.middle_end.ir.instr_types as t
from pymich.middle_end.ir.vm_types import *


class TestContract(unittest.TestCase):
    def test_get_contract_body(self):
        contract = Contract(
            storage=10,
            storage_type=t.Int(),
            entrypoints={
                "add": Entrypoint(
                    t.FunctionPrototype(t.Int(), t.Int()),
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                    ],
                ),
                "sub": Entrypoint(
                    t.FunctionPrototype(t.Int(), t.Int()),
                    [
                        Instr("PUSH", [t.Int(), 2], {}),
                    ],
                ),
                "div": Entrypoint(
                    t.FunctionPrototype(t.Int(), t.Int()),
                    [
                        Instr("PUSH", [t.Int(), 3], {}),
                    ],
                ),
            },
            instructions=[],
        )
        contract_body = contract.get_contract_body()
        sorted_entrypoints = [
            contract.entrypoints[name] for name in sorted(contract.entrypoints.keys())
        ]
        entrypoint_tree = EntrypointTree()
        expected_result = entrypoint_tree.list_to_tree(sorted_entrypoints)
        self.assertEqual(contract_body, expected_result)

    def test_entrypoints_to_tree(self):
        contract = Contract(
            storage=10,
            storage_type=t.Int(),
            entrypoints={
                "add": Entrypoint(
                    t.FunctionPrototype(t.Int(), t.Int()),
                    [
                        Instr("PUSH", [t.Int(), 1], {}),
                    ],
                ),
                "sub": Entrypoint(
                    t.FunctionPrototype(t.Int(), t.Int()),
                    [
                        Instr("PUSH", [t.Int(), 2], {}),
                    ],
                ),
                "div": Entrypoint(
                    t.FunctionPrototype(t.Int(), t.Int()),
                    [
                        Instr("PUSH", [t.Int(), 3], {}),
                    ],
                ),
            },
            instructions=[],
        )
        entrypoints = [
            contract.entrypoints[name] for name in sorted(contract.entrypoints.keys())
        ]
        entrypoint_tree = EntrypointTree()
        contract_body = entrypoint_tree.list_to_tree(entrypoints)
        expected_result = Instr(
            "IF_LEFT",
            [
                [
                    Instr(
                        "IF_LEFT",
                        [
                            contract.entrypoints["add"].instructions,
                            contract.entrypoints["div"].instructions,
                        ],
                        {},
                    )
                ],
                contract.entrypoints["sub"].instructions,
            ],
            {},
        )
        self.assertEqual(contract_body, expected_result)

    def skip_test_navigate_to_tree_leaf(self):
        entrypoint_list = ["add", "div", "mul", "sub", "modulo"]
        param_tree = ParameterTree()
        tree = param_tree.list_to_tree(entrypoint_list)
        entrypoint_param = 111

        entrypoint_index = entrypoint_list.index("add")
        contract_param = param_tree.navigate_to_tree_leaf(
            tree, entrypoint_index + 1, entrypoint_param
        )
        self.assertEqual(contract_param, Left(Left(Left(entrypoint_param))))

        entrypoint_index = entrypoint_list.index("div")
        contract_param = param_tree.navigate_to_tree_leaf(
            tree, entrypoint_index + 1, entrypoint_param
        )
        self.assertEqual(contract_param, Left(Left(Right(entrypoint_param))))

        entrypoint_index = entrypoint_list.index("mul")
        contract_param = param_tree.navigate_to_tree_leaf(
            tree, entrypoint_index + 1, entrypoint_param
        )
        self.assertEqual(contract_param, Left(Right(Left(entrypoint_param))))

        entrypoint_index = entrypoint_list.index("sub")
        contract_param = param_tree.navigate_to_tree_leaf(
            tree, entrypoint_index + 1, entrypoint_param
        )
        self.assertEqual(contract_param, Left(Right(Right(entrypoint_param))))

        entrypoint_index = entrypoint_list.index("modulo")
        contract_param = param_tree.navigate_to_tree_leaf(
            tree, entrypoint_index + 1, entrypoint_param
        )
        self.assertEqual(contract_param, Right(entrypoint_param))

    def skip_test_left_side_tree_height(self):
        param_tree = ParameterTree()
        self.assertEqual(
            param_tree.left_side_tree_height(param_tree.list_to_tree([1, 2, 3, 4])), 2
        )
        self.assertEqual(
            param_tree.left_side_tree_height(param_tree.list_to_tree([1, 2, 3, 4, 5])),
            3,
        )
        self.assertEqual(
            param_tree.left_side_tree_height(
                param_tree.list_to_tree([1, 2, 3, 4, 5]).left
            ),
            2,
        )

    def skip_test_list_to_tree(self):
        l = [1, 2]
        param_tree = ParameterTree()
        tree = param_tree.list_to_tree(l)
        self.assertEqual(Or(left=1, right=2), tree)

        l = [1, 2, 3]
        tree = param_tree.list_to_tree(l)
        self.assertEqual(Or(left=Or(left=1, right=2), right=3), tree)

        l = [1, 2, 3, 4]
        tree = param_tree.list_to_tree(l)
        self.assertEqual(Or(left=Or(left=1, right=2), right=Or(left=3, right=4)), tree)

        l = [1, 2, 3, 4, 5]
        tree = param_tree.list_to_tree(l)
        self.assertEqual(
            Or(left=Or(left=Or(left=1, right=2), right=Or(left=3, right=4)), right=5),
            tree,
        )

        l = [1, 2, 3, 4, 5, 6]
        tree = param_tree.list_to_tree(l)
        self.assertEqual(
            Or(
                left=Or(left=Or(left=1, right=2), right=Or(left=3, right=4)),
                right=Or(left=5, right=6),
            ),
            tree,
        )

        l = [1, 2, 3, 4, 5, 6, 7]
        tree = param_tree.list_to_tree(l)
        self.assertEqual(
            Or(
                left=Or(left=Or(left=1, right=2), right=Or(left=3, right=4)),
                right=Or(left=Or(left=5, right=6), right=7),
            ),
            tree,
        )

        l = [1, 2, 3, 4, 5, 6, 7, 8]
        tree = param_tree.list_to_tree(l)
        self.assertEqual(
            Or(
                left=Or(left=Or(left=1, right=2), right=Or(left=3, right=4)),
                right=Or(left=Or(left=5, right=6), right=Or(left=7, right=8)),
            ),
            tree,
        )

        l = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        tree = param_tree.list_to_tree(l)
        self.assertEqual(
            Or(
                left=Or(
                    left=Or(left=Or(left=1, right=2), right=Or(left=3, right=4)),
                    right=Or(left=Or(left=5, right=6), right=Or(left=7, right=8)),
                ),
                right=9,
            ),
            tree,
        )

    def skip_test_make_contract_param(self):
        contract = Contract(
            storage=10,
            storage_type=t.Int(),
            entrypoints={
                "add": Entrypoint(
                    t.FunctionPrototype(t.Int(), t.Int()),
                    [],
                ),
                "sub": Entrypoint(
                    t.FunctionPrototype(t.Int(), t.Int()),
                    [],
                ),
                "div": Entrypoint(
                    t.FunctionPrototype(t.Int(), t.Int()),
                    [],
                ),
            },
            instructions=[],
        )
        self.assertEqual(
            Left(Left(1)),
            contract.make_contract_param("add", 1),
        )
        self.assertEqual(
            Left(Right(1)),
            contract.make_contract_param("div", 1),
        )
        self.assertEqual(
            Right(1),
            contract.make_contract_param("sub", 1),
        )

    def skip_test_contract_parameter_type(self):
        contract = Contract(
            storage=10,
            storage_type=t.Int(),
            entrypoints={
                "add": Entrypoint(
                    t.FunctionPrototype(t.Int(), t.Int()),
                    [],
                ),
                "sub": Entrypoint(
                    t.FunctionPrototype(t.Int(), t.Int()),
                    [],
                ),
                "div": Entrypoint(
                    t.FunctionPrototype(t.Int(), t.Int()),
                    [],
                ),
            },
            instructions=[],
        )
        self.assertEqual(
            Or(Or(t.Int(), t.Int()), t.Int()),
            contract.get_parameter_type(),
        )


suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestContract)
unittest.TextTestRunner().run(suite)

if __name__ == "__main__":
    unittest.main()
