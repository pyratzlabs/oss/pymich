import unittest
import ast

from pymich.frontend.passes import *

class TestRewriteViews(unittest.TestCase):
    def test_function_call_in_block(self):
        source = """
class Contract:
    def myView(self, arg1: type1, arg2: type2) -> return_type:
        return f(arg1, arg2)
        """
        source_ast = ast.parse(source)
        new_ast = RewriteViews().visit(source_ast)
        new_ast = ast.fix_missing_locations(new_ast)
        view_ast = new_ast.body[0].body[0]

        # test function prototype
        self.assertEqual(view_ast.returns, None)
        last_argument = view_ast.args.args[-1]
        self.assertEqual(last_argument.arg, '__callback__')
        self.assertEqual(type(last_argument.annotation), ast.Subscript)
        self.assertEqual(type(last_argument.annotation.value), ast.Name)
        self.assertEqual(last_argument.annotation.value.id, 'Contract')
        self.assertEqual(type(last_argument.annotation.slice), ast.Name)
        self.assertEqual(last_argument.annotation.slice.id, 'return_type')

        # test transaction call
        transaction = view_ast.body[-1]
        self.assertEqual(transaction.func.id, "transaction")
        self.assertEqual(type(transaction.args[0]), ast.Name)
        self.assertEqual(transaction.args[0].id, "__callback__")
        self.assertEqual(type(transaction.args[1]), ast.Call)
        self.assertEqual(transaction.args[1].func.id, "Mutez")
        self.assertEqual(type(transaction.args[1].args[0]), ast.Constant)
        self.assertEqual(transaction.args[1].args[0].value, 0)
        return_value = ast.parse(source).body[0].body[0].body[0].value
        self.assertEqual(ast.dump(transaction.args[2]), ast.dump(return_value))


class TestFactorOutStorage(unittest.TestCase):
    def test_new_function_evaluates(self):
        source = """
class Contract:
    counter: int
    admin: str

    def update_counter(self, new_counter: int) -> int:
        self.counter = 1

    def update_admin(self, new_admin: str) -> int:
        self.admin = new_admin
        """
        source_ast = ast.parse(source)
        expander = FactorOutStorage()
        new_ast = expander.visit(source_ast)
        new_ast.body = [expander.storage_dataclass] + new_ast.body
        new_ast = ast.fix_missing_locations(new_ast)

    def test_replace_nested_self(self):
        source = """
(self.foo - bar).to_nat()
        """
        source_ast = ast.parse(source)
        expander = ExpandStorageInEntrypoints()
        new_ast = expander.visit(source_ast)
        self.assertEqual(ast.unparse(new_ast), "(__STORAGE__.foo - bar).to_nat()")

class TestRemoveSelfArgFromMethods(unittest.TestCase):
    def test_new_function_evaluates(self):
        source = """
class C:
    def f(self, x: int, y: str, z: int): return 1
        """
        source_ast = ast.parse(source)
        new_ast = RemoveSelfArgFromMethods().visit(source_ast)
        new_method_ast = source_ast.body[0].body[0]
        self.assertEqual(len(new_method_ast.args.args), 3)

        for arg_node, arg_name, arg_type in zip(new_method_ast.args.args, ['x', 'y', 'z'], ['int', 'str', 'int']):
            self.assertEqual(arg_node.arg, arg_name)
            self.assertEqual(arg_node.annotation.id, arg_type)


class TestTuplifyFunctionArguments(unittest.TestCase):
    def test_new_function_evaluates(self):
        source = """
from dataclasses import dataclass

def add(x: int, y: int, z: int) -> int:
    return x + y + z

def increment(x: int) -> int:
    return add(x, 1, 0)

assert add(1, 2, add(3, 4, 5)) == 15
assert increment(10) == 11
"""
        f_ast = ast.parse(source)
        pass1 = TuplifyFunctionArguments(std_lib={})
        new_f_ast = pass1.visit(f_ast)
        new_f_ast.body = new_f_ast.body[:1] + pass1.dataclasses + new_f_ast.body[1:]
        new_f_ast = ast.fix_missing_locations(new_f_ast)
        local_vars = {}
        eval(compile(new_f_ast, '', mode='exec'), local_vars)
        add = local_vars["add"]
        addParam = local_vars["addParam"]
        self.assertEqual(add(addParam(1, 2, add(addParam(3, 4, 5)))), 15)


class TestAssignAllFunctionCallsTests(unittest.TestCase):
    def test_function_call_in_block(self):
        source = """
f = lambda x: x
if True:
    y = f(1)
    f(2)

assert __placeholder__ == 2
assert y == 1
        """
        source_ast = ast.parse(source)
        new_ast = AssignAllFunctionCalls().visit(source_ast)
        new_ast = ast.fix_missing_locations(new_ast)
        eval(compile(new_ast, '', mode='exec'))


class TestRewriteOperations(unittest.TestCase):
    def test_function_call_in_block(self):
        source = "self.ops = self.ops.push(a, b, c)"
        expected_output = "__OPERATIONS__ = __OPERATIONS__.push(a, b, c)"
        source_ast = ast.parse(source)
        new_ast = RewriteOperations().visit(source_ast)
        new_ast = ast.fix_missing_locations(new_ast)
        self.assertEqual(ast.unparse(new_ast), expected_output)

for TestSuite in [
        TestRewriteViews,
        TestTuplifyFunctionArguments,
        TestAssignAllFunctionCallsTests,
        TestRemoveSelfArgFromMethods,
        TestFactorOutStorage,
]:
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestSuite)
    unittest.TextTestRunner().run(suite)
