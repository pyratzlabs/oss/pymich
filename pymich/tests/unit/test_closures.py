import ast
import unittest
from pymich.utils.environment import Env
from pymich.backend.closures import ClosureAnalyzer


class TestRewriteViews(unittest.TestCase):
    def test_closure_in_return(self):
        source = """
def f(x: Nat) -> Nat:
    return a
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"a": 0}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["a"])

    def test_var_not_in_closure(self):
        source = """
def f(x: Nat) -> Nat:
    return a
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, [])

    def test_closure_in_binop(self):
        source = """
def f(x: Nat) -> Nat:
    a + b
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"a": 0, "b": 1}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["a", "b"])

    def test_closure_in_assign(self):
        source = """
def f(x: Nat) -> Nat:
    b = a
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"a": 0, "b": 1}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["a"])
        self.assertEqual(list(closure_env.function_env.vars.keys()), ["x", "b"])

    def test_function_in_closure(self):
        source = """
def f(x: Nat) -> Nat:
    c = g(a, b)
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"g": 0, "a": 1, "b": 2}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["g", "a", "b"])

    def test_attributes(self):
        source = """
def f(x: Nat) -> Nat:
    b = foo.bar.baz
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"foo": 0}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["foo"])

    def test_subscript(self):
        source = """
def f(x: Nat) -> Nat:
    b = foo["bar"]
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"foo": 0}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["foo"])

    def test_nested(self):
        source = """
def f(x: Nat) -> Nat:
    b = foo.bar(Unit())(x).baz["bar"]["foo"]()
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"foo": 0}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["foo"])

    def test_condition(self):
        source = """
def f(x: Nat) -> Nat:
    if True:
        return a
    elif False:
        return b
    else:
        return c
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"a": 0, "b": 1, "c": 2}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["a", "b", "c"])

    def test_unary(self):
        source = """
def f(x: Nat) -> Nat:
    if not a:
        return 0
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"a": 0}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["a"])

    def test_compare(self):
        source = """
def f(x: Nat) -> Nat:
    if a == b:
        return 0
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"a": 0, "b": 1}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["a", "b"])

    def test_for(self):
        source = """
def f(x: Nat) -> Nat:
    for a, b in c:
        return 0
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"a": 0, "b": 1, "c": 2}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["a", "b", "c"])

    def test_tuple(self):
        source = """
def f(x: Nat) -> Nat:
    c = (a, b)
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"a": 0, "b": 1}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["a", "b"])

    def test_nested_function(self):
        source = """
def f(x: Nat) -> Nat:
    def g(y: Nat) -> Nat:
        c = a + b
        return c
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"a": 0, "b": 1}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["a", "b"])

    def test_multi_arg_function(self):
        source = """
def f(x: Nat, b: Nat) -> Nat:
    return a + b
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"a": 0, "b": 1}, 0, {}, {})
        f_ast = ast.parse(source).body[0]
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["a"])

    def test_module(self):
        source = """
def f(x: Nat) -> Nat:
    def g(y: Nat) -> Nat:
        c = a + b
        return c
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"a": 0, "b": 1}, 0, {}, {})
        f_ast = ast.parse(source)
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["a", "b"])

    def test_raise(self):
        source = """
def f(x: Nat) -> Nat:
    raise Exception("error")
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({}, 0, {}, {})
        f_ast = ast.parse(source)
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, [])

    def test_list(self):
        source = """
def f(x: Nat) -> List[Nat]:
    return List[Nat]([x, a])
        """
        closure_analyzer = ClosureAnalyzer()
        env = Env({"a": 0}, 0, {}, {})
        f_ast = ast.parse(source)
        closure_env = closure_analyzer.get_closure(f_ast, env)
        self.assertEqual(closure_env.closure, ["a"])
