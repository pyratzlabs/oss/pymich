import time
from datetime import datetime, timedelta

import unittest
import ast
import pprint

from pymich.compiler import Compiler, Env
from pymich.test_utils import VM
import pymich.utils.exceptions as E
import pymich.middle_end.ir.instr_types as t
from pymich.middle_end.ir.vm_types import Instr

from pytezos.michelson.types.core import *
from pytezos.michelson.types.domain import AddressType
from pytezos.michelson.types.map import MapType
from pytezos.michelson.types.set import SetType
from pytezos.michelson.types.operation import *
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.format import micheline_to_michelson

from pytezos.michelson.micheline import MichelsonRuntimeError
from pymich.backend.micheline_emitter import CompilerBackend


pprint = pprint.pprint


class TestDates(unittest.TestCase):
    def test_create_timestamp(self):
        source = "Timestamp(100)"
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, 100)

    def test_timestamp_now_ir(self):
        source = """
tmp_var_0 = Timestamp.now
tmp_var_0()
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(micheline, [{'prim': 'NOW'}])
        self.assertEqual(vm.stack.peek().value, 0)

    def test_timestamp_now_from_instance(self):
        source = "Timestamp.now()"
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, 0)

        source = """
timestamp = Timestamp(100)
a = Nat(1)
Timestamp.now()
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.items[0].value, 0)
        self.assertEqual(vm.stack.items[1].value, 1)
        self.assertEqual(vm.stack.items[2].value, 100)

    def test_create_date(self):
        source = """
date = datetime(2020, 12, 31, 0, 0)
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        expected_timestamp = int(time.mktime(datetime(2020, 12, 31, 0, 0).timetuple()))
        self.assertEqual(vm.stack.peek().value, expected_timestamp)
        self.assertEqual(type(compiler.env.types['date']), t.Datetime)

    def test_create_timedelta(self):
        source = """
delta = timedelta(0, 12, 31)
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        expected_delta_value = int(timedelta(0, 12, 31).total_seconds())
        self.assertEqual(vm.stack.peek().value, expected_delta_value)
        self.assertEqual(type(compiler.env.types['delta']), t.Int)

    def test_add_timedelta(self):
        source = """
datetime(2020, 1, 1, 0, 0) + timedelta(0, 12, 31)
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        expected_timestamp = int(time.mktime(datetime(2020, 1, 1).timetuple())) + int(timedelta(0, 12, 31).total_seconds())
        self.assertEqual(vm.stack.peek().value, expected_timestamp)

    def test_datetime_now(self):
        source = """
Timestamp.now()
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, 0)

    def test_compare_datetimes(self):
        source = """
Timestamp(1000) + Int(10) > Timestamp(1000)
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, False)

    def test_parse_datetime_type(self):
        source = """
d: datetime = datetime.now()
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(type(compiler.env.types["d"]), t.Datetime)


class TestErrors(unittest.TestCase):
    def test_unknown_type(self):
        source = """
def apply(x: foobar) -> Nat:
    return Nat(1)
        """
        compiler = Compiler(source)
        try:
            compiler.compile_expression()
            self.assertFalse(1)
        except E.TypeAnnotationDoesNotExistException as e:
            message = "Type annotation 'foobar' does not exist, expected ['Unit', 'Nat', 'Int', 'str', 'Address', 'bool', 'unit', 'Mutez', 'bytes', 'datetime', 'Records'] at line 2"
            self.assertEqual(e.message, message)

    def test_unknown_ploymorphic_type(self):
        source = """
def apply(x: Foo[Nat]) -> Nat:
    return Nat(1)
        """
        compiler = Compiler(source)
        try:
            compiler.compile_expression()
            self.assertFalse(1)
        except E.TypeAnnotationDoesNotExistException as e:
            message = "Type annotation 'Foo' does not exist, expected only polymorphic types (['Dict', 'List', 'Set', 'Callable', 'Contract']) are subscriptable at line 2"
            self.assertEqual(e.message, message)

    def test_unknown_type_format(self):
        source = """
def apply(x: Foo(int)) -> Nat:
    return Nat(1)
        """
        compiler = Compiler(source)
        try:
            compiler.compile_expression()
            self.assertFalse(1)
        except E.TypeAnnotationDoesNotExistException as e:
            message = "Type annotation 'Call(func=Name(id='Foo', ctx=Load()), args=[Name(id='int', ctx=Load())], keywords=[])' does not exist, expected expected a base type (['Unit', 'Nat', 'Int', 'str', 'Address', 'bool', 'unit', 'Mutez', 'bytes', 'datetime']) or an polymorphic type (['Dict', 'List', 'Set', 'Callable', 'Contract']) at line 2"
            self.assertEqual(e.message, message)

    def test_type_error_function_call_param_type(self):
        source = """
def add(x: Nat, y: Nat) -> Nat:
    return x + y
add(Nat(1), String("foo"))
"""
        vm = VM()
        compiler = Compiler(source)
        try:
            compiler.compile_expression()
            self.assertFalse(1)
        except E.TypeException as e:
            message = "Function expected parameters of type nat but got string at line 4"
            self.assertEqual(e.message, message)

    def test_type_error_not_a_record(self):
        source = """foo = Nat(1)
foo.bar
"""
        vm = VM()
        compiler = Compiler(source)
        try:
            compiler.compile_expression()
            self.assertFalse(1)
        except E.TypeException as e:
            message = "Unexpected type at line 2:\n  - expected <class 'pymich.middle_end.ir.instr_types.Record'>,\n  - got <class 'pymich.middle_end.ir.instr_types.Nat'>"
            self.assertEqual(e.message, message)

    def test_variable_does_not_exist(self):
        source = """bar = Nat(1)
foo
        """
        vm = VM()
        compiler = Compiler(source)
        try:
            compiler.compile_expression()
            self.assertFalse(1)
        except E.NameException as e:
            message = "Variable 'foo' does not exist at line 2"
            self.assertEqual(e.message, message)

    def test_variable_does_not_exist_in_function(self):
        source = """def f(x: Nat, y: Nat) -> Nat:
    return x + y + z
f(Nat(1), Nat(2))
        """
        vm = VM()
        compiler = Compiler(source)
        try:
            compiler.compile_expression()
            self.assertFalse(1)
        except E.NameException as e:
            message = "Variable 'z' does not exist at line 2"
            self.assertEqual(e.message, message)

    def test_function_does_not_exist(self):
        source = "g(Nat(1), Nat(2))"
        compiler = Compiler(source)

        with self.assertRaises(E.FunctionNameException) as e:
            compiler.compile_expression()
            message = "Function 'g' does not exist at line 1"
            self.assertEqual(e.message, message)

    def test_attribute_does_not_exist(self):
        source = """
@dataclass
class Bar:
    a: Int
    b: Int

@dataclass
class Foo:
    bar: Bar
    b: Int

a = Foo(Bar(Int(1), Int(1)), Int(1))

a.bar.baz
        """
        compiler = Compiler(source)

        with self.assertRaises(E.AttributeException) as e:
            compiler.compile_expression()
            message = "(int, int) pair Record has no attribute baz at line 14"
            self.assertEqual(e.message, message)


class TestGetType(unittest.TestCase):
    def test_dict(self):
        e = Env({}, -1, {}, {})
        e.types = {
            'my_storage': t.Dict(t.Int(), t.String())
        }
        node = ast.parse("my_storage").body[0].value
        self.assertEqual(Compiler().type_checker.get_expression_type(node, e), e.types['my_storage'])

        node = ast.parse("my_storage[Nat(1)]").body[0].value
        self.assertEqual(Compiler().type_checker.get_expression_type(node, e), t.String())

    def test_list(self):
        e = Env({}, -1, {}, {})
        e.types = {
            'my_storage': t.List(t.Int())
        }
        node = ast.parse("my_storage").body[0].value
        self.assertEqual(Compiler().type_checker.get_expression_type(node, e), e.types['my_storage'])

        node = ast.parse("my_storage[Nat(1)]").body[0].value
        self.assertEqual(Compiler().type_checker.get_expression_type(node, e), t.Int())

    def test_attribute(self):
        e = Env({}, -1, {}, {})
        e.types = {
            'my_storage': t.Record('Storage', ["key_A", "key_B"], [t.Int(), t.String()])
        }
        node = ast.parse("my_storage.key_A").body[0].value
        self.assertEqual(Compiler().type_checker.get_expression_type(node, e), t.Int())
        node = ast.parse("my_storage.key_B").body[0].value
        self.assertEqual(Compiler().type_checker.get_expression_type(node, e), t.String())

    def test_nested_attribute(self):
        e = Env({}, -1, {}, {})
        substorage = t.Record("SubStorage", ["key_A", "key_B"], [t.Int(), t.String()])
        e.types = {
            'my_storage': t.Record('Storage', ["key_A", "key_B"], [t.Int(), substorage])
        }

        node = ast.parse("my_storage.key_B").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, substorage)

        node = ast.parse("my_storage.key_B.key_A").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, t.Int())

        node = ast.parse("my_storage.key_B.key_B").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, t.String())

    def test_nested_attribute_with_iterables(self):
        e = Env({}, -1, {}, {})
        subsubstorage = t.Record("subsubstorage", ["key_A", "key_B"], [t.Int(), t.Dict(t.Int(), t.String())])
        substorage = t.Record("substorage", ["key_A", "key_B"], [t.Int(), subsubstorage])
        e.types = {
            'my_storage': t.Record("storage", ["key_A", "key_B"], [t.Int(), t.List(substorage)])
        }

        node = ast.parse("my_storage.key_B").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, t.List(substorage))

        node = ast.parse("my_storage.key_B[Nat(1)]").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, substorage)

        node = ast.parse("my_storage.key_B[Nat(1)].key_A").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, t.Int())

        node = ast.parse("my_storage.key_B[Nat(1)].key_B.key_B[Nat(1)]").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, t.String())

    def test_nested_attribute_with_contiguous_dicts(self):
        e = Env({}, -1, {}, {})
        sub_dict = t.Dict(t.Int(), t.Record("subrecord", ["key_A", "key_B"], [t.Int(), t.String()]))
        key_B = t.Dict(t.Int(), sub_dict)
        e.types = {
            'my_storage': t.Record(
                "storage",
                ["key_A", "key_B"],
                [t.Int(), key_B],
            )
        }

        node = ast.parse("my_storage.key_B").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, key_B)

        node = ast.parse("my_storage.key_B[Nat(1)][Nat(1)].key_B").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, t.String())

    def test_nested_attribute_with_contiguous_lists(self):
        e = Env({}, -1, {}, {})
        subrecord = t.Record("subrecord", ["key_A", "key_B"], [t.Int(), t.String()])
        e.types = {
            'my_storage': t.Record(
                "storage",
                ["key_A", "key_B"],
                [t.Int(), t.List(t.List(subrecord))],
            )
        }

        node = ast.parse("my_storage.key_B").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, t.List(t.List(subrecord)))

        node = ast.parse("my_storage.key_B[Nat(1)][Nat(1)].key_B").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, t.String())

    def test_nested_attribute_with_contiguous_list_dict(self):
        e = Env({}, -1, {}, {})
        subrecord = t.Record("subrecord", ["key_A", "key_B"], [t.Int(), t.String()])
        key_B = t.Dict(t.Int(), t.List(subrecord))
        e.types = {
            'my_storage': t.Record(
                "storage",
                ["key_A", "key_B"],
                [t.Int(), key_B],
            )
        }

        node = ast.parse("my_storage.key_B").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, key_B)

        node = ast.parse("my_storage.key_B[Nat(1)][Nat(1)].key_B").body[0].value
        res = Compiler().type_checker.get_expression_type(node, e)
        self.assertEqual(res, t.String())



class TestIteration(unittest.TestCase):
    def test_list_iteration_not_an_iterable_error(self):
        source = """
class Contract:
    counter: Nat
    admin: Address

    def update_counter(self, param: unit):
        for i in self.admin:
            self.counter = self.counter + i

    def update_counter_2(self, increments: Nat):
        self.counter = Nat(10)
"""
        vm = VM()
        compiler = Compiler(source)
        try:
            compiler.compile_contract()
            self.assertFalse(1)
        except E.NotAnIterableException as e:
            self.assertEqual(e.message, "Address is not an iterable")

    def test_list_iteration(self):
        source = """
nat_list = List[Nat]().prepend(Nat(2)).prepend(Nat(1))
counter = Nat(0)
for i in nat_list:
    counter = counter + i
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 3)

    def test_set_iteration(self):
        source = """
nat_set = Set[Nat]().add(Nat(2)).add(Nat(1))
counter = Nat(0)
for i in nat_set:
    counter = counter + i
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 3)

    def test_list_iteration_contract(self):
        source = """
def foo(a: Nat, b: Nat) -> Nat:
    return a + b

class Contract:
    counter: Nat
    admin: Address
    yo: Dict[Nat, String]

    def update_counter(self, increments: List[Nat]):
        for i in increments:
            self.counter = self.counter + i

    def update_counter_2(self, increments: Nat):
        self.counter = Nat(10)
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm.load_contract(micheline)
        new_storage = vm.contract.update_counter([10, 11, 12]).interpret().storage
        self.assertEqual(new_storage["counter"], 33)

    def test_list_iteration_record(self):
        source = """
@dataclass
class TransferArgs:
    to_: Address
    amount: Nat

class Contract:
    counter: Nat
    admin: Address

    def update_counter(self, txs: List[TransferArgs]):
        for tx in txs:
            self.counter = self.counter + tx.amount

    def update_counter_2(self, increments: Nat):
        self.counter = Nat(10)
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm.load_contract(micheline)
        new_storage = vm.contract.update_counter([{"to_": vm.context.sender, "amount": 10}]).interpret().storage
        self.assertEqual(new_storage["counter"], 10)

    def test_attribute_list_iteration_record(self):
        source = """
@dataclass
class TransferInfo:
    to_: Address
    amount: Nat

@dataclass
class TransferArgs:
    count: Nat
    txs: List[TransferInfo]

class Contract:
    counter: Nat
    admin: Address

    def update_counter(self, param: TransferArgs):
        for tx in param.txs:
            self.counter = self.counter + tx.amount

    def update_counter_2(self, increments: Nat):
        self.counter = Nat(10)
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm.load_contract(micheline)
        new_storage = vm.contract.update_counter({"count": 0, "txs": [{"to_": vm.context.sender, "amount": 10}]}).interpret().storage
        self.assertEqual(new_storage["counter"], 10)


class TestSetNestedExpressionType(unittest.TestCase):
    def test_set_nested_expr(self):
        source = """
@dataclass
class SubSubRecord:
    a: Nat
    c: String

@dataclass
class SubRecord:
    a: Nat
    b: Dict[Nat, SubSubRecord]

@dataclass
class Record:
    a: Nat
    b: SubRecord

rec = Record(Nat(1), SubRecord(Nat(1), Map[Nat, SubSubRecord]()))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)

        storage_location = compiler.env.vars["rec"]
        expected_value= (1, 1, {})
        self.assertEqual(vm.stack.items[-storage_location-1].to_python_object(), expected_value)

    def test_set_doubly_nested_attr(self):
            source = """
@dataclass
class SubRecord:
    a: Nat
    b: String

@dataclass
class Record:
    a: SubRecord
    b: Nat

rec = Record(SubRecord(Nat(1), String("my_string")), Nat(1))
            """
            vm = VM()
            compiler = Compiler(source)
            micheline = compiler.compile_expression()
            vm.execute(micheline)

            node = ast.parse("rec.a.b = String('hello')").body[0]
            instr = compiler.compile_assign_record(node, compiler.env)
            micheline = CompilerBackend().compile_instructions(instr)
            vm.execute(micheline)

            # node = ast.parse("rec.a.b[1].c = 'hello'").body[0].targets[0]
            # compiler._get_set_nested_expr_spec(node, compiler.env)
            # [{'type': 'FETCH', 'value': 'rec'}, {'type': 'ATTR', 'value': {'record': <pymich.instr_types.Record object at 0x10464a070>, 'key': 'a'}}, {'type': 'ATTR', 'value': {'record': <pymich.instr_types.Record object at 0x10461cfa0>, 'key': 'b'}}, {'type': 'SUBSCRIPT', 'value': <_ast.Constant object at 0x10464a9a0>}, {'type': 'ATTR', 'value': {'record': <pymich.instr_types.Record object at 0x10461cc10>, 'key': 'c'}}]

class TestBigMap(unittest.TestCase):
    def test_create_empty_map(self):
        source = """
BigMap[String, Nat]()
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), -1)

    def test_set_map_element(self):
        source = """
m = BigMap[Nat, Nat]().add(Nat(1), Nat(1))
m[Nat(1)]
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 1)

    def test_set_map_element_2(self):
        source = """
a = Map[Nat, Nat]()
a[Nat(3)] = Nat(3)
a[Nat(3)]
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 3)

class TestDict(unittest.TestCase):
    def test_create_empty_map(self):
        source = """
m = Map[Nat, Nat]()
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), {})

    def test_set_map_element(self):
        source = """
m = Map[Nat, Nat]()
m.add(Nat(1), Nat(1))
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), {1: 1})
        self.assertEqual(vm.stack.items[1].to_python_object(), {})

    def test_set_map_element_2(self):
        source = """
a = Map[Nat, Nat]().add(Nat(1), Nat(1)).add(Nat(2), Nat(2))
a.add(Nat(3), Nat(3))
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), {1: 1, 2: 2, 3: 3})
        self.assertEqual(vm.stack.items[1].to_python_object(), {1: 1, 2: 2})


    def test_non_empty_dict_literal(self):
        source = """
foo = Nat(10)
bar = {foo: String("yo"), Nat(11): String("baz")}
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), {10: "yo", 11: "baz"})

    def test_create_empty_dict(self):
        source = """
d: Dict[Nat, Nat] = {}
d[Nat(1)] = Nat(10)
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), {1: 10})

    def test_dict_get_with_default(self):
        source = """
@dataclass
class Storage:
    accounts: Dict[String, Nat]
    counter: Nat

store = Storage(Map[String, Nat](), Nat(0))
store.accounts.get(String("my_key"), Nat(0))
"""
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, 0)
        self.assertEqual(compiler.env.sp, 4)

        source = """
@dataclass
class Storage:
    accounts: Dict[String, String]
    counter: Nat

store = Storage(Map[String, String](), Nat(0))
store.accounts[String("my_key")] = String("my_value")
store.accounts.get(String("my_key"), String("some_default"))
"""
        vm = VM()
        micheline = Compiler(source).compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, 'my_value')
        self.assertEqual(compiler.env.sp, 4)

    def test_record_valued_dict(self):
        vm = VM()
        source = """
@dataclass
class Account:
    name: String
    balance: Nat

@dataclass
class Storage:
    accounts: Dict[String, Account]
    owner: String

store = Storage(Map[String, Account](), String("pymich"))
store.accounts["my_hash"] = Account(String("pymich"), Nat(1000))
store.accounts["my_hash"].name
"""
        micheline = Compiler(source).compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, 'pymich')

    def test_record_key_dict(self):
        vm = VM()
        source = """
@dataclass
class Key:
    a: Nat
    b: Nat

@dataclass
class Storage:
    accounts: Dict[Key, String]
    owner: String

store = Storage(Map[Key, String](), String("pymich"))
key = Key(Nat(10), Nat(20))
store.accounts[key] = String("hello")
store.accounts[key]
"""
        micheline = Compiler(source).compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, 'hello')

    def test_get_dict_no_key_error(self):
        source = """
@dataclass
class Storage:
    balances: Dict[String, Nat]
    owner: String

storage = Storage(Map[String, Nat](), String("owner"))
balances = storage.balances
user = String('Mr. Foobar')
balances[user] = Nat(100)
balances[user]
        """
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        expected_int = NatType(100)
        expected_string = StringType('Mr. Foobar')
        expected_map = MapType([(expected_string, expected_int)])
        empty_map = MapType([])
        expected_pair = PairType((MapType([]), StringType("owner")))
        expected_stack = [expected_int, expected_string, expected_map, expected_pair, empty_map]
        self.assertEqual(vm.stack.items, expected_stack)


    def test_key_in_dict(self):
        source = """
@dataclass
class Storage:
    balances: Dict[String, Nat]
    owner: String

storage = Storage(Map[String, Nat](), String("owner"))
balances = storage.balances
user = String('Mr. Foobar')
balances[user] = Nat(100)
user in balances
        """
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        expected_string = StringType('Mr. Foobar')
        expected_int = NatType(100)
        expected_map = MapType([(expected_string, expected_int)])
        empty_map = MapType([])
        expected_pair = PairType((MapType([]), StringType("owner")))
        expected_bool = BoolType(True)
        expected_stack = [expected_bool, expected_string, expected_map, expected_pair, empty_map]
        self.assertEqual(vm.stack.items, expected_stack)

    def test_get_dict_key_error(self):
        source = """
@dataclass
class Storage:
    balances: Dict[String, Nat]
    owner: String

storage = Storage(Map[String, Nat](), String("owner"))
balances = storage.balances
user = String('Mr. Foobar')
balances[user] = Nat(100)
balances[String('user')]
        """
        micheline = Compiler(source).compile_expression()
        vm = VM()
        try:
            vm.execute(micheline)
            assert 0
        except MichelsonRuntimeError as e:
            self.assertEqual(e.format_stdout(), "FAILWITH: 'Key does not exist'")


class TestRecord(unittest.TestCase):
    def test_nesting_records(self):
        vm = VM()
        source = """
@dataclass
class KYC:
    address: String
    name: String

@dataclass
class Account:
    name: String
    balance: Nat
    kyc: KYC

@dataclass
class Storage:
    counter: Nat
    owner: Account

store = Storage(Nat(0), Account(String("pymich"), Nat(1000), KYC(String("kyc_address"), String("kyc_name"))))
"""
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        attr_ast = ast.parse("store.owner").body[0].value
        instructions = compiler._compile_attribute(attr_ast, compiler.env)
        micheline2 = CompilerBackend().compile_instructions(instructions)
        vm.execute(micheline)
        vm.execute(micheline2)
        self.assertEqual(vm.stack.items[0].__repr__(), "('pymich' * (1000 * ('kyc_address' * 'kyc_name')))")

        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        attr_ast = ast.parse("store.owner.kyc").body[0].value
        instructions = compiler._compile_attribute(attr_ast, compiler.env)
        micheline2 = CompilerBackend().compile_instructions(instructions)
        vm.execute(micheline)
        vm.execute(micheline2)
        self.assertEqual(vm.stack.peek().__repr__(), "('kyc_address' * 'kyc_name')")

        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        attr_ast = ast.parse("store.owner.kyc.name").body[0].value
        instructions = compiler._compile_attribute(attr_ast, compiler.env)
        micheline2 = CompilerBackend().compile_instructions(instructions)
        vm.execute(micheline)
        vm.execute(micheline2)
        self.assertEqual(vm.stack.peek().value, "kyc_name")

    def test_nested_record_create(self):
        source = """
@dataclass
class SubStorage:
    a: Nat
    b: Nat
    c: Nat

@dataclass
class Storage:
    info: SubStorage
    counter: Nat

storage = Storage(SubStorage(Nat(3), Nat(2), Nat(1)), Nat(4))
storage.info
a = storage.info.a
b = storage.info.b
c = storage.info.c
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)

        for var_name, expected_value in zip(["a", "b", "c"], [3, 2, 1]):
            storage_location = compiler.env.vars[var_name]
            self.assertEqual(vm.stack.items[-storage_location-1].to_python_object(), expected_value)

    def test_doubly_nested_record_update(self):
        source = """
@dataclass
class SubStorage:
    a: Nat
    b: Nat
    c: Nat

@dataclass
class Storage:
    info: SubStorage
    counter: Nat

storage = Storage(SubStorage(Nat(1), Nat(2), Nat(3)), Nat(4))
storage.info.a = storage.info.a + Nat(10)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.__repr__(), '[1, (1 * (2 * 3)), (11 * (2 * 3)), ((11 * (2 * 3)) * 4), (1 * (2 * 3))]')
        self.assertEqual(len(vm.stack), 5)

    def test_nested_dict_and_record_update(self):
        source = """
@dataclass
class SubSubStorage:
    a: Nat
    b: Nat
    c: Nat

@dataclass
class SubStorage:
    a: Nat
    b: Dict[Nat, SubSubStorage]
    c: Nat

@dataclass
class Storage:
    info: SubStorage
    counter: Nat

storage = Storage(SubStorage(Nat(1), Map[Nat, SubSubStorage](), Nat(2)), Nat(3))
storage.info.b[Nat(10)] = SubSubStorage(Nat(4), Nat(5), Nat(6))
storage.info.b[Nat(10)].b
storage.info.b[Nat(10)].b = Nat(11)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)

        storage_location = compiler.env.vars["storage"]
        expected_value = (1, {10: (4, 11, 6)}, 2, 3)
        self.assertEqual(vm.stack.items[-storage_location-1].to_python_object(), expected_value)

    def test_triply_nested_record_update(self):
        source = """
@dataclass
class SubSubStorage:
    a: Nat
    b: Nat
    c: Nat

@dataclass
class SubStorage:
    a: Nat
    sub_storage: SubSubStorage
    c: Nat

@dataclass
class Storage:
    info: SubStorage
    counter: Nat

storage = Storage(SubStorage(Nat(1), SubSubStorage(Nat(10), Nat(20), Nat(30)), Nat(2)), Nat(3))
storage.info.sub_storage.b = Nat(1000)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.items[2].__repr__(), '((1 * ((10 * (1000 * 30)) * 2)) * 3)')
        self.assertEqual(len(vm.stack), 5)

    def test_record_create(self):
        source = """
@dataclass
class Storage:
    a: Nat
    b: Nat
    c: Nat

storage = Storage(Nat(1), Nat(2), Nat(3))
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), (1, 2, 3))

    def test_record_dict_attr(self):
        source = """
@dataclass
class Storage:
    ages: Dict[String, Nat]
    b: Nat
    c: Nat

storage = Storage(Map[String, Nat](), Nat(2), Nat(3))
foo = String("yolo")
storage.ages[String("foobar")] = Nat(10)
storage.ages[String("foobar")]
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(
            vm.stack.__repr__(),
            "[10, {'foobar': 10}, {'foobar': 10}, 'yolo', ({'foobar': 10} * (2 * 3)), {}]",
        )

    def test_nested_record_dict_attr(self):
        source = """
@dataclass
class SubStorage:
    ages: Dict[String, Nat]
    b: Nat
    c: Nat

@dataclass
class Storage:
    sub_storage: SubStorage
    b: Nat

storage = Storage(SubStorage(Map[String, Nat](), Nat(1), Nat(2)), Nat(3))
foo = String("yolo")
storage
storage.sub_storage.ages[String("foobar")] = Nat(10)
storage.sub_storage.ages[String("foobar")]
"""
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        storage_location = compiler.env.vars["storage"]
        self.assertEqual(vm.stack.items[-storage_location-1].to_python_object(), ({'foobar': 10}, 1, 2, 3))

    def test_record_get(self):
        source = """
@dataclass
class Storage:
    a: Nat
    b: Nat
    c: Nat

storage = Storage(Nat(1), Nat(2), Nat(3))
storage.a
storage.b
storage.c
        """
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual([item.value for item in vm.stack.items[:-1]], [3, 2, 1])

    def test_record_set(self):
        source = """
@dataclass
class Storage:
    a: Nat
    b: Nat
    c: Nat

storage = Storage(Nat(1), Nat(2), Nat(3))
storage.a = Nat(10)
storage.b = Nat(20)
storage.c = Nat(30)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), (10, 20, 30))

    def test_record_as_function_argument(self):
        source = """
@dataclass
class Storage:
    a: Nat
    b: Nat
    c: Nat

def add(storage: Storage) -> Nat:
    return storage.a + storage.b + storage.c

add(Storage(Nat(1), Nat(2), Nat(3)))
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek(), NatType(6))

    def test_compile_get_record_attribute(self):
        def test(attribute_name, stack_top_value):
            source = f"""
@dataclass
class Storage:
    a: Nat
    b: Nat
    c: Nat
    d: Nat
    e: Nat
    f: Nat

a = Nat(2)
b = Nat(4)
c = Nat(6)
my_storage = Storage(Nat(1), a, Nat(3), b, Nat(5), c)
my_storage.{attribute_name}
"""
            micheline = Compiler(source).compile_expression()
            vm = VM()
            vm.execute(micheline)
            self.assertEqual(vm.stack.peek().value, stack_top_value)

        test("a", 1)
        test("b", 2)
        test("c", 3)
        test("d", 4)
        test("e", 5)
        test("f", 6)

    def test_compile_create_record(self):
        source = """
@dataclass
class Storage:
    a: Nat
    b: Nat
    c: Nat
    d: Nat
    e: Nat
    f: Nat

a = Nat(2)
b = Nat(4)
c = Nat(6)
my_storage = Storage(Nat(1), a, Nat(3), b, Nat(5), c)
d = Nat(7)
my_storage # get storage
"""
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), (1, 2, 3, 4, 5, 6))

    def test_get_record_entry(self):
        attribute_names = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"]
        attribute_values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

        def make_source(attribute_to_get):
            source = "@dataclass\nclass Record:\n"
            for attribute_name in attribute_names:
                source += f"    {attribute_name}: Nat\n"
            source += "record = Record("
            for attribute_value in attribute_values:
                source += f"Nat({attribute_value}), "
            source += f")\nrecord.{attribute_to_get}"
            return source

        for attribute_name, attribute_value in zip(attribute_names, attribute_values):
            source = make_source(attribute_name)
            micheline = Compiler(source).compile_expression()
            vm = VM()
            vm.execute(micheline)
            self.assertEqual(vm.stack.peek().value, attribute_value)


class TestInterContractCall(unittest.TestCase):
    def test_cast_contract_address_to_record_type(self):
        source = f"""
@dataclass
class TransferParam:
    _from: Address
    _to: Address
    value: Nat

fa12 = "KT1EwUrkbmGxjiRvmEAa8HLGhjJeRocqVTFi"
transfer_entrypoint = Contract[TransferParam](fa12, "%transfer")
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, "KT1EwUrkbmGxjiRvmEAa8HLGhjJeRocqVTFi%transfer")

    def test_cast_contract_address_to_composite_type(self):
        source = f"""
fa12 = "KT1EwUrkbmGxjiRvmEAa8HLGhjJeRocqVTFi"
transfer_entrypoint = Contract[List[Nat]](fa12, "%transfer")
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, "KT1EwUrkbmGxjiRvmEAa8HLGhjJeRocqVTFi%transfer")

    def test_operation_from_entrypoint(self):
        source = f"""
from dataclasses import dataclass
from pymich.michelson_types import *
from pymich.stdlib import *


@dataclass
class TransferParam(Record):
    _from: Address
    _to: Address
    value: Nat

class Proxy(Contract):
    admin: Address
    fa12: Address

    def proxy_transfer(self):
        transfer_entrypoint = Contract[TransferParam](self.fa12, "%transfer")

        transaction(
            transfer_entrypoint,
            Mutez(0),
            TransferParam(self.fa12, self.fa12, Nat(10)),
        )

    def change_admin(self, new_admin: Address):
        if Tezos.sender == self.admin:
            self.admin = new_admin
        else:
            raise Exception("Not allowed")
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        operations = vm.contract.proxy_transfer().interpret().operations
        expected_operations = [{
            'kind': 'transaction',
            'source': 'KT1BEqzn5Wx8uJrZNvuS9DVHmLvG9td3fDLi',
            'destination': 'KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT',
            'amount': '0',
            'parameters': {
                'entrypoint': 'transfer',
                'value': {
                    'prim': 'Pair',
                    'args': [
                        {'string': 'KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT'},
                        {'string': 'KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT'},
                        {'int': '10'}
                    ]
                }
            }
        }]
        self.assertEqual(operations, expected_operations)


def extract_function_body(lambda_micheline):
    return lambda_micheline[0]["args"][2]



class TestOperations(unittest.TestCase):
    def test_push_operation(self):
        source = f"""
class Contract:
    counter: Nat
    admin: Address

    def counter(self, x: Nat):
        y = x

    def make_op(self, x: Nat):
        __OPERATIONS__ = __OPERATIONS__.push(Contract[Unit](Tezos.sender), Tezos.amount, Unit())
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        actual_ops = vm.contract.make_op(1).interpret().operations
        expected_ops = [{'kind': 'transaction', 'source': 'KT1BEqzn5Wx8uJrZNvuS9DVHmLvG9td3fDLi', 'destination': 'tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU', 'amount': '0', 'parameters': {'entrypoint': 'default', 'value': {'prim': 'Unit'}}}]
        self.assertEqual(expected_ops, actual_ops)

    def test_function_pushes_operations(self):
        source = f"""
def transfer(ops: Operations) -> Operations:
        return ops.push(Contract[Unit](Tezos.sender), Tezos.amount, Unit())

class Contract(AbstractContract):
    counter: Nat
    admin: Address

    def counter(self, x: Nat):
        y = x

    def make_op(self, x: Nat):
        __OPERATIONS__ = transfer(__OPERATIONS__)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        actual_ops = vm.contract.make_op(1).interpret().operations
        expected_ops = [{'kind': 'transaction', 'source': 'KT1BEqzn5Wx8uJrZNvuS9DVHmLvG9td3fDLi', 'destination': 'tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU', 'amount': '0', 'parameters': {'entrypoint': 'default', 'value': {'prim': 'Unit'}}}]
        self.assertEqual(expected_ops, actual_ops)

class TestContract(unittest.TestCase):
    def test_upgradable(self):
        source = f"""
class Contract:
    counter: Nat
    f: Callable[[Nat], Nat]

    def update_f(self, f: Callable[[Nat], Nat]):
        self.f = f

    def update_counter(self, x: Nat):
        self.counter = self.f(x)
        """
        double_source = f"""
def doublef(x: Nat) -> Nat:
    return x * Nat(2)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)

        double_micheline = Compiler(double_source).compile_expression()
        init_storage = vm.contract.storage.dummy()
        init_storage["f"] = micheline_to_michelson(extract_function_body(double_micheline))

        new_storage = vm.contract.update_counter(10).interpret(storage=init_storage).storage
        self.assertEqual(new_storage["counter"], 20)

    def test_amount(self):
        source = f"""
class Contract:
    amount: Mutez
    admin: Address

    def update_counter(self):
        self.amount = Tezos.amount

    def get_counter(self):
        transaction(Contract[Unit](Tezos.sender), Tezos.amount, Unit())
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        amount = 10
        new_storage = vm.contract.update_counter().interpret(amount=amount).storage
        self.assertEqual(new_storage["amount"], amount)
        ops = vm.contract.get_counter().interpret(amount=amount, sender=vm.context.sender).operations
        self.assertEqual(len(ops), 1)
        self.assertEqual(ops[0]['destination'], vm.context.sender)
        self.assertEqual(ops[0]['amount'], str(amount))

    def test_callback_view_no_param(self):
        source = f"""
class Contract:
    counter: Nat
    admin: Address

    def update_counter(self, new_counter: Nat):
        self.counter = new_counter

    def get_counter(self, contract: Contract[Nat]):
        transaction(contract, Mutez(0), self.counter)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        self.assertEqual(vm.contract.get_counter().callback_view(), 0)

    def test_callback_view_with_param(self):
        source = f"""
class Contract:
    counters: Dict[String, Nat]
    admin: Address

    def update_counter(self, counter_name: String, count: Nat):
        self.counters[counter_name] = count

    def get_counter(self, counter_name: String, contract: Contract[Nat]):
        transaction(contract, Mutez(0), self.counters.get(counter_name, Nat(0)))
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        self.assertEqual(vm.contract.get_counter({"counter_name": "foo", "contract_1": None}).callback_view(), 0)

    def test_callback_view_macro_with_param(self):
        source = f"""
class Contract:
    counters: Dict[String, Nat]
    admin: Address

    def update_counter(self, counter_name: String, count: Nat):
        self.counters[counter_name] = count

    def get_counter(self, counter_name: String) -> Nat:
        return self.counters.get(counter_name, Nat(0))
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        init_storage = {"admin": vm.context.sender, "counters": {"foo": 10}}
        res = vm.contract.get_counter({"counter_name": "foo", "contract_1": None}).interpret(storage=init_storage)
        self.assertEqual(res.operations, [{'kind': 'transaction', 'source': 'KT1BEqzn5Wx8uJrZNvuS9DVHmLvG9td3fDLi', 'destination': 'KT1BEqzn5Wx8uJrZNvuS9DVHmLvG9td3fDLi', 'amount': '0', 'parameters': {'entrypoint': 'default', 'value': {'int': '10'}}}])
        self.assertEqual(res.storage, init_storage)

    def test_storage_inside_contract(self):
        source = f"""
class Contract:
    counter: Nat
    admin: Address

    def update_counter(self, new_counter: Nat) :
        self.counter = new_counter

    def update_admin(self, new_admin: Address):
        self.admin = new_admin
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        self.assertEqual(vm.contract.update_counter(10).interpret().storage['counter'], 10)
        self.assertEqual(vm.contract.update_admin(vm.context.sender).interpret().storage['admin'], vm.context.sender)

    def test_final(self):
        source = f"""
from dataclasses import dataclass
from typing import Dict
from . stubs import *


def require(condition: Boolean, message: String) -> Nat:
    if not condition:
        raise Exception(message)

    return Nat(0)

@dataclass
class Contract:
    balances: Dict[Address, Nat]
    total_supply: Nat
    admin: Address

    def mint(self, to: Address, amount: Nat):
        require(Tezos.sender == self.admin, String("Only admin can mint"))

        self.total_supply = self.total_supply + amount

        if to in self.balances:
            self.balances[to] = self.balances[to] + amount
        else:
            self.balances[to] = amount

    def transfer(self, to: Address, amount: Nat):
        require(amount > Nat(0), String("You need to transfer a positive amount of tokens"))
        require(self.balances[Tezos.sender] >= amount, String("Insufficient sender balance"))

        self.balances[Tezos.sender] = abs(self.balances[Tezos.sender] - amount)

        if to in self.balances:
            self.balances[to] = self.balances[to] + amount
        else:
            self.balances[to] = amount
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        init_storage = vm.contract.storage.dummy()
        init_storage['admin'] = vm.context.sender

        new_storage = vm.contract.mint({"to": vm.context.sender, "amount": 10}).interpret(storage=init_storage, sender=vm.context.sender).storage
        self.assertEqual(new_storage['balances'], {vm.context.sender: 10})

        try:
            vm.contract.mint({"to": vm.context.sender, "amount": 10}).interpret(storage=init_storage).storage
            assert 0
        except MichelsonRuntimeError as e:
            self.assertEqual(e.format_stdout(), "FAILWITH: 'Only admin can mint'")

        investor = "KT1EwUrkbmGxjiRvmEAa8HLGhjJeRocqVTFi"
        new_storage = vm.contract.transfer({"to": investor, "amount": 4}).interpret(storage=new_storage, sender=vm.context.sender).storage
        self.assertEqual(new_storage['balances'], {vm.context.sender: 6, investor: 4})

        try:
            vm.contract.transfer({"to": investor, "amount": 0}).interpret(storage=new_storage).storage
            assert 0
        except MichelsonRuntimeError as e:
            self.assertEqual(e.format_stdout(), "FAILWITH: 'You need to transfer a positive amount of tokens'")

        try:
            vm.contract.transfer({"to": investor, "amount": 10}).interpret(storage=new_storage, sender=vm.context.sender).storage
            assert 0
        except MichelsonRuntimeError as e:
            self.assertEqual(e.format_stdout(), "FAILWITH: 'Insufficient sender balance'")


    def test_multi_arg_entrypoint_or_function(self):
        source = f"""
def require(condition: Boolean, message: String) -> Nat:
    if not condition:
        raise Exception(message)

    return Nat(0)

class Contract:
    a: Nat
    b: Nat

    def add_positives(self, x: Nat, y: Nat):
        require(x > Nat(0), String("x is not a positive integer"))
        self.a = x + y

    def sub(self, x: Nat):
        self.a = x
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        init_storage = vm.contract.storage.dummy()
        expected_storage = 5
        actual_storage = vm.contract.add_positives({"x": 2, "y": 3}).interpret(storage=init_storage).storage
        self.assertEqual(actual_storage["a"], expected_storage)

    def test_dataclass_entrypoint_param_1(self):
        source = f"""
@dataclass
class AddParam:
    x: Nat
    y: Nat

class Contract:
    x: Nat
    y: Nat

    def add(param: AddParam):
        self.x = param.x + param.y

    def sub(x: Nat):
        self.y = x
        """
        micheline = Compiler(source).compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        init_storage = vm.contract.storage.dummy()
        expected_storage = 5
        actual_storage = vm.contract.add({"x": 2, "y": 3}).interpret(storage=init_storage).storage
        self.assertEqual(actual_storage["x"], expected_storage)

    def test_dataclass_entrypoint_param_2(self):
        source = """
@dataclass
class ChangeSupplyParam:
    to: Address
    amount: Nat

class Contract:
    balances: Dict[Address, Nat]
    total_supply: Nat

    def mint(param: ChangeSupplyParam):
        self.total_supply = self.total_supply + param.amount

        balances = self.balances

        if param.to in balances:
            balances[param.to] = balances[param.to] + param.amount
        else:
            balances[param.to] = param.amount

        self.balances = balances

    def burn(param: ChangeSupplyParam):
        balances = self.balances
        self.total_supply = (self.total_supply - param.amount).is_nat().get_with_default(Nat(0))
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        init_storage = vm.contract.storage.dummy()
        tr_to, amount = vm.context.sender, 10
        new_storage = vm.contract.mint({"to": tr_to, "amount": amount}).interpret(storage=init_storage).storage
        expected_storage = {
            "balances": {
                tr_to: amount,
            },
            "total_supply": 10,
        }
        self.assertEqual(new_storage, expected_storage)

    def test_condition_in_function(self):
        source = f"""
@dataclass
class AddParam:
    x: Nat
    y: Nat

def foo(param: AddParam) -> Nat:
    x = param.x
    y = param.y
    if x == param.y:
        x = x + x + x
    else:
        x = x + Nat(10)
    return x + y

class Contract:
    a: Nat
    b: Nat

    def add(param: AddParam):
        self.a = self.a + foo(param)

    def sub(x: Nat):
        self.b = x
        """
        micheline = Compiler(source).compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        init_storage = vm.contract.storage.dummy()
        new_storage = vm.contract.add({"x": 2, "y": 2}).interpret(storage=init_storage).storage
        self.assertEqual(new_storage["a"], 8)
        new_storage = vm.contract.add({"x": 2, "y": 3}).interpret(storage=init_storage).storage
        self.assertEqual(new_storage["a"], 15)

    def test_function(self):
        vm = VM()
        admin = vm.context.sender
        source = f"""
@dataclass
class RequireArg:
    condition: Boolean
    message: String

def require(param: RequireArg) -> Nat:
    if not param.condition:
        raise Exception(param.message)

    return Nat(0)

def double(x: Nat) -> Nat:
    return x + x

def triple(x: Nat) -> Nat:
    return x + x + x

class Contract:
    admin: Address
    counter: Nat

    def add(param: Nat):
        _ = require(RequireArg(Tezos.sender == self.admin, String("Only owner can call open")))

        self.counter = self.counter + param

    def sub(param: Nat):
        if Tezos.sender != self.admin:
            raise Exception("Only owner can call open")

        self.counter = abs(self.counter - param)

    def quintuple(param: Nat):
        _ = require(RequireArg(Tezos.sender == self.admin, String("Only owner can call open")))

        self.counter = double(self.counter) + triple(self.counter)
        """
        micheline = Compiler(source).compile_contract()
        vm.load_contract(micheline)
        init_storage = vm.contract.storage.dummy()
        init_storage['admin'] = admin
        new_storage = vm.contract.add(1).interpret(storage=init_storage, sender=vm.context.sender).storage
        expected_storage = {
            'admin': admin,
            'counter': 1,
        }
        self.assertEqual(new_storage, expected_storage)

        init_storage = expected_storage
        new_storage = vm.contract.sub(1).interpret(storage=init_storage, sender=vm.context.sender).storage
        expected_storage = {
            'admin': admin,
            'counter': 0,
        }
        self.assertEqual(new_storage, expected_storage)

        init_storage = expected_storage
        init_storage['counter'] = 1

        new_storage = vm.contract.quintuple(1).interpret(storage=init_storage, sender=vm.context.sender).storage
        expected_storage = {
            'admin': admin,
            'counter': 5,
        }
        self.assertEqual(new_storage, expected_storage)

    def test_election(self):
        source = f"""
def require(condition: Boolean, message: String) -> Nat:
    if not condition:
        raise Exception(message)

    return Nat(0)


@dataclass
class Contract:
    admin: Address
    manifest_url: String
    manifest_hash: String
    _open: String
    _close: String
    artifacts_url: String
    artifacts_hash: String

    def open(self, _open: String, manifest_url: String, manifest_hash: String):
        require(Tezos.sender == self.admin, String("Only admin can call this entrypoint"))
        self._open = _open
        self.manifest_url = manifest_url
        self.manifest_hash = manifest_hash

    def close(self, _close: String):
        require(Tezos.sender == self.admin, String("Only admin can call this entrypoint"))
        self._close = _close

    def artifacts(self, artifacts_url: String, artifacts_hash: String):
        require(Tezos.sender == self.admin, String("Only admin can call this entrypoint"))
        self.artifacts_url = artifacts_url
        self.artifacts_hash = artifacts_hash
        """
        compiler = Compiler(source)
        micheline = compiler.compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        init_storage = vm.contract.storage.dummy()
        init_storage['admin'] = vm.context.sender

        try:
            vm.contract.open({"_open": "foo", "manifest_url": "bar", "manifest_hash": "baz"}).interpret(storage=init_storage)
            assert 0
        except MichelsonRuntimeError as e:
            self.assertEqual(e.format_stdout(), "FAILWITH: 'Only admin can call this entrypoint'")

        new_storage = vm.contract.open({"_open": "foo", "manifest_url": "bar", "manifest_hash": "baz"}).interpret(storage=init_storage, sender=vm.context.sender).storage
        expected_storage = init_storage.copy()
        expected_storage["_open"] = "foo"
        expected_storage["manifest_url"] = "bar"
        expected_storage["manifest_hash"] = "baz"
        self.assertEqual(new_storage, expected_storage)

        expected_storage["_close"] = "foobar"
        new_storage = vm.contract.close("foobar").interpret(storage=new_storage, sender=vm.context.sender).storage
        self.assertEqual(new_storage, expected_storage)

        new_storage = vm.contract.artifacts({"artifacts_url": "1", "artifacts_hash": "2"}).interpret(storage=new_storage, sender=vm.context.sender).storage
        expected_storage["artifacts_url"] = "1"
        expected_storage["artifacts_hash"] = "2"
        self.assertEqual(new_storage, expected_storage)

    def test_attribute_reassign(self):
        source = """
class Contract:
    a: Nat
    b: Nat
    c: Nat

    def set_a(new_a: Nat):
        self.a = new_a

    def set_b(new_b: Nat):
        self.b = new_b

    def set_c(new_c: Nat):
        self.c = new_c
        """
        micheline = Compiler(source).compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        init_storage = vm.contract.storage.dummy()
        new_storage = vm.contract.set_a(10).interpret(storage=init_storage).storage
        expected_storage = init_storage
        expected_storage["a"] = 10
        self.assertEqual(new_storage, expected_storage)

        new_storage = vm.contract.set_b(20).interpret(storage=expected_storage).storage
        expected_storage["b"] = 20
        self.assertEqual(new_storage, expected_storage)

        new_storage = vm.contract.set_c(30).interpret(storage=expected_storage).storage
        expected_storage["c"] = 30
        self.assertEqual(new_storage, expected_storage)


    def test_contract_final(self):
        vm = VM()
        owner = vm.context.sender
        source = f"""
class Contract:
    owner: Address
    name: String
    counter: Nat

    def add(a: Nat):
        if a < Nat(10):
            raise Exception('input smaller than 10')
        else:
            a = a + a
            self.counter = self.counter + a

    def update_owner(new_owner: Address):
        self.owner = new_owner

    def update_name(new_name: String):
        self.name = new_name
        """
        micheline = Compiler(source).compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        init_storage = vm.contract.storage.dummy()

        try:
            vm.contract.add(1).interpret(storage=init_storage).storage
            assert 0
        except MichelsonRuntimeError as e:
            self.assertEqual(e.format_stdout(), "FAILWITH: 'input smaller than 10'")

        new_storage = vm.contract.add(10).interpret(storage=init_storage).storage
        expected_storage = init_storage
        expected_storage["counter"] = 20
        self.assertEqual(new_storage, expected_storage)

        ZERO_ADDRESS = "tz1burnburnburnburnburnburnburjAYjjX"
        new_storage = vm.contract.update_owner(ZERO_ADDRESS).interpret(storage=new_storage).storage
        expected_storage["owner"] = ZERO_ADDRESS
        self.assertEqual(new_storage, expected_storage)

        new_storage = vm.contract.update_name("bar").interpret(storage=new_storage).storage
        expected_storage["name"] = "bar"
        self.assertEqual(new_storage, expected_storage)

    def contract_multitype_storage(self):
        source = """
@dataclass
class Storage:
    owner: str
    counter: Nat

class Contract:
    def add(self, a: Nat):
        self.counter = self.counter + a)

    def update_owner(self, new_owner: str):
        self.owner = new_owner
        """
        micheline = Compiler(source).compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        init_storage = vm.contract.storage.dummy()

        new_storage = vm.contract.add(10).interpret(storage=init_storage).storage
        expected_storage = init_storage
        expected_storage["counter"] = 10
        self.assertEqual(new_storage, expected_storage)

        new_storage = vm.contract.update_owner("foo").interpret(storage=init_storage).storage
        expected_storage["owner"] = "foo"
        self.assertEqual(new_storage, expected_storage)

    def test_contract_storage(self):
        source = """
class Contract:
    owner_id: Nat
    counter: Nat

    def add(self, a: Nat):
        b = Nat(10)
        self.counter = self.counter + a + b

    def update_owner_id(self, new_id: Nat):
        self.owner_id = new_id
        """
        micheline = Compiler(source).compile_contract()
        vm = VM()
        vm.load_contract(micheline)
        init_storage = vm.contract.storage.dummy()

        new_storage = vm.contract.add(10).interpret(storage=init_storage).storage
        expected_storage = init_storage
        expected_storage["counter"] = 20
        self.assertEqual(new_storage, expected_storage)

        new_storage = vm.contract.update_owner_id(10).interpret(storage=init_storage).storage
        expected_storage["owner_id"] = 10
        self.assertEqual(new_storage, expected_storage)


class TestCompilerList(unittest.TestCase):
    def test_create_empty_michelson_list(self):
        source = "my_list: List[Nat] = List()"
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual([el.value for el in vm.stack.peek().items], [])

    def test_create_empty_michelson_list_2(self):
        source = "my_list = List[Nat]()"
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual([el.value for el in vm.stack.peek().items], [])

    def test_create_michelson_list_prepend_from_variable(self):
        source = """
my_list = List[Nat]()
my_list.prepend(Nat(1))
"""
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual([el.value for el in vm.stack.items[1].items], [])
        self.assertEqual([el.value for el in vm.stack.peek().items], [1])

    def test_create_michelson_list_prepend_from_constructor(self):
        source = "List[Nat]().prepend(Nat(1)).prepend(Nat(2)).prepend(Nat(3)).prepend(Nat(4))"
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual([el.value for el in vm.stack.peek().items], [4, 3, 2, 1])

    def test_create_michelson_list_prepend_from_empty_constructor(self):
        source = "List[Nat]().prepend(Nat(3)).prepend(Nat(4))"
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual([el.value for el in vm.stack.peek().items], [4, 3])

    def test_create_typed_list(self):
        source = "my_list: List[Nat] = []"
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual([el.value for el in vm.stack.peek().items], [])

    def test_nested_list(self):
        source = """
b = [Nat(1), Nat(1)]
[String("foo"), String("bar")]
[[Nat(1), Nat(0)], [Nat(0), Nat(1)]]
[{Nat(1): Nat(0)}, {Nat(0): Nat(2)}]
"""
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.items[0].to_python_object(), [{1: 0}, {0: 2}])
        self.assertEqual(vm.stack.items[1].to_python_object(), [[1, 0], [0, 1]])
        self.assertEqual(vm.stack.items[2].to_python_object(), ["foo", "bar"])
        self.assertEqual(vm.stack.items[3].to_python_object(), [1, 1])

    def todo_test_create_list(self):
        # should throw error since empty list is untyped
        source = "[]"
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual([el.value for el in vm.stack.peek().items], [])

    def test_list_instanciation(self):
        source = """
[Nat(1), Nat(2), Nat(3)]
[String("hello"), String("world")]
        """
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual([el.value for el in vm.stack.peek().items], ["hello", "world"])
        self.assertEqual([el.value for el in vm.stack.items[1].items], [1, 2, 3])

class TestCompilerAssign(unittest.TestCase):
    def test_reassign(self):
        source = """
a = Nat(1)
b = Nat(2)
a = a + Nat(2)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual([item.value for item in vm.stack.items], [2, 3])
        self.assertEqual(compiler.env.vars["a"], 0)
        self.assertEqual(compiler.env.vars["b"], 1)


class TestCompilerDefun(unittest.TestCase):
    def test_func_def(self):
        source = """baz = Nat(1)
def foo(a: Nat) -> Nat:
    b = Nat(2)
    return a + b + Nat(3)
bar = foo(baz)
fff = foo(bar)
foo(foo(bar))
"""
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)

        storage_location = compiler.env.vars["bar"]
        self.assertEqual(vm.stack.items[-storage_location-1].to_python_object(), 6)

        storage_location = compiler.env.vars["fff"]
        self.assertEqual(vm.stack.items[-storage_location-1].to_python_object(), 11)

        self.assertEqual(vm.stack.peek().value, 16)


class TestCompilerIntegration(unittest.TestCase):
    def test_function_in_record(self):
        source = """
@dataclass
class Point:
    x: Nat
    y: Nat

@dataclass
class Storage:
    point: Point
    double: Callable[[Point], Point]

def double(p: Point) -> Point:
    return Point(p.x * Nat(2), p.y * Nat(2))

storage = Storage(Point(Nat(1), Nat(2)), double)
p = Point(Nat(1), Nat(2))
result = storage.double(p)
"""
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        storage_location = compiler.env.vars["result"]
        self.assertEqual(vm.stack.items[-storage_location-1].to_python_object(), (2, 4))

    def test_function_in_record_returns_dict(self):
        source = """
@dataclass
class Point(Record):
    x: Nat
    y: Nat

@dataclass
class Storage:
    point: Point
    double: Callable[[Point], Map[String, Point]]

def double(p: Point) -> Map[String, Point]:
    new_p = Point(p.x * Nat(2), p.y * Nat(2))
    return Map[String, Point]()   \
        .add(String("original"), p) \
        .add(String("double"), new_p)

storage = Storage(Point(Nat(1), Nat(2)), double)
p = Point(Nat(1), Nat(2))
original = storage.double(p)[String("original")]
double = storage.double(p)[String("double")]
"""
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)

        storage_location = compiler.env.vars["original"]
        self.assertEqual(vm.stack.items[-storage_location-1].to_python_object(), (1, 2))

        storage_location = compiler.env.vars["double"]
        self.assertEqual(vm.stack.items[-storage_location-1].to_python_object(), (2, 4))

    def test_point(self):
        source = """
@dataclass
class Point:
    x: Nat
    y: Nat

points: Dict[Nat, Point] = {}
for key in [Nat(0), Nat(1), Nat(2), Nat(3)]:
    points[key] = Point(Nat(0), Nat(0))
points[Nat(2)]  # ==> Point(Nat(0), Nat(0))
"""
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.items[0].to_python_object(), (0, 0))
        self.assertEqual(vm.stack.items[1].to_python_object(), [0, 1, 2, 3])
        self.assertEqual(vm.stack.items[2].to_python_object(), {0: (0, 0), 1: (0, 0), 2: (0, 0), 3: (0, 0)})

    def test_change_iterable_element_in_list(self):
        source = """
my_dict: Dict[Nat, Nat] = {}
for key in [Nat(0), Nat(1), Nat(2), Nat(3)]:
    my_dict[key] = key + Nat(1)
my_dict[Nat(2)]  # ==> 3
"""
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.items[2].to_python_object(), {0: 1, 1: 2, 2: 3, 3: 4})
        self.assertEqual(vm.stack.items[1].to_python_object(), [0, 1, 2, 3])
        self.assertEqual(vm.stack.items[0].to_python_object(), 3)

    def test_push_address(self):
        user_address = "tz1S792fHX5rvs6GYP49S1U58isZkp2bNmn6"
        contract_address = "KT1EwUrkbmGxjiRvmEAa8HLGhjJeRocqVTFi"
        regular_string = "foobar"
        source = f"""
user_address = "{user_address}"
contract_address = Address("{contract_address}")
contract_address_2 = Address("{contract_address}")
regular_string = "{regular_string}"
        """
        c = Compiler(source, is_debug=False)
        instructions = [instr for instr in c._compile(c.ast) if instr.name != "COMMENT"]
        expected_instructions = [
            Instr("PUSH", [t.Address(), user_address], {}),
            Instr("PUSH", [t.Address(), contract_address], {}),
            Instr("PUSH", [t.Address(), contract_address], {}),
            Instr("PUSH", [t.String(), regular_string], {}),
        ]
        self.assertEqual(instructions, expected_instructions)

    def test_store_vars_and_add(self):
        source = """
a = Nat(1)
b = Nat(2)
c = a + b + b
a + b + c
        """
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.items, [NatType(i) for i in [8, 3, 5, 3, 2, 1]])

    def test_push_string(self):
        source = "'foobar'"
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.items, [StringType("foobar")])

    def test_compare(self):
        source = "Nat(1) < Nat(2)"
        c = Compiler(source, is_debug=False)
        instructions = c._compile(c.ast)
        expected_instructions = [
            Instr("PUSH", [t.Nat(), 2], {}),
            Instr("PUSH", [t.Nat(), 1], {}),
            Instr("COMPARE", [], {}),
            Instr("LT", [], {}),
        ]
        self.assertEqual(instructions, expected_instructions)

        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.items, [BoolType(True)])

    def test_if(self):
        source = """
if Nat(1) < Nat(2):
    "foo"
else:
    "bar"
        """
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.items, [StringType("foo"), BoolType(True)])

    def test_if_reassign(self):
        source = """
foo = "foo"
if  Nat(1) < Nat(2):
    foo = "bar"
else:
    foo = "baz"
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.items, [BoolType(True), StringType("bar")])
        self.assertEqual(compiler.env.vars["foo"], 0)

    def test_if_failwith(self):
        source = """
foo = "foo"
if Nat(1) < Nat(2):
    raise Exception("my error")
else:
    foo = "baz"
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        try:
            vm.execute(micheline)
            assert 0
        except MichelsonRuntimeError as e:
            self.assertEqual(e.format_stdout(), "FAILWITH: 'my error'")

    def test_raise(self):
        source = "raise Exception('foobar')"
        compiler = Compiler(source, is_debug=False)
        instructions = compiler._compile(compiler.ast)
        expected_instructions = [
            Instr("PUSH", [t.String(), 'foobar'], {}),
            Instr("FAILWITH", [], {})
        ]
        self.assertEqual(instructions, expected_instructions)

        micheline = compiler.compile_expression()
        vm = VM()
        try:
            vm.execute(micheline)
            assert 0
        except MichelsonRuntimeError as e:
            self.assertEqual(e.format_stdout(), "FAILWITH: 'foobar'")

    def test_reassign_in_condition(self):
        def get_source(a):
            return f"""
a = Nat({a})
if a > Nat(0):
    a = Nat(11)
else:
    a = Nat(12)
            """
        micheline = Compiler(get_source(10)).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.items, [BoolType(True), NatType(11)])

        micheline = Compiler(get_source(0)).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.items, [BoolType(False), NatType(12)])

    def test_sender(self):
        source = "Tezos.sender"
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek(), AddressType.from_value(vm.context.sender))

    def test_self_address(self):
        source = "Tezos.self_address"
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek(), AddressType.from_value(vm.context.address))

    def test_source(self):
        source = "Tezos.source"
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.context.source = vm.context.sender
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek(), AddressType.from_value(vm.context.source))

    def test_booleans(self):
        vm = VM()

        for boolean in (True, False):
            source = str(boolean)
            micheline = Compiler(source).compile_expression()
            vm.execute(micheline)
            self.assertEqual(vm.stack.peek(), BoolType(boolean))

    def test_not(self):
        source = "not True"
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek(), BoolType(False))

    def test_and(self):
        sources = [
            ("True and False", False),
            ("True and True", True),
            ("False and False", False),
            ("False and True", False),
        ]
        for source, answer in sources:
            micheline = Compiler(source).compile_expression()
            vm = VM()
            vm.execute(micheline)
            self.assertEqual(vm.stack.peek().to_python_object(), answer)

    def test_import(self):
        source = """
from pymich import *
False
        """
        micheline = Compiler(source).compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek(), BoolType(False))

    def test_callback(self):
        source = """
def apply(f: Callable[[Nat], Nat], x: Nat) -> Nat:
    return f(x)

def increment(x: Nat) -> Nat:
    return x + Nat(1)

apply(increment, Nat(10))
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek(), NatType(11))

    def test_natural_number(self):
        source = """
d = {Nat(1): Nat(1)}
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), {1: 1})

    def test_pack(self):
        source = """
Bytes("hello")
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, b'\x05\x01\x00\x00\x00\x05hello')

    def test_unpack(self):
        source = """
bytes = Bytes(String("hello"))
result = bytes.unpack(String)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, "hello")
        self.assertEqual(compiler.env.types["result"], t.String())

        source = """
result = Bytes(String("hello")).unpack(String)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, "hello")
        self.assertEqual(compiler.env.types["result"], t.String())

        source = """
Bytes(String("hello")).unpack(Nat)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()

        with self.assertRaises(MichelsonRuntimeError) as e:
            vm.execute(micheline)

        self.assertEqual(e.exception.args[2], "'Cannot unpack'")

        source = """
Bytes(Map[String, Int]().add(String("hey"), Int(10))).unpack(Map[String, Int])
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), {"hey": 10})


        source = """
k = String("hey")
v = Bytes(Int(10))
m = Map[String, Bytes]().add(k, v)
result = m[k].unpack(Int)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 10)
        self.assertEqual(compiler.env.types["result"], t.Int())


        source = """
k = String("hey")
v = Map[Int, Int]().add(Int(10), Int(10))
m = Map[String, Bytes]().add(k, Bytes(v))
m[k].unpack(Map[Int, Int])[Int(10)]
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 10)

    def skip_test_blake2b(self):
        source = """
blake2b(pack("hello"))
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, b'\x05\n\x00\x00\x00\x0b\x05\x01\x00\x00\x00\x05hello')

    def test_floor_div(self):
        source = """
Nat(5) // Nat(2)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, 2)

    def test_floor_div_by_0(self):
        source = """
Nat(5) // Nat(0)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        try:
            vm.execute(micheline)
            assert 0
        except MichelsonRuntimeError as e:
            self.assertEqual(e.format_stdout(), "FAILWITH: 'Division by 0 not allowed'")

    def test_modulo(self):
        source = """
Nat(9) % Nat(5)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().value, 4)

    def test_modulo_by_0(self):
        source = """
Nat(5) % Nat(0)
        """
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm = VM()
        try:
            vm.execute(micheline)
            assert 0
        except MichelsonRuntimeError as e:
            self.assertEqual(e.format_stdout(), "FAILWITH: 'Division by 0 not allowed'")


class TestDictIR(unittest.TestCase):
    def test_get_nested_expr(self):
        source = """
tmp_var_0 = Map[Nat, Nat]()
tmp_var_1 = tmp_var_0.add
tmp_var_2 = Nat(1) + Nat(2)
tmp_var_3 = tmp_var_1(tmp_var_2, Nat(4))
tmp_var_4 = tmp_var_3.get
x = tmp_var_4(tmp_var_2, Nat(0))
y = tmp_var_4(Nat(10), Nat(100))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(str(vm.stack.items), str([100, 4, {3: 4}, {3: 4}, 3, {}, {}]))

    def test_set_nested_expr(self):
        source = """
tmp_var_0 = Map[Nat, Nat]()
tmp_var_1 = tmp_var_0.add
tmp_var_2 = Nat(1) + Nat(2)
tmp_var_1(tmp_var_2, Nat(4))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(str(vm.stack.items), str([{3: 4}, 3, {}, {}]))

class TestNestedRecordIR(unittest.TestCase):
    def test_nest_record_as_tac(self):
        source = """
@dataclass
class Bar(Record):
    baz: Nat
    bazz: Nat

@dataclass
class Foo(Record):
    bar: Bar
    barr: Nat

baz = Nat(1)
bar = Bar(baz, Nat(1))
foo = Foo(bar, Nat(1))

# IR equivalent of `foo.bar.baz = Nat(1)`
tmp_var_0 = foo.bar
tmp_var_0.baz = Nat(2)
foo.bar = tmp_var_0
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(str(vm.stack.items), "[(2 * 1), ((2 * 1) * 1), (1 * 1), 1]")


class TestNumbers(unittest.TestCase):
    def test_nat_to_int(self):
        source = """
n = Nat(1)
i = n.to_int()
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(isinstance(vm.stack.items[0], IntType), True)
        self.assertEqual(isinstance(vm.stack.items[1], NatType), True)
        self.assertEqual(compiler.env.types["i"], t.Int())

    def test_is_nat_true(self):
        source = """
i = Int(1)
optional = i.is_nat()
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(
            vm.stack.peek().to_micheline_value(),
            {'prim': 'Some', 'args': [{'int': '1'}]},
        )
        self.assertEqual(compiler.env.types["optional"], t.Option(t.Nat()))

    def test_is_nat_false(self):
        source = """
i = Int(-1)
optional = i.is_nat()
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(
            vm.stack.peek().to_micheline_value(),
            {'prim': 'None'},
        )
        self.assertEqual(compiler.env.types["optional"], t.Option(t.Nat()))

    def test_nat_subtraction(self):
        source = """
i = Nat(10) - Nat(5)
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(compiler.env.types["i"], t.Int())

    def test_complex_nat_casting(self):
        source = """
i = (Nat(10) - Nat(5)).is_nat().get_with_default(Nat(0))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(compiler.env.types["i"], t.Nat())

    def test_complex_nat_casting_2(self):
        source = """
a = Nat(10)
b = Nat(1)
i = (a - b).is_nat().get_with_default(Nat(0))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(compiler.env.types["i"], t.Nat())

class TestOption(unittest.TestCase):
    def test_instantiate_some(self):
        source = """
some = Option(Int(10))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(
            vm.stack.peek().to_micheline_value(),
            {'prim': 'Some', 'args': [{'int': '10'}]},
        )
        self.assertEqual(compiler.env.types["some"], t.Option(t.Int()))

    def test_instantiate_some_infer(self):
        source = """
some = Option(Nat(10) - Nat(5))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(
            vm.stack.peek().to_micheline_value(),
            {'prim': 'Some', 'args': [{'int': '5'}]},
        )
        self.assertEqual(compiler.env.types["some"], t.Option(t.Int()))

    def test_instantiate_none(self):
        source = """
none = Option[Nat](None)
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(
            vm.stack.peek().to_micheline_value(),
            {'prim': 'None'},
        )
        self.assertEqual(compiler.env.types["none"], t.Option(t.Nat()))

    def test_some_get(self):
        source = """
some = Option(Int(10))
some_get = some.get("option is empty")
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(
            vm.stack.peek().to_micheline_value(),
            {'int': '10'},
        )
        self.assertEqual(compiler.env.types["some_get"], t.Int())

    def test_none_get(self):
        source = """
none = Option[Int](None)
none.get("option is empty")
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()

        with self.assertRaises(MichelsonRuntimeError) as e:
            vm.execute(micheline)

        self.assertEqual(e.exception.format_stdout(), "FAILWITH: 'option is empty'")

    def test_some_get_with_default(self):
        source = """
some = Option(Int(10))
some_get = some.get_with_default(Int(0))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(
            vm.stack.peek().to_micheline_value(),
            {'int': '10'},
        )
        self.assertEqual(compiler.env.types["some_get"], t.Int())

    def test_none_get_with_default(self):
        source = """
none = Option[Int](None)
default = none.get_with_default(Int(0))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(
            vm.stack.peek().to_micheline_value(),
            {'int': '0'},
        )
        self.assertEqual(compiler.env.types["default"], t.Int())


class TestCondition(unittest.TestCase):
    def test_return_after_if(self):
        source = """
def return_nat(numerator: Nat, denominator: Nat) -> Nat:
    if denominator == Nat(0):
        raise Exception("DIV by 0")

    return Nat(1)

return_nat(Nat(1), Nat(2))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 1)

    def test_return_in_if(self):
        source = """
def return_nat(numerator: Nat) -> Nat:
    if True:
        return Nat(0)
    else:
        return Nat(1)

return_nat(Nat(2))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)


class TestTezos(unittest.TestCase):
    def test_tezos_amount_assign(self):
        source = """
a = Tezos.amount
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertTrue(isinstance(compiler.env.types["a"], t.Mutez))
        self.assertEqual(compiler.env.vars["a"], 0)
        self.assertEqual(vm.stack.peek().to_python_object(), 0)

    def test_tezos_amount(self):
        source = """
Tezos.amount
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 0)

    def test_tezos_sender(self):
        source = """
Tezos.sender
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), vm.context.sender)

    def test_tezos_sender(self):
        source = """
Tezos.balance
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 0)

    def test_tezos_sender(self):
        source = """
Tezos.self_address
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), vm.context.address)

    def test_tezos_sender(self):
        source = """
def f(x: Mutez) -> Mutez:
    return x
f(Tezos.amount)
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 0)

import logging
logging.getLogger("pytezos").setLevel(logging.DEBUG)

class TestClosures(unittest.TestCase):
    def test_single_closure(self):
        source = """
a = Nat(2)
def add_a(x: Nat) -> Nat:
   return x + a
double_a = add_a(a)
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 4)

    def test_double_closure(self):
        source = """
a = Nat(2)
b = Nat(4)
def add_a(x: Nat) -> Nat:
   return x + a + b
double_a = add_a(a)
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 8)

    def test_func_in_func_closure(self):
        source = """
a = Nat(2)
b = Nat(4)

def add_b(y: Nat) -> Nat:
        return y + b

def add_ab(x: Nat) -> Nat:
   return a + add_b(x)

result = add_ab(Nat(1))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 7)

    def test_nested_functions(self):
        source = """
a = Nat(2)
b = Nat(4)

def add_ab(x: Nat) -> Nat:
    def add_b(y: Nat) -> Nat:
        return y + b
    return a + add_b(x)

result = add_ab(Nat(1))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 7)

    def test_no_closure(self):
        source = """
def f(x: Nat) -> Nat:
   return x * Nat(2)
b = f(Nat(2))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 4)


    def test_var_and_func_closure(self):
        source = """
def f(x: Nat) -> Nat:
   return x * Nat(2)

a = Nat(1)
b = Nat(2)

def g(x: Nat) -> Nat:
    c = f(a + b)
    return c

g(Nat(2))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 6)


    def test_use_closure_var_twice(self):
        source = """
PRICE_NUM = Nat(3)

def f(x: Nat) -> Nat:
    y = x * PRICE_NUM
    return y * PRICE_NUM

f(Nat(2))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), 18)


class TestSet(unittest.TestCase):
    def test_new_empty_set(self):
        source = """
a = Set[Nat]()
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertTrue(isinstance(vm.stack.peek(), SetType))

    def test_set_add(self):
        source = """
a = Set[Nat]()
a.add(Nat(1))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertTrue(isinstance(vm.stack.peek(), SetType))
        self.assertEqual(vm.stack.peek().to_python_object(), [1])

    def test_set_remove(self):
        source = """
a = Set[Nat]()
a.add(Nat(1))
a.remove(Nat(1))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertTrue(isinstance(vm.stack.peek(), SetType))
        self.assertEqual(vm.stack.peek().to_python_object(), [])

    def test_set_fun_arg(self):
        source = """
def set_add(el: Nat, s: Set[Nat]) -> Set[Nat]:
    return s.add(Nat(1))

set_add(Nat(1), Set[Nat]())
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertTrue(isinstance(vm.stack.peek(), SetType))
        self.assertEqual(vm.stack.peek().to_python_object(), [1])

    def test_el_not_in_set(self):
        source = """
Nat(1) not in Set[Nat]()
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), True)

    def test_el_in_set(self):
        source = """
Nat(1) in Set[Nat]()
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), False)

    def test_set_add(self):
        source = """
Set[Nat]().add(Nat(2)).add(Nat(1))
        """
        vm = VM()
        compiler = Compiler(source)
        micheline = compiler.compile_expression()
        vm.execute(micheline)
        self.assertEqual(vm.stack.peek().to_python_object(), [1, 2])

if __name__ == "__main__":
    unittest.main()
